<?php
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use App\Models\User\Rights;
use App\Models\User\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    /*
     * Authorizing resourse
     */
    public function __construct(){
        // $this->authorizeResource(Role::class, 'role');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
        ->active(1)
        ->get();
        $resources = Role::all();
        return view('role', [
            'resources' => $resources,
            'modules' => $modules,
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Rights::all();
        return view('role_create', [
            'rights' => $rights
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Role();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        
        /*
         * Create new array
         */
        $newPermissions = [];
        foreach($request->permissions as $key=>$value){
            $newPermissions[$value] = 1;
        }
        
        $resource = Role::create([
            'title' => $request->title,
            'slug' => $request->slug,
            'permissions' => $newPermissions,
        ]);

        Session::flash('message','Role created successfully!');
        Session::flash('alert-type','success');
        return response()->json($resource);
    }
    
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $rightsArray = [];
        foreach ($role->permissions as $slug => $value){
            array_push($rightsArray, $slug) ;
        }
        $permissions = Rights::all();
        
        return view('role_edit', [
            'rights' => $rightsArray,
            'permissions' => $permissions,
        ]);

    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $model = new Role();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        /*
         * Create new array
         */
        $newPermissions = [];
        foreach($request->permissions as $key=>$value){
            $newPermissions[$value] = 1;
        }
        
        $resource = $role->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'permissions' => $newPermissions,
        ]);

        Session::flash('message','Role updated successfully!');
        Session::flash('alert-type','success');
        return response()->json($resource);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }

    public function modify(Request $request, Role $role)
    {
        $model = new Role();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        /*
         * Create new array
         */
        $newPermissions = [];
        foreach($request->permissions as $key=>$value){
            $newPermissions[$value] = 1;
        }
        
        $resource = $role->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'permissions' => $newPermissions,
        ]);

        Session::flash('message','Role updated successfully!');
        Session::flash('alert-type','success');
        return response()->json($resource);
    }

    public function delete(Role $role)
    {
        $role->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }
    
}
