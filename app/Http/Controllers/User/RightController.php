<?php
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use App\Models\User\Rights;
use Illuminate\Support\Facades\Validator;

class RightController extends Controller
{
    /*
     * Authorizing resourse
     */
    public function __construct(){
        // $this->authorizeResource(Rights::class, 'right');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
        $resources = Rights::all();
        return view('right', [
            'resources' => $resources,
            'modules' => $modules
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('right_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Rights();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        $resource = Rights::create($request->all());
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('right_edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rights $right)
    {
        $model = new Rights();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        $resource = $right->update($request->all());
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rights $right)
    {
        $right->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }
}
