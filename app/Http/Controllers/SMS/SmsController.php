<?php
namespace App\Http\Controllers\SMS;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use AfricasTalking\SDK\AfricasTalking;
use App\Models\SMS\Sms;
use App\Models\Admin\Caterers;
use App\User;
use App\Models\Admin\Modules;
use App\Models\Admin\Visitors;
use App\Models\Admin\Students;

class SmsController extends Controller
{

    /*
     * Authorizing resourse
     */
    public function __construct(){
        // $this->authorizeResource(Sms::class, 'sms');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
        ->active(1)
        ->get();
        
        $messages = Sms::all();
        $model = new Sms();
        $colors = $model->colors;
        return view('sms', [
            'messages' => $messages,
            'colors' => $colors,
            'modules' => $modules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Sms();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource = Sms::create($request->all());
            toastr()->success('Sms template saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sms $sm)
    {
        $model = new Sms();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource = $sm->update([
                'title' => $request->title,
                'detail' => $request->detail
            ]);
            toastr()->success('Record Updated successfully!', 'Success', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sms $sm)
    {
        $sm->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }

    public function sendSms(Request $request)
    {
        $group = $request->group;
        if ($group == 1) {
            $recipients = Caterers::active(1)->get('phone');
        } elseif ($group == 2) {
            $recipients = User::active(1)->get('phone');
        } elseif ($group == 3) {
            $recipients = Visitors::active(1)->get('phone');
        } elseif ($group == 4) {
            $recipients = Students::active(1)->get('phone');
        } else {
            return response()->json([
                'error2' => 'You must select atleast one recepient'
            ]);
        }
        $messages = $request->messages;
        if(count($request->messages)<3){
            return response()->json([
                'error' => 'You must select atleast one template'
            ]);
        }

        foreach ($recipients as $recipient) {
            for ($i = 1; $i < count($messages) - 1; $i ++) {
                $text = $messages[$i];
                $this->send($text,$recipient->phone);
            }
        }
        return response()->json([
            'success' => 'Messages sent successfuly!'
        ]);
    }

    public function send($message, $recepient)
    {
        // add AT API Key
        $userName = "eboard";
        $apiKey = "c83e6a8a02b740383c85ff37e94d76c8242b4ddb418ecb8b2b150562df64a348";
        $AT = new AfricasTalking($userName, $apiKey);
        $from = "eHorizon";
        
        $sms = $AT->sms();
        $response = $sms->send([
            "to" => $recepient,
            "from"=> $from,
            "message" => $message
        ]);
        
        return response()->json($response);
    }
}
