<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use App\User;

class UserGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users_active = User::active(1)->get();
        $users= User::all();
        $modules = Modules::with('children')->root(1)->active(1)->get();
        return view('users',[
            'users'=>$users,$modules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $model = new User();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
            else {
                $request->password = Hash::make($request->password);
                $resource = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'active' => $request->active,
                    'role' => $request->role,
                    'password' => $request->password,
                ]);
            }
            toastr()->success('User saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user_show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user_edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $model = new User();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
            else {
                $request->password = Hash::make($request->password);
                
                $resource = $user->update([
                    'phone' => $request->phone,
                    'active' => $request->active,
                    'role' => $request->role,
                    'password' => $request->password,
                ]);
            }
            toastr()->success('User saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        toastr('Record deleted!','success','sucess');
    }
}
