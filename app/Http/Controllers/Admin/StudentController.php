<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use App\Models\Admin\Students;
use App\Exports\StudentsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StudentsImport;

class StudentController extends Controller
{
    /*
     * Authorizing resourse
     */
    public function __construct(){
        // $this->authorizeResource(Students::class, 'student');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Students::all();
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
        return view('student', [
            'students' => $students,
            'modules' => $modules
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Students();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource = Students::create($request->all());
        }
        toastr()->success('Student created successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('student_show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('student_edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Students $student)
    {
        $model = new Students();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource = $student->update($request->all());
        }
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Students $student)
    {
        $student->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }

    public function createImport()
    {
        return view('student_import');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'require|mimes:xml,xlsx'
        ]);
        
        $students = Excel::toCollection(new StudentsImport(), $request->file('import_file'));
        // Truncate the table
        Students::truncate();
        
        foreach ($students[0] as $student) {
            $data = Students::create([
                'title' => $student[1],
                'school' => $student[2],
                'phone' => $student[3],
                'address' => $student[4],
                'active' => $student[5]
            ]);
        }
        if ($data) {
            return 'success';
        } else {
            return Response::json(array(
                'errors' => $data
            ));
        }
        
        // $path = $request->file('select_file')->getRealPath();
    }

    public function export()
    {
        return Excel::download(new StudentsExport(), 'mealbs_students_export.xlsx');
    }
}
