<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Route;

class ModuleController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)->active(1)->get();
        $resources = Modules::all();
        return view('modules',[
            'resources' =>$resources,
            'modules' => $modules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Modules::all();
        $routes = Route::all();
        return view('module_create',[
            'modules' => $modules,
            'routes' => $routes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Modules();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()){
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        $resource = Modules::create($request->all());
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modules = Modules::all();
        $routes = Route::all();
        return view('module_edit',[
            'modules' => $modules,
            'routes' => $routes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modules $module)
    {
        $model = new Modules();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()){
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        $resource = $module->update($request->all());
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modules $module)
    {
        $module->delete();
        toastr('Record deleted!','success','sucess');
    }
}
