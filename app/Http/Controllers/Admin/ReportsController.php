<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Reports\SalesByCustomer;
use App\Models\Admin\Modules;
use App\Models\Booking\Booking;
use App\Models\Reports\BookingChecklist;
use App\Models\Reports\BookingReport;
use App\Reports\MyReport;
use App\User;
use PdfReport;
use ExcelReport;
use Illuminate\Support\Facades\Date;

// require_once "SalesByCustomer.php";

class ReportsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
    public function bookingReport()
    {
        $modules =  HomeController::getModules();
        $users = User::all();
        $today = Carbon::today();
        $now = Carbon::now();

        $report = new BookingReport([
            'start_date' =>  $today,
            'end_date' =>  $now,
        ]);
        $report->run();

        return view("reports.booking_report", [
            'modules' => $modules,
            'users' => $users,
            'report' => $report,
        ]);
    }

    public function bookingReportLoad(Request $request)
    {
        $report = new BookingReport([
            'start_date' =>  $request->start_date,
            'end_date' =>  $request->end_date
        ]);
        $report->run();

        return view("reports.rpt_booking_report", [
            "report" => $report,
            'start_date' =>  $request->start_date,
            'end_date' =>  $request->end_date
        ]);
    }

    public function bookingChecklist()
    {
        $modules =  HomeController::getModules();
        $today = Carbon::today();
        // $today = '2019-10-21';

        $report = new BookingChecklist([
            'start_date' =>  $today,
        ]);
        $report->run();

        return view("reports.booking_checklist", [
            'modules' => $modules,
            'report' => $report,
        ]);
    }

    public function bookingChecklistLoad(Request $request)
    {
        $time = strtotime($request->start_date);
        $report = new BookingChecklist([
            'start_date' =>  date('Y-m-d',$time),
        ]);
        $report->run();

        return view("reports.rpt_booking_checklist", [
            "report" => $report,
            'start_date' =>  date('Y-m-d',$time),
        ]);
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
