<?php

namespace  App\Http\Controllers\Reports;

// require_once "koolreport/core/autoload.php";
use \koolreport\processes\Group;
use \koolreport\processes\Sort;
use \koolreport\processes\Limit;

class SalesByCustomer extends \koolreport\KoolReport
{
    public function settings()
    {
        return array(
            "dataSources"=>array(
                "sales"=>array(
                    "connectionString"=>"mysql:host=localhost;dbname=test",
                    "username"=>"root",
                    "password"=>"P@ssword123!",
                    "charset"=>"utf8"
                )
            )
        );
    }

    public function setup()
    {
        $this->src('sales')
        ->query("SELECT * FROM users")
        ->pipe(new Limit(array(10)))
        ->pipe($this->dataStore('users'));
    }
}