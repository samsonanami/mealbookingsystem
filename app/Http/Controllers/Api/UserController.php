<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Menu\Menu;
use App\Models\Booking\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller 
{
    public $successStatus = 200;
    public $errorStatus = 401;
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            
            return response()->json(['success'=>1, 'message'=>'Authenticated', 'data'=>$user, 'token' => $user->createToken('MyApp')-> accessToken], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['success'=>0, 'message'=>'Incorrect email or password'], $this-> successStatus); 
        } 
    } 
    
    public function menu()
    {
        
        $menu = Menu::where([
            'start' => date("Y-m-d",strtotime(request('start'))), 'end'=>date("Y-m-d",strtotime(request('end')))
        ])->get();
        
        return response()->json(['success'=>1, 'message'=>'Menu', 'data' => $menu], $this-> successStatus);
    }
    public function book(Request $request)
    {
        $model = new Booking();
        $request['start'] = date("Y-m-d", strtotime($request['start']));
        $request['end'] = date("Y-m-d", strtotime($request['end']));
        
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return response()->json(['success' => 0, 'message'=>$validator->getMessageBag()->toarray()], $this-> successStatus);
        } else {
            $resource = Booking::create($request->all());
            return response()->json(['success' => 1, 'message'=>'Booking completed successfully','data' => $resource], $this-> successStatus);
        }
    }
    public function booking(Request $request)
    {
        $user_bookings = Booking::where([
            'owner' => $request['user_id']
        ])->orderBy('id', 'desc')->get();
        
        return response()->json(['success' => 1, 'message'=>' User bookings','data' => $user_bookings], $this-> successStatus);
    }
    
    public function cancel(Request $request)
    {
        $check = Booking::find($request['id']);
        if ($check) {
            if(strtotime($check->start) < strtotime(date("Y-m-d"))){
                return response()->json(['success' => 1, 'message'=>'Sorry, you can\'t cancel booking for past days'], $this-> successStatus);
            }else{
                $delete = Booking::where('id', $request['id'])->delete();
                if ($delete == 1) {
                    return response()->json(['success' => 1, 'message'=>'Your booking has been cancelled'], $this-> successStatus);
                }else{
                    return response()->json(['success' => 1, 'message'=>'Your booking could not be cancelled'], $this-> successStatus);
                }
            }
        }else{
            return response()->json(['success' => 1, 'message'=>'No record found'], $this-> successStatus);
        }
    }
}