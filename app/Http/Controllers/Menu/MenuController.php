<?php
namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Menu\Meal;
use App\Menu\Menu;
use App\Models\Admin\Modules;
use App\Models\Booking\Booking;
use App\User;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    /*
     * Authorizing resourse
     */
    public function __construct(){
        // $this->authorizeResource(Menu::class, 'menu');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
        foreach ($modules as $module) {
            $module['permKey'] = 'view-' . $module->title;
        }
        
        $meals = Meal::all();
        $menus = Menu::all();
        $labels = [
            'label-info',
            'label label-primary',
            'label-success',
            'label-info',
            'label tar',
            'label orange',
            'label-warning',
            'label-danger',
            'label-info'
        ];
        return view('menu', [
            'menus' => $menus,
            'labels' => $labels,
            'meals' => $meals,
            'modules' => $modules
        ]);
    }

    public function calendar()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
        $meals = Meal::all();
        $menus = Menu::all();
        $user =  Auth::user();
        $users = User::all();
        $labels = [
            'label-info',
            'label label-primary',
            'label-success',
            'label-info',
            'label tar',
            'label orange',
            'label-warning',
            'label-danger',
            'label-info'
        ];
        return view('menu_calendar', [
            'menus' => $menus,
            'labels' => $labels,
            'meals' => $meals,
            'modules' => $modules,
            'user' => $user,
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Menu();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        } else {
            $resource = Menu::create($request->all());
            toastr()->success('Record saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $model = new Menu();
        $validation = Validator::make($request->all(), $model->rules);
        $resource = $menu->update([
            'title' => $request->title,
            'detail' => $request->detail,
            'start' => $request->start,
            'end' => $request->end
        ]);
        toastr()->success('Record Updated successfully!', 'Success', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }

    public function getEvents()
    {
        $menus = Menu::get()->toArray();
        $user_id = Auth::user()->id;
        $bookings = Booking::where([
            'owner' => $user_id
        ])->get()->toArray();
        $resource = array_merge($bookings, $menus);
        $events = Response::json($resource);
        
        return $resource;
    }
}
