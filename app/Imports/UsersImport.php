<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'name' => $row[1],
            'payroll' => $row[2],
            'email' => $row[3],
            'password' =>Hash::make($row[4]),
            'phone' => $row[5], 
            'active' => $row[6],
            // 'role' => $row[7],
        ]);
    }
}
