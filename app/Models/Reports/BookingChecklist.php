<?php

namespace App\Models\Reports;


use koolreport\KoolReport;
use koolreport\laravel\Friendship;

class BookingChecklist extends KoolReport
{
    use Friendship;

    function setup()
    {
        $day1 = $this->params["start_date"];
        $day1_name = date("l", strtotime($day1));
        $day2 = date("Y-m-d", strtotime($day1. ' + 1 days'));
        $day2_name = date("l", strtotime($day2));
        $day3 = date("Y-m-d",strtotime($day1. ' + 2 days'));
        $day3_name = date("l", strtotime($day3));
        // dd($day3_name);
        $day4 = date("Y-m-d",strtotime($day1. ' + 3 days'));
        $day4_name = date("l", strtotime($day4));
        $day5 = date("Y-m-d",strtotime($day1. ' + 4 days'));
        $day5_name = date("l", strtotime($day5));
        $day6 = date("Y-m-d",strtotime($day1. ' + 5 days'));
        $day6_name = date("l", strtotime($day6));
        $day7 = date("Y-m-d",strtotime($day1. ' + 6 days'));
        $day7_name = date("l", strtotime($day7));

        // dd($day2);
        // $day1 = date(strtotime($day1));

        $this->src("mysql")
            ->query(
                "SELECT  own.payroll `Payroll`, usr.name `Booked For`, usr.name `User Name`, sum(bkg.price) `Total Price`, 
                            sum(IF(date(bkg.start) = '$day1', 1,0)) AS '$day1_name',
                            sum(IF(date(bkg.start) = '$day2', 1,0)) AS '$day2_name',
                            sum(IF(date(bkg.start) = '$day3', 1,0)) AS '$day3_name',
                            sum(IF(date(bkg.start) = '$day4', 1,0)) AS '$day4_name',
                            sum(IF(date(bkg.start) = '$day5', 1,0)) AS '$day5_name',
                            sum(IF(date(bkg.start) = '$day6', 1,0)) AS '$day6_name',
                            sum(IF(date(bkg.start) = '$day7', 1,0)) AS '$day7_name'
                            FROM `bookings` bkg 
                            JOIN `users` usr ON usr.id = bkg.user_id
                            JOIN `users` own ON own.id = bkg.owner
                            WHERE
                            bkg.group IS NULL
                            AND
                            date(bkg.start) BETWEEN :date_from AND :date_to
                            GROUP BY usr.name, `Payroll`
                            UNION 
-- for visitor
                SELECT  'N/A' `Payroll`, vst.title `Booked For`, vst.title `User Name`, sum(bkg.price) `Total Price`, 
                            sum(IF(date(bkg.start) = '$day1', 1,0)) AS '$day1_name',
                            sum(IF(date(bkg.start) = '$day2', 1,0)) AS '$day2_name',
                            sum(IF(date(bkg.start) = '$day3', 1,0)) AS '$day3_name',
                            sum(IF(date(bkg.start) = '$day4', 1,0)) AS '$day4_name',
                            sum(IF(date(bkg.start) = '$day5', 1,0)) AS '$day5_name',
                            sum(IF(date(bkg.start) = '$day6', 1,0)) AS '$day6_name',
                            sum(IF(date(bkg.start) = '$day7', 1,0)) AS '$day7_name'
                            FROM `bookings` bkg 
                            JOIN `users` usr ON usr.id = bkg.user_id
                            LEFT JOIN `visitors` vst ON vst.id = bkg.owner
                            WHERE
                            bkg.group = 3
                            AND
                            bkg.start BETWEEN :date_from1 AND :date_to1
                            GROUP BY usr.name, `Payroll`,vst.title
                            UNION 
-- for students
                SELECT  'N/A' `Payroll`, std.title `Booked For`, usr.name `User Name`, sum(bkg.price) `Total Price`, 
                            sum(IF(date(bkg.start) = '$day1', 1,0)) AS '$day1_name',
                            sum(IF(date(bkg.start) = '$day2', 1,0)) AS '$day2_name',
                            sum(IF(date(bkg.start) = '$day3', 1,0)) AS '$day3_name',
                            sum(IF(date(bkg.start) = '$day4', 1,0)) AS '$day4_name',
                            sum(IF(date(bkg.start) = '$day5', 1,0)) AS '$day5_name',
                            sum(IF(date(bkg.start) = '$day6', 1,0)) AS '$day6_name',
                            sum(IF(date(bkg.start) = '$day7', 1,0)) AS '$day7_name'
                            FROM `bookings` bkg 
                            JOIN `users` usr ON usr.id = bkg.user_id
                            LEFT JOIN `students` std ON std.id = bkg.owner
                            WHERE
                            bkg.group = 3
                            AND
                            bkg.start BETWEEN :date_from2 AND :date_to2
                            GROUP BY usr.name, `Payroll`, std.title
                            "
            )->params(array(
                ":date_from" => $this->params["start_date"],
                ":date_to" => $day5,
                ":date_from1" => $this->params["start_date"],
                ":date_to1" => $day5,
                ":date_from2" => $this->params["start_date"],
                ":date_to2" => $day5,
            ))
            ->pipe($this->dataStore("bookings_data"));
    }
}
