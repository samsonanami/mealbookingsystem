<?php

namespace App\Models\Reports;


use koolreport\KoolReport;
use koolreport\laravel\Friendship;

class BookingReport extends KoolReport
{
    use Friendship;

    function setup()
    {
        $this->src("mysql")
            ->query(
                "SELECT  own.payroll `Payroll`, usr.name `Booked For`, usr.name `User Name`, count(*) `Plates`, sum(bkg.price) `Total Price` 
                            FROM `bookings` bkg 
                            JOIN `users` usr ON usr.id = bkg.user_id
                            JOIN `users` own ON own.id = bkg.owner
                            LEFT JOIN `students` std ON std.id = bkg.owner
                            LEFT JOIN `visitors` vst ON vst.id = bkg.owner
                            WHERE
                            bkg.group IS NULL
                            AND
                            bkg.start BETWEEN :date_from AND :date_to
                            GROUP BY usr.name, own.payroll

                            UNION
                        --    for users
                            SELECT  own.payroll `Payroll`, usr.name `Booked For`, usr.name `User Name`,  count(*) `Plates`, sum(bkg.price) `Total Price` 
                            FROM `bookings` bkg 
                            JOIN `users` usr ON usr.id = bkg.user_id
                            JOIN `users` own ON own.id = bkg.owner
                            LEFT JOIN `students` std ON std.id = bkg.owner
                            LEFT JOIN `visitors` vst ON vst.id = bkg.owner
                            WHERE
                            bkg.group = 2
                            AND
                            bkg.start BETWEEN :date_from1 AND :date_to1
                            GROUP BY usr.name , Payroll
                           
                            "
            )->params(array(
                ":date_from" => $this->params["start_date"],
                ":date_to" => $this->params["end_date"],
                ":date_from1" => $this->params["start_date"],
                ":date_to1" => $this->params["end_date"]
            ))
            ->pipe($this->dataStore("bookings_data"));
    }
}
