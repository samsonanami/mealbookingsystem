<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Admin\Caterers;
use App\Models\Admin\Students;
use App\Models\Admin\Visitors;

class Booking extends Model
{
    protected $table = 'bookings';
    protected $fillable = ['title', 'user_id', 'start', 'end', 'created_by','owner', 'group', 'color'];
    
    public $rules = [
        'user_id' => 'integer',
        'title' => 'nullable|string',
        'color' => '',
        'start' => 'string|max:20',
        'end' => 'string|max:20',
        'owner' => 'integer',
        'group' => 'integer',
        'created_by' => 'string|max:100'
    ];
    
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function owners(){
        return $this->hasOne(User::class, 'id', 'owner');
    }
    public function caterer(){
        return $this->hasOne(Caterers::class, 'id', 'owner');
    }
    public function visitor(){
        return $this->hasOne(Visitors::class, 'id', 'owner');
    }
    public function student(){
        return $this->hasOne(Students::class, 'id', 'owner');
    }

    public function scopeUser($query){
        return $query->where('user_id','=', 'owner');
    }
    
}
