<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Admin\Students;
use App\Models\Admin\Visitors;
use App\Models\Admin\Caterers;

class AdminBooking extends Model
{
    protected $table = 'bookings';
    protected $fillable = ['user_id', 'start', 'end', 'created_by','owner', 'group'];
    
    public $rules = [
        'user_id' => 'integer',
        'start' => 'string|max:20',
        'end' => 'string|max:20',
        'group' => 'required|integer',
        'created_by' => 'string|max:100'
    ];

    public $rulesForSingleUser = [
        'user_id' => 'integer',
        'start' => 'required',
        'end' => 'required',
        'created_by' => 'string|max:100'
    ];
    
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function owner(){
        return $this->hasOne(User::class, 'id', 'owner');
    }
    public function student(){
        return $this->hasOne(Students::class, 'id', 'owner');
    }
    public function visitor(){
        return $this->hasOne(Visitors::class, 'id', 'owner');
    }    
    public function caterer(){
        return $this->hasOne(Caterers::class, 'id', 'owner');
    }
}
