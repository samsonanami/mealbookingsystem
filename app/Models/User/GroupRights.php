<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class GroupRights extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['id_group','id_right'];
}
