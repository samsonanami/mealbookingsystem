<?php

namespace App\Models\User;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Rights extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['title','detail'];
    
    public $rules = [
        'title'=> 'string|unique:rights',
        'detail' => 'string',
    ];
    
    public $update_rules = [
        'title'=> 'string',
        'detail' => 'string',
    ];
    
    public function group(){
        return $this->belongsToMany(UserGroups::class, 'group_rights', 'id_group', 'id_right');
    }
}
