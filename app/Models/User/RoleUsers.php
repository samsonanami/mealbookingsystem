<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class RoleUsers extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','role_id'];
    
}
