<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable = ['name', 'detail'];
    
    public $rules = [
        'name' => 'string|max:100',
        'detail' => 'string'
    ];
}
