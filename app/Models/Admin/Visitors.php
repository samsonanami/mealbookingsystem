<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Visitors extends Model
{
    protected $primaryKey = 'id';
    
    protected $fillable = ['title', 'address', 'phone', 'active'];
    
    public $rules = [
        'title'=>'string|unique:caterers',
        'phone'=>'required|string|unique:caterers',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ];    
    
    public $update_rules = [
        'phone'=>'required|string',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ];  
    
    public function scopeActive($query, $value){
        return $query->where('active',$value);
    }
}
