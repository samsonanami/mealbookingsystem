<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $primaryKey = 'id';
    
    protected $fillable = ['title', 'school', 'address', 'phone', 'active'];
    
    public $rules = [
        'title'=>'string|unique:students',
        'school'=>'string',
        'phone'=>'required|string|unique:students',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ];
    
    
    public $update_rules = [
        'title'=>'string',
        'school'=>'string',
        'phone'=>'required|string',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ]; 
    
    public function scopeActive($query, $value){
        return $query->where('active',$value);
    }
        
}
