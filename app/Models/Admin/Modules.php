<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['title','color','detail','order', 'link', 'icon', 'id_parent', 'is_root', 'active'];

    public $rules = [
        'title'=>'required|string',
        'color'=>'nullable|string',
        'link' => 'required|string',
        'detail' => 'nullable|string',
        'icon' => 'required|string|nullable',
        'id_parent' => 'integer|nullable',
        'is_root' => 'nullable',
        'active' => 'integer'
    ];
    
    public function parent(){
        return $this->belongsTo(Modules::class, 'id_parent');
    }
    
    public function children(){
        return $this->hasMany(Modules::class, 'id_parent');    
    }
    
    public function scopeRoot($query, $value){
        return $query->where('is_root', $value);
    }
    
    public function scopeActive($query, $value){
        return $query->where('active', $value);
    }
}