<?php

namespace App\Models\SMS;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $primaryKey = 'id';
    
    protected $fillable = ['title', 'detail'];
    
    public $rules = [
        'title' => 'required|string|max:100|unique:sms',
        'detail' => 'required|string'
    ];
    public $update_rules = [
        'title' => 'required|string|max:100|unique:sms',
        'detail' => 'required|string'
    ];
    
    public $colors = [
        'red',
        'green',
        'blue',
        'yellow',
        'purple',
    ];
}
