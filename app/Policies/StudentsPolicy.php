<?php
namespace App\Policies;

use App\User;
use App\Models\Admin\Students;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any students.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess([
            'view-student'
        ]);
    }

    /**
     * Determine whether the user can view the students.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Students $students
     * @return mixed
     */
    public function view(User $user, Students $students)
    {
        return $user->hasAccess([
            'view-student'
        ]);
    }

    /**
     * Determine whether the user can create students.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess([
            'create-student'
        ]);
    }

    /**
     * Determine whether the user can update the students.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Students $students
     * @return mixed
     */
    public function update(User $user, Students $students)
    {
        return $user->hasAccess([
            'update-student'
        ]);
    }

    /**
     * Determine whether the user can delete the students.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Students $students
     * @return mixed
     */
    public function delete(User $user, Students $students)
    {
        return $user->hasAccess([
            'delete-student'
        ]);
    }

    /**
     * Determine whether the user can restore the students.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Students $students
     * @return mixed
     */
    public function restore(User $user, Students $students)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the students.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Students $students
     * @return mixed
     */
    public function forceDelete(User $user, Students $students)
    {
        //
    }
}
