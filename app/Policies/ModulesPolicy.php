<?php
namespace App\Policies;

use App\User;
use App\Models\Admin\Modules;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModulesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any modules.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess([
            'view-module'
        ]);
    }

    /**
     * Determine whether the user can view the modules.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Modules $modules
     * @return mixed
     */
    public function view(User $user, Modules $modules)
    {
        return $user->hasAccess([
            'view-module'
        ]);
    }

    /**
     * Determine whether the user can create modules.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess([
            'create-module'
        ]);
    }

    /**
     * Determine whether the user can update the modules.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Modules $modules
     * @return mixed
     */
    public function update(User $user, Modules $modules)
    {
        return $user->hasAccess([
            'update-module'
        ]);
    }

    /**
     * Determine whether the user can delete the modules.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Modules $modules
     * @return mixed
     */
    public function delete(User $user, Modules $modules)
    {
        return $user->hasAccess([
            'delete-module'
        ]);
    }

    /**
     * Determine whether the user can restore the modules.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Modules $modules
     * @return mixed
     */
    public function restore(User $user, Modules $modules)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the modules.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Modules $modules
     * @return mixed
     */
    public function forceDelete(User $user, Modules $modules)
    {
        //
    }
}
