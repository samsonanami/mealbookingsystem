<?php
namespace App\Policies;

use App\User;
use App\Models\Admin\Caterers;
use Illuminate\Auth\Access\HandlesAuthorization;

class CatererPolicy
{
    use HandlesAuthorization;
    
    
    public function viewAny(User $user){
        return $user->hasAccess([
            'view-caterer'
        ]);
    }

    /**
     * Determine whether the user can view the caterers.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Caterers $caterers
     * @return mixed
     */
    public function view(User $user, Caterers $caterers)
    {
        return $user->hasAccess([
            'view-caterer'
        ]);
    }

    /**
     * Determine whether the user can create caterers.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess([
            'create-caterer'
        ]);
    }

    /**
     * Determine whether the user can update the caterers.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Caterers $caterers
     * @return mixed
     */
    public function update(User $user, Caterers $caterers)
    {
        return $user->hasAccess([
            'update-caterer'
        ]);
    }

    /**
     * Determine whether the user can delete the caterers.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Caterers $caterers
     * @return mixed
     */
    public function delete(User $user, Caterers $caterers)
    {
        return $user->hasAccess([
            'delete-caterer'
        ]);
    }

    /**
     * Determine whether the user can restore the caterers.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Caterers $caterers
     * @return mixed
     */
    public function restore(User $user, Caterers $caterers)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the caterers.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Caterers $caterers
     * @return mixed
     */
    public function forceDelete(User $user, Caterers $caterers)
    {
        //
    }
}
