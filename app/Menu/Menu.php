<?php

namespace App\Menu;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['name', 'detail', 'title', 'start', 'end'];
    protected $nullable = ['url'];
    
    public $rules = [
        'name' => '',
        'detail' => '',
        'title' =>''
    ];

}
