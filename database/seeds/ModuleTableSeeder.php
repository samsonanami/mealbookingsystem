<?php
use Illuminate\Database\Seeder;
use App\Models\Admin\Modules;

class ModuleTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1 Create Dashboard Link
        Modules::create([
            'title' => 'Dashboard',
            'color' => '#FFF',
            'detail' => 'Main Panel',
            'order' => '999',
            'link' => 'home',
            'icon' => 'fa fa-dashboard',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1,
        ]);
        
        //2 Create Menu Link
        Modules::create([
            'title' => 'Menu',
            'color' => '#FFF',
            'detail' => 'Menu Panel',
            'order' => '999',
            'link' => 'default',
            'icon' => 'fa fa-laptop',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);
        
        //3 Create Menus Link
        Modules::create([
            'title' => 'Menu(s)',
            'color' => '#FFF',
            'detail' => 'Menu Panel',
            'order' => '999',
            'link' => 'menu.index',
            'icon' => '',
            'id_parent' => 2,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //4 Create Menu Calendar Link
        Modules::create([
            'title' => 'Menu Calendar',
            'color' => '#FFF',
            'detail' => 'Menu Calendar Panel',
            'order' => '999',
            'link' => 'menu_calendar',
            'icon' => '',
            'id_parent' => 2,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //5 Create Booking Link
        Modules::create([
            'title' => 'Booking',
            'color' => '#FFF',
            'detail' => 'Booking Panel',
            'order' => '999',
            'link' => 'default',
            'icon' => 'fa fa-book',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);
        
        //6 Create Meal Booking Link
        Modules::create([
            'title' => 'Meal Booking',
            'color' => '#FFF',
            'detail' => 'Meal Booking Panel',
            'order' => '999',
            'link' => 'booking.index',
            'icon' => '',
            'id_parent' => 5,
            'is_root' => 0,
            'active' => 1
        ]);
        
        
        //7 Create Admin Booking Link
        Modules::create([
            'title' => 'Admin Booking',
            'color' => '#FFF',
            'detail' => 'Booking Panel',
            'order' => '999',
            'link' => 'booking_admin.index',
            'icon' => '',
            'id_parent' => 5,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //8 Create SMS Link
        Modules::create([
            'title' => 'SMS',
            'color' => '#FFF',
            'detail' => 'SMS Panel',
            'order' => '999',
            'link' => 'default',
            'icon' => 'fa fa-envelope',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);
        
        //9 Create SMS Reminders Link
        Modules::create([
            'title' => 'SMS Reminders',
            'color' => '#FFF',
            'detail' => 'Reminders Panel',
            'order' => '999',
            'link' => 'sms.index',
            'icon' => '',
            'id_parent' => 8,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //10 Create Admin Link
        Modules::create([
            'title' => 'Admin',
            'color' => '#FFF',
            'detail' => 'Administration Panel',
            'order' => '999',
            'link' => 'default',
            'icon' => 'fa fa-cogs',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);
        
        //11 Create Meals Link
        Modules::create([
            'title' => 'Meals',
            'color' => '#FFF',
            'detail' => 'Administration Panel',
            'order' => '999',
            'link' => 'meal.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //12 Create Users Link
        Modules::create([
            'title' => 'Users',
            'color' => '#FFF',
            'detail' => 'Users Panel',
            'order' => '999',
            'link' => 'user.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);
        //13 Create User Right  Link
        Modules::create([
            'title' => 'Roles',
            'color' => '#FFF',
            'detail' => 'User Roles Panel',
            'order' => '999',
            'link' => 'role.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);
        //14 Create User Group Link
        Modules::create([
            'title' => 'Permissions',
            'color' => '#FFF',
            'detail' => 'User Group Panel',
            'order' => '999',
            'link' => 'right.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //15 Create Student Link
        Modules::create([
            'title' => 'Students',
            'color' => '#FFF',
            'detail' => 'Students Panel',
            'order' => '999',
            'link' => 'student.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //16 Create Caterers Link
        Modules::create([
            'title' => 'Caterers',
            'color' => '#FFF',
            'detail' => 'Caterers Panel',
            'order' => '999',
            'link' => 'caterer.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //17 Create Visitors Link
        Modules::create([
            'title' => 'Visitors',
            'color' => '#FFF',
            'detail' => 'Visitors Panel',
            'order' => '999',
            'link' => 'visitor.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //18 Create Report Link
        Modules::create([
            'title' => 'Report',
            'color' => '#FFF',
            'detail' => 'Visitors Panel',
            'order' => '999',
            'link' => 'report.index',
            'icon' => 'fa fa-list',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);
        
        //19 Create Booking Report Link
        Modules::create([
            'title' => 'Booking Report',
            'color' => '#FFF',
            'detail' => 'Visitors Panel',
            'order' => '999',
            // 'link' => 'report_meal',
            'link' => 'booking-report',
            'icon' => '',
            'id_parent' => 18,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //19 Create Modules Link
        Modules::create([
            'title' => 'Modules',
            'color' => '#FFF',
            'detail' => 'Modules Panel',
            'order' => '999',
            'link' => 'module.index',
            'icon' => '',
            'id_parent' => 10,
            'is_root' => 0,
            'active' => 1
        ]);

         //19 Create Booking Report Link
         Modules::create([
            'title' => 'Booking Checklist',
            'color' => '#FFF',
            'detail' => 'Checklist Panel',
            'order' => '999',
            'link' => 'booking-checklist',
            'icon' => 'fa fa-check',
            'id_parent' => 18,
            'is_root' => 0,
            'active' => 1
        ]);
        
    }
}
