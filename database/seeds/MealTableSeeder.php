<?php

use Illuminate\Database\Seeder;
use App\Menu\Meal;

class MealTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Meal::create([
          'name' => 'Ugali',
          'detail' => 'Maize Ugali',
          'price' => '50',
        ]);
        Meal::create([
          'name' => 'Boiled Beans',
          'detail' => 'Boiled Beans',
          'price' => '80',
        ]);
        Meal::create([
          'name' => 'Chicken',
          'detail' => 'Chicken',
          'price' => '80',
        ]);
        Meal::create([
          'name' => 'Boiled Potatoes',
          'detail' => 'Boiled Potatoes',
          'price' => '80',
        ]);
        Meal::create([
          'name' => 'Fried Potatoes',
          'detail' => 'Friedn Potatoes',
          'price' => '80',
        ]);
        Meal::create([
          'name' => 'Mboga Kienyeji',
          'detail' => 'Mboga Kienyeji',
          'price' => '80',
        ]);
        Meal::create([
          'name' => 'Boiled Rice',
          'detail' => 'Boiled Rice',
          'price' => '80',
        ]);
    }
}
