<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\User\Role;
use App\Models\User\Rights;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         DB::table('users')->insert([
//             'name' => Str::random(10),
//             'email' => Str::random(10).'@gmail.com',
//             'password' => bcrypt('secret'),
//             'role'=>1,
//             'phone'=>'0711111111',
//             'active'=>1
//         ]);
        
        //create admin account & give admin rights
        User::insert([
            'name' => 'admin',
            'payroll' => 'STL000001',
            'email' => 'samsonanami@gmail.com',
            'password' => bcrypt('secret'),
            'phone'=>'0717084613',
            'active'=>1
        ]);
        
        //create user account
        User::insert([
            'name' => 'user',
            'email' => 'anami.samdoh@gmail.com',
            'payroll' => 'STL001520',
            'password' => bcrypt('secret'),
            'phone'=>'0702345678',
            'active'=>1
        ]);
        //create user1 account
        User::insert([
            'name' => 'user1',
            'email' => 'user1@gmail.com',
            'payroll' => 'STL001521',
            'password' => bcrypt('secret'),
            'phone'=>'0712345678',
            'active'=>1
        ]);
        //create user2 account
        User::insert([
            'name' => 'user2',
            'email' => 'user2@gmail.com',
            'payroll' => 'STL001522',
            'password' => bcrypt('secret'),
            'phone'=>'0722345678',
            'active'=>0
        ]);
        //create user3 account
        User::insert([
            'name' => 'user1',
            'email' => 'user3@gmail.com',
            'payroll' => 'STL001523',
            'password' => bcrypt('secret'),
            'phone'=>'0732345678',
            'active'=>0
        ]);
        
        //create test account
        User::insert([
            'name' => 'test',
            'payroll' => 'STL001524',
            'email' => 'test@gmail.com',
            'password' => bcrypt('secret'),
            'phone'=>'0742345678',
            'active'=>1
        ]);
      
    }
}
