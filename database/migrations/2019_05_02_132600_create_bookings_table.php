<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->string('title')->default('Book');
            $table->string('color')->default('#28a745');
            $table->bigIncrements('id');
            $table->string('start', 20);
            $table->string('end', 20);
            $table->unsignedBigInteger('user_id');
            $table->integer('owner');
            $table->string('created_by');
            $table->integer('price')->default(80);
            $table->integer('group')->nullable();
            $table->timestamps();
            $table->unique(['title', 'start' , 'owner']);
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
