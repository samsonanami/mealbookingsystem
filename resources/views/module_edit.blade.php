<form role="form">
    @csrf
    <div class="form-group">
        <label for="id">Module ID<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="id" name="id" disabled>
    </div>
    <div class="form-group">
        <label for="title">Module Title<i class="text-danger">*</i></label>
        <input type="title" class="form-control" id="title" name="title" placeholder="Enter title ...">
        <label class="error-title"></label>
    </div>
    <div class="form-group">
        <label for="color">Color</label>
        <input type="input" class="form-control" id="color" name="color" placeholder="e.g #FFFFF">
        <label class="error-color"></label>
    </div>
    <div class="form-group">
        <label for="order">Order</label>
        <input type="input" class="form-control" id="order" name="order" placeholder="e.g 999">
        <label class="error-order"></label>
    </div>
    
    <div class="form-group">
        <label for="link">Link/Route</label>
        <input type="input" class="form-control" id="link" name="link" placeholder="e.g default">
        <label class="error-link"></label>
    </div>
    <div class="form-group">
        <label for="icon">Icon<i class="text-danger">*</i></label>
        <input type="input" class="form-control" id="icon" name="icon" placeholder="e.g fa fa-coffee">
        <label class="error-icon    "></label>
    </div>
    <div class="form-group">
        <label for="active">Active<i class="text-danger">*</i></label>
        <select class="form-control" id="active" name="active">
            <option value="1" selected>Yes</option>
            <option value="0">No</option>
        </select>
        <label class="error-active"></label>
    </div>
    <div class="form-group">
        <label for="root">Is Root ?<i class="text-danger">*</i></label>
        <select class="form-control" id="root" name="root">
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>
        <label class="error-root"></label>
    </div>
    <div class="form-group">
        <label for="parent">Parent Module<i class="text-danger">*</i></label>
        <select class="form-control" id="parent" name="parent">
        @foreach($modules as $module)
            <option value="{{$module->id}}">{{$module->title }}</option>
        @endforeach
        </select>
        <label class="error-parent"></label>
    </div>

    <div class="form-group">
        <label for="detail">Detail</label>
        <textarea class="form-control" id="detail" name="detail" placeholder="Describe here ..."></textarea>
        <label class="error-detail"></label>
    </div>

    <button type="button" id="add_module" class="btn btn-primary"><i class="fa fa-check" ></i>Submit</button>
</form>


<script type="text/javascript">
// Craete module

$("#add_module").click(function() {
	var url = '{{ route("module.update", ["module"=>":module"]) }}';
    $.ajax({
        type: 'PATCH',
        url: url.replace(":module", $('input[name=id]').val()),
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=title]').val(),
            'color': $('input[name=color]').val(),
            'detail': $('textarea[name=detail]').val(),
            'order': $('input[name=order]').val(),
            'link': $('input[name=link]').val(),
            'icon': $('input[name=icon]').val(),
            'is_root': $('select[name=root]').val(),
            'active': $('select[name=active]').val(),
            'id_parent': $('select[name=parent]').val(),
        },
        success: function(data) {
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden').addClass('text-danger');
                    $('#title').addClass('has-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.color) {
                    $('.error-color').removeClass('hidden').addClass('text-danger');
                    $('#color').addClass('has-error');
                    $('.error-color').text(data.errors.color);
                }else if (data.errors.order) {
                    $('.error-order').removeClass('hidden').addClass('text-danger');
                    $('#order').addClass('has-error');
                    $('.error-order').text(data.errors.order);
                }else if (data.errors.link) {
                    $('.error-link').removeClass('hidden').addClass('text-danger');
                    $('#link').addClass('has-error');
                    $('.error-link').text(data.errors.link);
                }else if (data.errors.icon) {
                    $('.error-icon').removeClass('hidden').addClass('text-danger');
                    $('#icon').addClass('has-error');
                    $('.error-icon').text(data.errors.icon);
                }else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden').addClass('text-danger');
                    $('#detail').addClass('has-error');
                    $('.error-detail').text(data.errors.detail);
                }else if (data.errors.active){
                    $('.error-active').removeClass('hidden').addClass('text-danger');
                    $('#active').addClass('has-error');
                    $('.error-active').text(data.errors.active);
                }else if (data.errors.is_root) {
                    $('.error-root').removeClass('hidden').addClass('text-danger');
                    $('#root').addClass('has-error');
                    $('.error-root').text(data.errors.root);
                }else if (data.errors.parent) {
                    $('.error-parent').removeClass('hidden').addClass('text-danger');
                    $('#parent').addClass('has-error');
                    $('.error-parent').text(data.errors.parent);
                }
            } else {
                console.log(data);
                location.reload();
            }
        },
    });
});
</script>