<form role="form">
    @csrf
    <div class="form-group">
        <label for="id">ID<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="id" name="id" disabled>
    </div>
    <div class="form-group">
        <label for="title">Title<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title ...">
        <label class="error-title"></label>
    </div>
    
    <div class="form-group">
        <label for="slug">Slug<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="slug" name="slug" placeholder="Enter slug...">
        <label class="error-slug"></label>
    </div>
    
    <div class="form-group">
        <h4>Group Roles</h4>
        <hr>
            @foreach($permissions as $permission)
            <div class="checkbox">
            <label  class="form-group" >
                @php
               		$checked = in_array($permission->title,$rights);
                @endphp
            
                @if($checked==true)
					<input type="checkbox" value="{{$permission->title}}" checked> 
				@else
				    <input type="checkbox" value="{{$permission->title}}">      
                @endif
                
                &nbsp; {{$permission->detail}}
            </label>
            </div>
            @endforeach
    </div>

    <div class="text-right">
        <button type="button" data-dismiss="modal" class="btn btn-white pull-left"><i class="fa fa-times" ></i> &nbsp;Exit</button>
        <button type="button" id="add_right" class="btn btn-success pull-right" 
        data-back_path = {{ route('role.index') }}
        ><i class="fa fa-save" >&nbsp;</i>Save role</button>
        <br><br>
    </div>
    </form>


<script type="text/javascript">
// Craete right

$("#add_right").click(function() {

	var selectedPermissions = { 'permissions' : []};
	var value = 1;

    $(".checkbox :checked").each(function() {
    	selectedPermissions['permissions'].push($(this).val());
    });
    
    console.log(selectedPermissions['permissions']);
    
    var permissions = selectedPermissions['permissions'];
	var url = '{{ route("role.update", ["role"=>":role"]) }}';
    var back_url = $(this).data('back_path');
	
    $.ajax({
        type: 'PATCH',
        url: url.replace(":role", $('input[name=id]').val()),
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=title]').val(),
            'slug': $('input[name=slug]').val(),
            'permissions': permissions,
        },
        success: function(data) {
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden').addClass('text-danger');
                    $('#title').addClass('has-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.slug) {
                    $('.error-slug').removeClass('hidden').addClass('text-danger');
                    $('#slug').addClass('has-error');
                    $('.error-slug').text(data.errors.detail);
                }
            } else {
                console.log(data);
                toastr.success('Record deleted successfully!', 'Notification', {"timeOut":1500});
                window.location = back_url;
            }
        },
    });
});
</script>