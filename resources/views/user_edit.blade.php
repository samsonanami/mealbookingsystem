<form role="form">
    @csrf
    <div class="update_form1">
     <div class="form-group">
        <label for="id">User ID<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="id" name="id" disabled>
        <label class="error-id"></label>
    </div>
    <div class="form-group">
        <label for="username">User Name<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="username" name="username">
        <label class="error-username"></label>
    </div>
    <div class="form-group">
        <label for="name">Full Name<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="name" name="name">
        <label class="error-name"></label>
    </div>
    <div class="form-group">
        <label for="name">Payroll<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="payroll" name="payroll">
        <label class="error-payroll"></label>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" id="email" name="email" disabled>
        <label class="error-email"></label>
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" class="form-control" id="phone" name="phone">
        <label class="error-phone"></label>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password">
        <label class="error-password"></label>
    </div>
    
     <div class="form-group">
        <label for="password">Confirm Password</label>
        <input type="password" class="form-control" id="password" name="password_confirmation">
        <label class="error-password"></label>
    </div>
    
     <div class="form-group">
        <label for="role">User Role<i class="text-danger">*</i></label>
        <select id="role" class="form-control" name="role" required>
        @foreach($roles as $id => $role)
            <option value="{{$id}}">{{$role}}</option>
        @endforeach
        </select>
        <label class="error-role"></label>
    </div>
    
    <div class="form-group">
        <label for="active">Active<i class="text-danger">*</i></label>
        <select class="form-control" id="active" name="active">
            <option value=1 selected>Yes</option>
            <option value="0">No</option>
        </select>
        <label class="error-active"></label>
    </div>

    <button type="button" id="exit" data-dismiss="modal" class="btn btn-default pull-left"><i class="fa fa-times" ></i>Exit</button>
    <button type="button" id="add_module" class="btn btn-success pull-right"><i class="fa fa-check" ></i>Submit</button>
    <br><br>
</div>

</form>



<script type="text/javascript">
// Craete module

$("#add_module").click(function() {
	var url = '{{ route("user.update", ["user"=>":user"]) }}';
    $.ajax({
        type: 'PATCH',
        url: url.replace(":user", $('input[name=id]').val()),
        data: {
            '_token': $('[name=_token]').val(),
            'name': $('.update_form1 input[name=name]').val(),
            'username': $('.update_form1 input[name=username]').val(),
            'email': $('.update_form1 input[name=username]').val(),
            'phone': $('.update_form1 input[name=phone]').val(),
            'payroll': $('.update_form1 input[name=payroll]').val(),
            'role': $('.update_form1 select[name=role]').val(),
            'active': $('.update_form1 select[name=active]').val(),
            'password': $('.update_form1 input[name=password]').val(),
        },
        success: function(data) {
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.phone) {
                    $('.error-phone').removeClass('hidden').addClass('text-danger');
                    $('#phone').addClass('has-error');
                    $('.error-phone').text(data.errors.phone);
                }else if (data.errors.role) {
                    $('.error-role').removeClass('hidden').addClass('text-danger');
                    $('#role').addClass('has-error');
                    $('.error-role').text(data.errors.role);
                }else if (data.errors.payroll) {
                    $('.error-payroll').removeClass('hidden').addClass('text-danger');
                    $('#payroll').addClass('has-error');
                    $('.error-payroll').text(data.errors.payroll);
                }else if (data.errors.password) {
                    $('.error-password').removeClass('hidden').addClass('text-danger');
                    $('#password').addClass('has-error');
                    $('.error-password').text(data.errors.password);
                }
                else if (data.errors.active) {
                    $('.error-active').removeClass('hidden').addClass('text-danger');
                    $('#active').addClass('has-error');
                    $('.error-active').text(data.errors.active);
                }
            } else {
                console.log(data);
                location.reload();
            }
        },
    });
});
</script>