<form role="form">
    @csrf
    <div class="form-group">
        <label for="title">Name<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title ...">
        <label class="error-title"></label>
    </div>
    <div class="form-group">
        <label for="slug">Slug<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="slug" name="slug" placeholder="Enter slug ...">
        <label class="error-slug"></label>
    </div>

    <div class="form-group">
        <h4>Group Rights</h4>
        <hr>
            @foreach($rights as $right)
            <div class="checkbox">
            <label  class="form-group" >
                <input type="checkbox" value="{{$right->title}}" > - {{$right->detail}}
            </label>
            </div>
            @endforeach
            <br>
    </div>

	<hr>
     <br>
    <div class="text-right">
        <button type="button" data-dismiss="modal" class="btn btn-white pull-left"><i class="fa fa-times" ></i> &nbsp;Exit</button>
         <button type="button" id="add_right" class="btn btn-success"
        data-back_path = {{ route('role.index') }}><i class="fa fa-save" >&nbsp;</i>Save role</button>
    </div>
</form>


<script type="text/javascript">
// Craete right

$("#add_right").click(function() {

    var selectedPermissions = { 'permissions' : []};

    $(".checkbox :checked").each(function() {
    	selectedPermissions['permissions'].push($(this).val());
    });
	// concert to json
    var permissions = JSON.stringify(selectedPermissions['permissions']);
    
 	console.log(selectedPermissions['permissions']);
    
    var permissions = selectedPermissions['permissions'];
    var back_url = $(this).data('back_path');
	
    $.ajax({
        type: 'POST',
        url: '{{ route('role.store') }}',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=title]').val(),
            'slug': $('input[name=slug]').val(),
            'permissions': permissions,
        },
        success: function(data) {
            console.log(data);
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden').addClass('text-danger');
                    $('#title').addClass('has-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.slug) {
                    $('.error-slug').removeClass('hidden').addClass('text-danger');
                    $('#slug').addClass('has-error');
                    $('.error-slug').text(data.errors.slug);
                }
            } else {
                console.log(data);
                toastr.success('Role saved successfully!', 'Notification', {"timeOut":1500});
                window.location.href = back_url;
            }
        },
    });
});
</script>