@extends('layouts.app')
@section('content')

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                       <a class="text-danger">WEEKLY MENU</a>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="pull-right">
                            <button type="button" class="btn btn-default">Default</button>
                            <button type="button" class="btn btn-primary">Primary</button>
                            <button type="button" class="btn btn-success">Success</button>
                            <button type="button" class="btn btn-info">Info</button>
                            <button type="button" class="btn btn-warning">Warning</button>
                            <button type="button" class="btn btn-danger">Danger</button>
                        </div>
                        <br>
                        <section class="panel">
                            <header class="panel-heading tab-bg-dark-navy-blue ">
                                <ul class="nav nav-tabs">
                                    <li class="">
                                        <a data-toggle="tab" href="#home" aria-expanded="false">MONDAY</a>
                                    </li>
                                    <li class="">
                                        <a data-toggle="tab" href="#about" aria-expanded="false">TEUSDAY</a>
                                    </li>
                                    <li class="">
                                        <a data-toggle="tab" href="#profile" aria-expanded="false">WEDNESDAY</a>
                                    </li>
                                    <li class="active">
                                        <a data-toggle="tab" href="#contact" aria-expanded="true">THURSDAY</a>
                                    </li>
                                    <li class="active">
                                        <a data-toggle="tab" href="#contact" aria-expanded="true">FRIDAY</a>
                                    </li>
                                </ul>
                            </header>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane">
                                        Home
                                    </div>
                                    <div id="about" class="tab-pane">About</div>
                                    <div id="profile" class="tab-pane">Profile</div>
                                    <div id="contact" class="tab-pane active">Contact</div>
                                </div>
                            </div>
                        </section>

                    </div>
                </section>
            </div>
        </div>
        </div>
        </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

@endsection