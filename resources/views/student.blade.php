@extends('layouts.main') @section('content')
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Student Table <span class="tools pull-right"> <a href="javascript:;"
							class="fa fa-chevron-down"></a> <a href="javascript:;"
							class="fa fa-cog"></a> <a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
									<button
									type="button" id="newstudent"
									class="btn btn-success"
									data-toggle="modal"
									data-target="#dynamic-modal"
									data-path="{{ route('student.create') }}"
									>
										<i class="fa fa-plus"></i> Add New
									</button>
									<button
									type="button" id="import"
									class="btn btn-success"
									data-toggle="modal"
									data-target="#dynamic-modal"
									data-path="{{ route('student_create_import') }}"
									>
									<i class="fa fa-download"></i> Import Excel
									</button>
									<button
									type="button" id="export"
									class="btn btn-success"
									data-path="{{ route('student_export') }}"
									>
									<i class="fa fa-file"></i> Export Excel
									</button>
								</div>
								<div class="btn-group pull-left">
									<button class="btn btn-default dropdown-toggle"
										data-toggle="dropdown">
										Tools <i class="fa fa-angle-down"></i>
									</button>

								</div>
							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>School</th>
										<th>Phone</th>
										<th>Address</th>
										<th>Active</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($students as $student)
									<tr class="">
										<td>{{$student->id}}</td>
										<td>{{$student->title}}</td>
										<td>{{$student->school}}</td>
										<td>{{$student->phone}}</td>
										<td>{{$student->address}}</td>
										<td>{{$student->active}}</td>
										<td class="text-right"><a href="#"
											class="show-modal btn btn-default "
											data-id="{{$student->id}}"
											data-title="{{$student->title}}"
											data-phone="{{$student->phone}}"
											data-school="{{$student->school}}" 
											data-phone="{{$student->phone}}"
											data-address="{{$student->address}}"
											data-active="{{$student->active}}"
											data-path="{{ route('student.show',['student'=>$student->id]) }}"
											data-toggle="modal"
											data-target="#dynamic-modal"
											> <i class="fa fa-eye"></i>
										</a> <a href="#" class="edit-modal btn btn-default"
											data-id="{{$student->id}}"
											data-title="{{$student->title}}"
											data-phone="{{$student->phone}}"
											data-school="{{$student->school}}" 
											data-phone="{{$student->phone}}"
											data-address="{{$student->address}}"
											data-active="{{$student->active}}"
											data-path="{{ route('student.edit',['student'=>$student->id]) }}"
											data-toggle="modal"
											data-target="#dynamic-modal"
											>
											<i class="fa fa-pencil"></i>
										</a> <a href="#" class="delete-modal btn btn-danger"
											data-id="{{$student->id}}"
											data-title="{{$student->title}}"
											data-phone="{{$student->phone}}"
											data-school="{{$student->school}}" 
											data-phone="{{$student->phone}}"
											data-address="{{$student->address}}"
											data-active="{{$student->active}}"
											data-phone="{{$student->phone}}" data-detail="{{$student->active}}">
												<i class="fa fa-trash-o"></i>
										</a></td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>

			</div>
		</div>
		<!-- page end-->
	</section>

</section>
<!--main content end-->


<!-- My Dynamic Modal -->
<div id="dynamic-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class=" text-info pull-left">{{env('APP_NAME', 'STL E-Meal')}}</span>
                <button type="button" class="btn btn-danger btn-md pull-right btn-round"
                    data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <!-- Body content will come here -->
            </div>
            <div class="modal-footer">
                {{env('APP_NAME')}}
            </div>
        </div>
    </div>
</div>
<!-- End modal -->


<script type="text/javascript">
//create new
$("#newstudent").click(function () {
    $.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
        },
    });
});


//export 
$("#export").click(function () {
	window.location = $(this).data('path');
});


//import 
$("#import").click(function () {
    $.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
        },
    });
});

//form Delete function
$(document).on('click', '.delete-modal', function() {
   $('#dynamic-modal').addClass('modal-danger');
   $('.modal-body').html('<h4 class="text-danger">Delete this item? ' + $(this).data('id') + ':' + $(this).data('title')+'</h4><input type="hidden" class="id">');
    $('.modal-footer').html('<button type="button" id="calcel" data-dismiss="modal" class="btn btn-default pull-left" >Exit</button> <button type="button" id="delete" class="delete btn btn-danger pull-right" >Delete</button>');
    $('#dynamic-modal').modal('show');
    $('.id').text($(this).data('id'));
});

$('.modal-footer').on('click', '.delete', function() {
	 var url = '{{ route("student.destroy", ["id"=>":id"]) }}';
    $.ajax({
        type: 'DELETE',
        url: url.replace(":id", $('.id').text()),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
            location.reload();
        }
    });
});
//Action Edit/Update
$(document).on('click', '.edit-modal', function() {
	var id = $(this).data('id');
	var title = $(this).data('title');
	var phone = $(this).data('phone');
	var school = $(this).data('school');
	var address = $(this).data('address');
	var active = $(this).data('active');
	
	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
            $('input[name=id]').val(id);
            $('input[name=title]').val(title);
            $('input[name=phone]').val(phone);
            $('input[name=school]').val(school);
            $('textarea[name=address]').val(address);
            $('select[name=active]').val(active);
        },
    });
});

//Show function
$(document).on('click', '.show-modal', function() {
	var id = $(this).data('id');
	var title = $(this).data('title');
	var phone = $(this).data('phone');
	var school = $(this).data('school');
	var address = $(this).data('address');
	var active = $(this).data('active');
	
	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
            $('#id').text(id);
            $('#title').text(title);
            $('#phone').text(phone);
            $('#address').text(address);
            $('#active').text(active);
            $('#school').text(school);
        },
    });
});

</script>
@endsection
