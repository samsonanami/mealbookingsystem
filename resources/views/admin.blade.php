@extends('layouts.app') @section('content')

<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head"></div>
        <!-- END PAGE HEAD -->


        <!-- BEGIN ACTUAL CONTENT -->
        <div id="menus_page" class="hidden portlet light" id="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-home font-green-sharp"></i> <span
                        class="caption-subject font-green-sharp bold uppercase ng-binding">
                        Administration </span> <span class="caption-helper ng-binding">
                        Panel </span>
                </div>
                <div>
                    <button class="btn btn-info btn-sm pull-right" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> Create Task
                    </button>
                </div>
            </div>
            <!-- Echo Data Import Template -->
            <div class="portlet-body paddingT20"></div>
            <!-- Echo Data Import Template -->
        </div>
        <!-- END ACTUAL CONTENT -->

        <!-- BEGIN BOOKING ACTUAL CONTENT -->
        <div class="portlet light" id="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-home font-green-sharp"></i> <span
                        class="caption-subject font-green-sharp bold uppercase ng-binding">
                        Administration </span> <span class="caption-helper ng-binding">
                        Panel </span>
                </div>
               
            </div>
            <!-- Echo Data Import Template -->
            <div class="portlet-body paddingT20">

                <section class="panel">
                <section class="panel">
                <header class="panel-heading">
                    Default Buttons
                     <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                </header>
                <div class="panel-body" style="display: block;">
                    <button type="button" class="btn btn-default">Default</button>
                    <button type="button" class="btn btn-primary">Primary</button>
                    <button type="button" class="btn btn-success">Success</button>
                    <button type="button" class="btn btn-info">Info</button>
                    <button type="button" class="btn btn-warning">Warning</button>
                    <button type="button" class="btn btn-danger">Danger</button>

                </div>
            </section>
            
            
            
            <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> Add User
                    </button>

                    <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> Add Meal
                    </button>

                    <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> Add Menu
                    </button>


                    <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> Book Meal
                    </button>


                    <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> SMS
                    </button>

                    <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> SMS
                    </button>

                    <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> SMS
                    </button>

                    <button class="btn btn-success btn-sm pull-left" style="margin-right: 15px">
                        <i class="fa fa-plus"></i> SMS
                    </button>


                </section>


            </div>
            <!-- Echo Data Import Template -->
        </div>
        <!-- END ACTUAL BOOKING CONTENT -->




    </div>
</div>


@endsection