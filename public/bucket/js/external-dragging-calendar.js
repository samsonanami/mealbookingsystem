var Script = function () {


    /* initialize the external events
     -----------------------------------------------------------------*/

    $('#external-events div.external-event').each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });


    /* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    var calendar =
        $('#calendar').fullCalendar({
        	weekends: false, // will hide Saturdays and Sundays
        	
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            selectable: true,
            selectHelper: false,
            editable: true,
            droppable: true,
            eventLimit: true,

            select: function (start, end, allDay) {
                var check = $.fullCalendar.formatDate(start, 'yyyy-MM-dd');
                var today = $.fullCalendar.formatDate(new Date(), 'yyyy-MM-dd');
                if (check <= today) {
                    // do nothing
                }
                else {
                    var evt = $('#calendar').fullCalendar('clientEvents');
                    var dayEvents = $('#calendar').fullCalendar('clientEvents', function (event) {
                        // return event.start.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0);
                        return event.start.setHours(0, 0, 0, 0) === start.setHours(0, 0, 0, 0);
                    });
                    console.log(dayEvents);
                    $('#create-booking').modal('show');
                    $('.form-horizontal').show();
                    $('.modal-title').text('Meal Booking');
                    $('#header').text(start);

                    var start = $.fullCalendar.formatDate(start, "yyyy-MM-dd");
                    var end = $.fullCalendar.formatDate(end, "yyyy-MM-dd");

                    $('#start-date').text(start);
                    $('#end-date').text(end);
                    $('#start').val(start);
                    $('#end-d').val(start);
                    $("#detail ol > li").remove();

                    // Create events list
                    var itemId = 0;
                    $.each(dayEvents, function (index, value) {
                        if (value.title == 'Booked') {
                            $("#book-meal").hide();
                            itemId = value.id;
                            console.log(itemId);
                            return true;
                        }
                        $("#detail ol").append("<li>" + value.detail + "</li>");
                    });
                    //Exec when price is attached
                    $("#attach-price").click(function () {
                        console.log('Attach price clicked');
                        $.ajax({
                            type: "POST",
                            url: 'booking',
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'title': $('select[name=price]').val(),
                                'start': $('input[name=start]').val(),
                                'end': $('input[name=end-d]').val(),
                                'user_id': $('input[name=user_id]').val(),
                                'owner': $('input[name=owner]').val(),
                                'created_by': $('input[name=created_by]').val(),
                            },
                            success: function () {
                                calendar.fullCalendar('refetchEvents');
                                location.reload();
                            }
                        });
                    });

                    //Exec when booked button is clicked
                    $("#book-meal").click(function () {
                        $.ajax({
                            type: "POST",
                            url: 'booking',
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'title': 'Booked',
                                'start': $('input[name=start]').val(),
                                'end': $('input[name=end-d]').val(),
                                'user_id': $('input[name=user_id]').val(),
                                'owner': $('input[name=owner]').val(),
                                'created_by': $('input[name=created_by]').val(),
                            },
                            success: function () {
                                calendar.fullCalendar('refetchEvents');
                                location.reload();
                            }
                        });
                    });
                    $("#cancel-book-meal").click(function () {
                        console.log('Delete clicked');
                        $.ajax({
                            type: "POST",
                            url: 'booking/' + itemId,
                            data: {
                                '_method': 'DELETE',
                                '_token': $('input[name=_token]').val(),
                                'id': itemId,
                            },
                            success: function () {
                                calendar.fullCalendar('refetchEvents');
                                location.reload();
                            }
                        });
                    });
                }
            },

            // Drag drop between cells
            eventDrop: function (event) {
                var start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd");
                var end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd");
                var title = event.title;
                var id = event.id;
                console.log(id);

                $.ajax({
                    type: 'PUT',
                    url: '/menu/' + id,
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'title': title,
                        'detail': title,
                        'start': start,
                        'end': end,
                    },
                    success: function () {
                        calendar.fullCalendar('refetchEvents');
                    }
                });

            },


            // For dragged and droped items
            drop: function (date, start, end, allDay) {
                var originalEventObject = $(this).data('eventObject');
                var copiedEventObject = $.extend({}, originalEventObject);

                copiedEventObject.start = $.fullCalendar.formatDate(date, "yyyy-MM-dd");
                copiedEventObject.end = $.fullCalendar.formatDate(date, "yyyy-MM-dd");

                $.ajax({
                    url: "menu",
                    type: "POST",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'title': copiedEventObject.title,
                        'detail': copiedEventObject.title,
                        'start': copiedEventObject.start,
                        'end': copiedEventObject.end,
                    },
                    success: function () {
                        calendar.fullCalendar('refetchEvents');
                    }
                })
                if ($('#drop-remove').is(':checked')) {
                    $(this).remove();
                }

            },

            // Delete event

            // eventClick: function (event) {
            //     if (confirm("Are you sure you want to remove item?")) {
            //         var id = event.id;
            //         $.ajax({
            //             type: "DELETE",
            //             url: '/menu/' + id,
            //             data: {
            //                 '_token': $('input[name=_token]').val(),
            //                 id: id,
            //             },
            //             success: function () {
            //                 calendar.fullCalendar('refetchEvents');
            //             }
            //         });
            //     }
            // },
            

            events: 'get_events',
            dayClick: function (date, jsEvent, view) {
                $(this).css('background-color', '#e4e4e4');
            },

        });

}();