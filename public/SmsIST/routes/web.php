<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */

use App\Http\Controllers\HomeController;

Auth::routes();


Route::group([
    'middleware' => 'auth'
], function () {
    Route::get('/change-password', 'HomeController@showChangePasswordForm')->name('change-password');
    Route::post('/password-reset-local', 'HomeController@resetPassword')->name('password-reset-local');

    Route::get('/', 'HomeController@index')->name('default');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('meal', 'Menu\MealController');
    Route::resource('user', 'Admin\UserController');
    Route::resource('menu', 'Menu\MenuController');
    Route::resource('module', 'Admin\ModuleController');
    Route::get('menu_calendar', 'Menu\MenuController@calendar')->name('menu_calendar');
    Route::resource('booking', 'Booking\BookingController');
    Route::resource('booking_admin', 'Booking\AdminBookingController');
    Route::POST('booking-for-user', 'Booking\AdminBookingController@bookForUser')->name('booking-for-user');
    Route::post('pricing', 'Booking\BookingController@pricing');
    Route::post('booking_book', 'Booking\BookingController@book')->name('booking_book');
    Route::resource('food', 'Menu\FoodController');
           
    
    Route::resource('sms', 'SMS\SmsController');
    Route::get('sms_template', 'SMS\SmsController@templates')->name('sms.template');
    Route::get('call', 'SMS\SmsController@call')->name('call');
    Route::get('sms_compose', 'SMS\SmsController@composeSMS')->name('sms_compose');
    Route::post('sms_send', 'SMS\SmsController@sendSMS')->name('sms.send');
    Route::post('sms_send_from_compose', 'SMS\SmsController@sendSMSFromCompose')->name('sms_send_from_compose');
    
    Route::get('sms_sent', 'SMS\SmsController@sentSMS')->name('sms_sent');
    Route::get('sms_inbox', 'SMS\SmsController@inboxSMS')->name('sms_inbox');
    Route::get('sms_draft', 'SMS\SmsController@draftSMS')->name('sms_draft');
    Route::get('sms_trash', 'SMS\SmsController@trashSMS')->name('sms_trash');
    
    Route::resource('student', 'Admin\StudentController');
    Route::resource('settings', 'Admin\SettingController');
    Route::resource('contact-group', 'Admin\ContactGroupController');
    


    

    Route::get('student_create_import', 'Admin\StudentController@createImport')->name('student_create_import');
    Route::post('student_import', 'Admin\StudentController@Import')->name('student_import');
    Route::get('student_export', 'Admin\StudentController@export')->name('student_export');

    Route::get('user_create_import', 'Admin\UserController@createImport')->name('user_create_import');
    Route::post('user_import', 'Admin\UserController@Import')->name('user_import');
    Route::get('user_export', 'Admin\UserController@export')->name('user_export');

    Route::resource('caterer', 'Admin\CatererController');
    Route::get('get_events', 'Menu\MenuController@getEvents');
    Route::resource('visitor', 'Admin\VisitorController');
    Route::resource('right', 'User\RightController');
    Route::resource('user_group', 'User\UserGroupController');
    Route::resource('role', 'User\RoleController');
    Route::get('role-edit/{role}', 'User\RoleController@edit')->name('role-edit');
    Route::post('modify-role/{role}', 'User\RoleController@modify')->name('modify-role');
    Route::post('delete-role/{role}', 'User\RoleController@delete')->name('delete-role');

    Route::resource('report', 'Admin\ReportsController');

    Route::get('booking-report', 'Admin\ReportsController@bookingReport')->name('booking-report');
    Route::get('booking-checklist', 'Admin\ReportsController@bookingChecklist')->name('booking-checklist');

    Route::post('rpt-booking-report', 'Admin\ReportsController@bookingReportLoad')->name('rpt-booking-report');
    Route::post('rpt-booking-checklist', 'Admin\ReportsController@bookingChecklistLoad')->name('rpt-booking-checklist');

    Route::get('/test', 'HomeController@test');
});

/*
 * Custom accessors
 *
 */
Route::get('/gates', 'HomeController@getGates')->name('gates');
Route::get('/modules', 'HomeController@getModules')->name('modules');
Route::get('/permissions', 'HomeController@getPermission')->name('permissions');
Route::get('/roles', 'HomeController@getRoles')->name('roles');

Route::POST('/StlMealBookingReport', 'Admin\ReportsController@rptBooking')->name('StlMealBookingReport');
Route::POST('/rpt_users', 'Admin\ReportsController@rptUsers')->name('rpt_users');




// Route::get('/test', 'HomeController@test');


/**
 * Download Route
 * 
 * Route for templates download
 */

// Route::get('download/{filename}', function ($filename) {
//     // Check if file exists in app/storage/file folder
//     $file_path = storage_path() . '/file/' . $filename;
//     if (file_exists($file_path)) {
//         // Send Download
//         return Response::download($file_path, $filename, [
//             'Content-Length: ' . filesize($file_path)
//         ]);
//     } else {
//         // Error
//         exit('Requested file does not exist on our server!');
//     }
// })->where('filename', '[A-Za-z0-9\-\_\.]+');
