<?php

use Illuminate\Database\Seeder;
use App\Models\User\Rights;
use App\Models\User\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Create admin account role
         *
         * Create admin role and add all rights/permission
         */

        $permissions = [];
        $rightsArray = Rights::all()->pluck('title');
        foreach ($rightsArray as $right){
            $permissions[$right] = true;
        }

        $admin = Role::create([
            'title' => 'Administrator',
            'slug' => 'admin',
            'permissions' => $permissions,
        ]);

        $standard = Role::create([
            'title' => 'Standard',
            'slug' => 'standard',
            'permissions' => [],
        ]);




    }
}
