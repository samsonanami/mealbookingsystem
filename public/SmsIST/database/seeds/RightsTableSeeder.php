<?php

use Illuminate\Database\Seeder;
use App\Models\User\Rights;
use App\Models\Admin\Modules;

class RightsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Caterers CRUD rights
         */
        Rights::create([
            'title' => 'create-caterer',
            'detail' => 'Can CREATE caterer',
        ]);
        Rights::create([
            'title' => 'update-caterer',
            'detail' => 'Can UPDATE caterer',
        ]);
        Rights::create([
            'title' => 'delete-caterer',
            'detail' => 'Can DELETE caterer',
        ]);
        Rights::create([
            'title' => 'view-caterer',
            'detail' => 'Can VIEW caterer',
        ]);
        Rights::create([
            'title' => 'view-admin-dashboard',
            'detail' => 'Can VIEW admin dashboard',
        ]);

        /*
         * Menus CRUD rights
         */
        Rights::create([
            'title' => 'create-menu',
            'detail' => 'Can CREATE menu',
        ]);
        Rights::create([
            'title' => 'update-menu',
            'detail' => 'Can UPDATE menu',
        ]);
        Rights::create([
            'title' => 'delete-menu',
            'detail' => 'Can DELETE menu',
        ]);
        Rights::create([
            'title' => 'view-menu',
            'detail' => 'Can VIEW menu',
        ]);

        /*
         * Meals CRUD rights
         */
        Rights::create([
            'title' => 'create-meal',
            'detail' => 'Can CREATE meal',
        ]);
        Rights::create([
            'title' => 'update-meal',
            'detail' => 'Can UPDATE meal',
        ]);
        Rights::create([
            'title' => 'delete-meal',
            'detail' => 'Can DELETE meal',
        ]);
        Rights::create([
            'title' => 'view-meal',
            'detail' => 'Can VIEW meal',
        ]);

        /*
         * User CRUD rights
         */
        Rights::create([
            'title' => 'create-user',
            'detail' => 'Can CREATE user',
        ]);
        Rights::create([
            'title' => 'update-user',
            'detail' => 'Can UPDATE user',
        ]);
        Rights::create([
            'title' => 'delete-user',
            'detail' => 'Can DELETE user',
        ]);
        Rights::create([
            'title' => 'view-user',
            'detail' => 'Can VIEW user',
        ]);

        /*
         * Booking CRUD rights
         */
        Rights::create([
            'title' => 'create-booking',
            'detail' => 'Can CREATE booking',
        ]);
        Rights::create([
            'title' => 'update-booking',
            'detail' => 'Can UPDATE booking',
        ]);
        Rights::create([
            'title' => 'delete-booking',
            'detail' => 'Can DELETE booking',
        ]);
        Rights::create([
            'title' => 'view-booking',
            'detail' => 'Can VIEW booking',
        ]);

        /*
         * Admin Booking CRUD rights
         */
        Rights::create([
            'title' => 'create-admin-booking',
            'detail' => 'Can CREATE admin booking',
        ]);
        Rights::create([
            'title' => 'update-admin-booking',
            'detail' => 'Can UPDATE admin booking',
        ]);
        Rights::create([
            'title' => 'delete-admin-booking',
            'detail' => 'Can DELETE admin booking',
        ]);
        Rights::create([
            'title' => 'view-admin-booking',
            'detail' => 'Can VIEW admin booking',
        ]);

        /*
         * Role CRUD rights
         */
        Rights::create([
            'title' => 'create-role',
            'detail' => 'Can CREATE role',
        ]);
        Rights::create([
            'title' => 'update-role',
            'detail' => 'Can UPDATE role',
        ]);
        Rights::create([
            'title' => 'delete-role',
            'detail' => 'Can DELETE role',
        ]);
        Rights::create([
            'title' => 'view-role',
            'detail' => 'Can VIEW role',
        ]);

        /*
         * Sms CRUD rights
         */
        Rights::create([
            'title' => 'create-sms',
            'detail' => 'Can CREATE sms',
        ]);
        Rights::create([
            'title' => 'update-sms',
            'detail' => 'Can UPDATE sms',
        ]);
        Rights::create([
            'title' => 'delete-sms',
            'detail' => 'Can DELETE sms',
        ]);
        Rights::create([
            'title' => 'view-sms',
            'detail' => 'Can VIEW sms',
        ]);
        Rights::create([
            'title' => 'send-sms',
            'detail' => 'Can SEND sms',
        ]);


        /*
         * Rights CRUD rights
         */
        Rights::create([
            'title' => 'create-rights',
            'detail' => 'Can CREATE rights',
        ]);
        Rights::create([
            'title' => 'update-rights',
            'detail' => 'Can UPDATE rights',
        ]);
        Rights::create([
            'title' => 'delete-rights',
            'detail' => 'Can DELETE rights',
        ]);
        Rights::create([
            'title' => 'view-rights',
            'detail' => 'Can VIEW rights',
        ]);

        /*
         * Student CRUD rights
         */
        Rights::create([
            'title' => 'create-student',
            'detail' => 'Can CREATE student',
        ]);
        Rights::create([
            'title' => 'update-student',
            'detail' => 'Can UPDATE student',
        ]);
        Rights::create([
            'title' => 'delete-student',
            'detail' => 'Can DELETE student',
        ]);
        Rights::create([
            'title' => 'view-student',
            'detail' => 'Can VIEW student',
        ]);

        /*
         * Visitor CRUD rights
         */
        Rights::create([
            'title' => 'create-visitor',
            'detail' => 'Can CREATE visitor',
        ]);
        Rights::create([
            'title' => 'update-visitor',
            'detail' => 'Can UPDATE visitor',
        ]);
        Rights::create([
            'title' => 'delete-visitor',
            'detail' => 'Can DELETE visitor',
        ]);
        Rights::create([
            'title' => 'view-visitor',
            'detail' => 'Can VIEW visitor',
        ]);

        /*
         * Module CRUD rights
         */
        Rights::create([
            'title' => 'create-module',
            'detail' => 'Can CREATE module',
        ]);
        Rights::create([
            'title' => 'update-module',
            'detail' => 'Can UPDATE module',
        ]);
        Rights::create([
            'title' => 'delete-module',
            'detail' => 'Can DELETE module',
        ]);
        Rights::create([
            'title' => 'view-module',
            'detail' => 'Can VIEW module',
        ]);

        /*
         * Create modules view permisions
         *
         */
        $modules = Modules::all();
        foreach ($modules as $module){
            $permKey = 'view-'.$module->title;
            Rights::create([
                'title' => $permKey,
                'detail' => 'Can VIEW '. $module->title,
            ]);
        }


    }
}
