<?php
use Illuminate\Database\Seeder;
use App\Models\Admin\Modules;

class ModuleTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1 Create Dashboard Link
        Modules::create([
            'title' => 'DASHBOARD',
            'color' => '#FFF',
            'detail' => 'Main Panel',
            'order' => '999',
            'link' => 'home',
            'icon' => 'fa fa-dashboard',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1,
        ]);
         
        //2 Create SMS Link
        Modules::create([
            'title' => 'MESSAGES',
            'color' => '#FFF',
            'detail' => 'SMS Panel',
            'order' => '999',
            'link' => 'default',
            'icon' => 'fa fa-envelope',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);

        //3 Create SMS Reminders Link
        Modules::create([
            'title' => 'Messages',
            'color' => '#FFF',
            'detail' => 'Reminders Panel',
            'order' => '999',
            'link' => 'sms.index',
            'icon' => 'fa fa-envelope-o',
            'id_parent' => 2,
            'is_root' => 0,
            'active' => 1
        ]);

        //4
        Modules::create([
            'title' => 'Templates',
            'color' => '#FFF',
            'detail' => 'Reminders Panel',
            'order' => '999',
            'link' => 'sms.template',
            'icon' => 'fa fa-file-text-o',
            'id_parent' => 2,
            'is_root' => 0,
            'active' => 1
        ]);
        
        //5 
        Modules::create([
            'title' => 'Settings',
            'color' => '#FFF',
            'detail' => 'Administration Panel',
            'order' => '999',
            'link' => 'default',
            'icon' => 'fa fa-cogs',
            'id_parent' => 2,
            'is_root' => 0,
            'active' => 1
        ]);

        //6
        Modules::create([
            'title' => 'PHONE BOOK',
            'color' => '#FFF',
            'detail' => 'PHONE BOOK Panel',
            'order' => '999',
            'link' => 'settings.index',
            'icon' => 'fa fa-mobile-phone',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);
        //7
        Modules::create([
            'title' => 'Contacts',
            'color' => '#FFF',
            'detail' => 'Contacts Panel',
            'order' => '999',
            'link' => 'student.index',
            'icon' => 'fa fa-phone',
            'id_parent' => 6,
            'is_root' => 0,
            'active' => 1
        ]);
        //8
        Modules::create([
            'title' => 'Contact Groups',
            'color' => '#FFF',
            'detail' => 'Receivers Groups Panel',
            'order' => '999',
            'link' => 'contact-group.index',
            'icon' => 'fa fa-users',
            'id_parent' => 6,
            'is_root' => 0,
            'active' => 1
        ]);
         
         
        //9
        Modules::create([
            'title' => 'USERS',
            'color' => '#FFF',
            'detail' => 'PHONE BOOK Panel',
            'order' => '999',
            'link' => 'default',
            'icon' => 'fa fa-users',
            'id_parent' => 0,
            'is_root' => 1,
            'active' => 1
        ]);
        //10
        Modules::create([
            'title' => 'Users',
            'color' => '#FFF',
            'detail' => 'Users Panel',
            'order' => '999',
            'link' => 'user.index',
            'icon' => '',
            'id_parent' => 9,
            'is_root' => 0,
            'active' => 1
        ]);
        //11
        Modules::create([
            'title' => 'Roles',
            'color' => '#FFF',
            'detail' => 'User Roles Panel',
            'order' => '999',
            'link' => 'role.index',
            'icon' => '',
            'id_parent' => 9,
            'is_root' => 0,
            'active' => 1
        ]);
        //12
        Modules::create([
            'title' => 'Permissions',
            'color' => '#FFF',
            'detail' => 'User Group Panel',
            'order' => '999',
            'link' => 'right.index',
            'icon' => '',
            'id_parent' => 9,
            'is_root' => 0,
            'active' => 1
        ]);
        
        
    }
}
