<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\User\Role;
use App\Models\User\Rights;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create admin account & give admin rights
        User::insert([
            'name' => 'admin',
            'username' => 'admin',
            'payroll' => 'STL000001',
            'email' => 'samsonanami@gmail.com',
            'password' => bcrypt('secret'),
            'phone'=>'0717084613',
            'active'=>1
        ]);
    }
}
