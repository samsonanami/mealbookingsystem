<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',20);
            $table->string('number', 255)->nullable();
            $table->string('cost', 255)->nullable();
            $table->string('messageId', 255)->nullable();
            $table->string('Message', 255)->nullable();
            $table->string('detail', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('statusCode', 255)->nullable();
            $table->integer('inbox')->default(0);
            $table->integer('sent')->default(0);
            $table->integer('failed')->default(0);
            $table->integer('trash')->default(0);
            $table->integer('draft')->default(0);
            $table->integer('template')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
