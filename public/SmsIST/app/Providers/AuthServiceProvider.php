<?php
namespace App\Providers;

use App\Models\Admin\Caterers;
use App\Models\Admin\Modules;
use App\Policies\CatererPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Menu\Meal;
use App\Policies\MealPolicy;
use App\Menu\Menu;
use App\Policies\MenuPolicy;
use App\User;
use App\Policies\UserPolicy;
use App\Models\Booking\Booking;
use App\Policies\BookingPolicy;
use App\Models\Booking\AdminBooking;
use App\Policies\AdminBookingPolicy;
use App\Models\User\Role;
use App\Policies\RolePolicy;
use App\Models\SMS\Sms;
use App\Policies\SmsPolicy;
use App\Models\User\Rights;
use App\Policies\RightsPolicy;
use App\Models\Admin\Student;
use App\Models\Admin\Visitors;
use App\Policies\VisitorsPolicy;
use App\Policies\ModulesPolicy;
use App\Policies\StudentPolicy;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        // Caterers::class => CatererPolicy::class,
        // Meal::class => MealPolicy::class,
        // Menu::class => MenuPolicy::class,
        // User::class => UserPolicy::class,
        // Booking::class => BookingPolicy::class,
        // AdminBooking::class => AdminBookingPolicy::class,
        // Role::class => RolePolicy::class,
        // Sms::class => SmsPolicy::class,
        // Rights::class => RightsPolicy::class,
        Student::class => StudentPolicy::class,
        // Visitors::class => VisitorsPolicy::class,
        // Modules::class => ModulesPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerCustomPolicies();
        $this->registerModulesPolicy();
        // Gate::define('create-caterer', 'App\Policies\CatererPolicy@create');
    }

    /*
     * Admin policy Gatesfor later use
     */
    public function registerAdminPolicies()
    {
        Gate::before(function ($user, $ability) {
            if ($user->isSuperAdmin()) {
                return true;
            }
        });
    }

    /*
     * Caterer Gates
     *
     */
    public function registerCustomPolicies()
    {

        // Special gates
        Gate::define('view-admin-dashboard', function ($user) {
            return $user->hasAccess([
                'view-admin-dashboard'
            ]);
        });

        // Caterers gates
        Gate::define('view-caterer', 'CatererPolicy@index');

        GAte::define('exec-custom', 'CatererPolicy@customFunct');

        Gate::define('create-caterer', function ($user) {
            return $user->hasAccess([
                'create-caterer'
            ]);
        });
        Gate::define('update-caterer', function ($user) {
            return $user->hasAccess([
                'update-caterer'
            ]);
        });
        Gate::define('publish-caterer', function ($user) {
            return $user->hasAccess([
                'publish-caterer'
            ]);
        });

        // Meals gates
        Gate::define('create-meal', function ($user) {
            return $user->hasAccess([
                'create-meal'
            ]);
        });
        Gate::define('update-meal', function ($user) {
            return $user->hasAccess([
                'update-meal'
            ]);
        });
        Gate::define('delete-meal', function ($user) {
            return $user->hasAccess([
                'delete-meal'
            ]);
        });

        // Menus gates
        Gate::define('create-menu', function ($user) {
            return $user->hasAccess([
                'create-menu'
            ]);
        });
        Gate::define('update-menu', function ($user) {
            return $user->hasAccess([
                'update-menu'
            ]);
        });
        Gate::define('delete-menu', function ($user) {
            return $user->hasAccess([
                'delete-menu'
            ]);
        });

        // User gates
        Gate::define('create-user', function ($user) {
            return $user->hasAccess([
                'create-user'
            ]);
        });
        Gate::define('update-user', function ($user) {
            return $user->hasAccess([
                'update-user'
            ]);
        });
        Gate::define('delete-user', function ($user) {
            return $user->hasAccess([
                'delete-user'
            ]);
        });

        // Other gates
        Gate::define('view-admin-dashboard', function ($user) {
            return $user->hasAccess([
                'view-admin-dashboard'
            ]);
        });

        // Booking gates
        Gate::define('create-booking', function ($user) {
            return $user->hasAccess([
                'create-booking'
            ]);
        });
        Gate::define('update-booking', function ($user) {
            return $user->hasAccess([
                'update-booking'
            ]);
        });
        Gate::define('delete-booking', function ($user) {
            return $user->hasAccess([
                'delete-booking'
            ]);
        });

        // Booking gates
        Gate::define('create-booking', function ($user) {
            return $user->hasAccess([
                'create-booking'
            ]);
        });
        Gate::define('update-booking', function ($user) {
            return $user->hasAccess([
                'update-booking'
            ]);
        });
        Gate::define('delete-booking', function ($user) {
            return $user->hasAccess([
                'delete-booking'
            ]);
        });

        // Admin Booking gates
        Gate::define('create-admin-booking', function ($user) {
            return $user->hasAccess([
                'create-admin-booking'
            ]);
        });
        Gate::define('update-admin-booking', function ($user) {
            return $user->hasAccess([
                'update-admin-booking'
            ]);
        });
        Gate::define('delete-admin-booking', function ($user) {
            return $user->hasAccess([
                'delete-admin-booking'
            ]);
        });

        // Sms gates
        Gate::define('create-sms', function ($user) {
            return $user->hasAccess([
                'create-sms'
            ]);
        });
        Gate::define('update-sms', function ($user) {
            return $user->hasAccess([
                'update-sms'
            ]);
        });
        Gate::define('delete-sms', function ($user) {
            return $user->hasAccess([
                'delete-sms'
            ]);
        });
        Gate::define('send-sms', function ($user) {
            return $user->hasAccess([
                'send-sms'
            ]);
        });

        // Rights gates
        Gate::define('create-right', function ($user) {
            return $user->hasAccess([
                'create-right'
            ]);
        });
        Gate::define('update-right', function ($user) {
            return $user->hasAccess([
                'update-right'
            ]);
        });
        Gate::define('delete-right', function ($user) {
            return $user->hasAccess([
                'delete-right'
            ]);
        });

        // Student gates
        Gate::define('create-student', function ($user) {
            return $user->hasAccess([
                'create-student'
            ]);
        });
        Gate::define('update-right', function ($user) {
            return $user->hasAccess([
                'update-student'
            ]);
        });
        Gate::define('delete-student', function ($user) {
            return $user->hasAccess([
                'delete-student'
            ]);
        });

        // Visitor gates
        Gate::define('create-visitor', function ($user) {
            return $user->hasAccess([
                'create-visitor'
            ]);
        });
        Gate::define('update-visitor', function ($user) {
            return $user->hasAccess([
                'update-visitor'
            ]);
        });
        Gate::define('delete-visitor', function ($user) {
            return $user->hasAccess([
                'delete-visitor'
            ]);
        });

        // Module gates
        Gate::define('create-module', function ($user) {
            return $user->hasAccess([
                'create-module'
            ]);
        });
        Gate::define('update-module', function ($user) {
            return $user->hasAccess([
                'update-module'
            ]);
        });
        Gate::define('delete-module', function ($user) {
            return $user->hasAccess([
                'delete-module'
            ]);
        });

        // Role  gates
        Gate::define('create-role', function ($user) {
            return $user->hasAccess([
                'create-role'
            ]);
        });
        Gate::define('update-role', function ($user) {
            return $user->hasAccess([
                'update-role'
            ]);
        });
        Gate::define('delete-role', function ($user) {
            return $user->hasAccess([
                'delete-role'
            ]);
        });
    }

    /*
     *
     * create modules view gates
     *
     */
    public function registerModulesPolicy()
    {
        $modules = Modules::all();
        foreach ($modules as $module) {
            $permKey = 'view-' . $module->title;
            Gate::define($permKey, function ($user, $permKey) {
                return $user->hasAccess([
                    $permKey
                ]);
            });
        }
    }
}
