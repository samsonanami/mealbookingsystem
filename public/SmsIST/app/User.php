<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Booking\Booking;
use App\Models\User\Rights;
use App\Models\User\UserGroups;
use App\Models\User\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'payroll',
        'email',
        'password',
        'active',
        'phone',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public $rules = [
        'name' => 'string|required',
        'username' => 'string|required',
        'email' => 'required|email|unique:users',
        'payroll' => 'required|string|unique:users',
        'password' => 'required|min:6',
        'active' => 'integer',
        'phone' => 'string|nullable',
    ];

    public $update_rules = [
        'name' => 'required|string',
        'username' => 'required|string',
        'email' => 'required|email',
        'payroll' => 'required',
        'password' => '',
        'active' => '',
        'phone' => 'nullable|string',
    ];

    public function scopeActive($query, $value)
    {
        return $query->where('active', $value);
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function rights()
    {
        return $this->belongsToMany(Rights::class, 'user_rights', 'right_id', 'user_id');
    }

    public function group()
    {
        return $this->hasOne(UserGroups::class, 'id', 'role');
    }

    /*
     * User permissions
     *
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users');
    }

    /**
     * Checks if User has access to $permissions.
     */
    public function hasAccess(array $permissions): bool
    {
        // check if the permission is available in any role
        foreach ($this->roles as $role) {
            //             print_r($role->hasAccess($permissions));die();
            if ($role->hasAccess($permissions)) {
                return true;
            }
        }
        return false;
    }

    /*
     * Checks if the user belongs to role
     *
     */
    public function inRole(string $roleSlug)
    {
        return $this->roles()
            ->where('slug', $roleSlug)
            ->count() == 1;
    }
}
