<?php

namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Menu\Meal;
use App\Models\Admin\Modules;

class MealController extends Controller
{
    /*
     * Authorizing resourse
     */
    public function __construct(){
        // $this->authorizeResource(Meal::class, 'meal');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)->active(1)->get();
        $meals = Meal::all();
        return view('meals', [
            'meals' => $meals,
            'modules'=> $modules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Meal();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
            else {
                $resource = Meal::create($request->all());
                toastr()->success('Meal saved successfully!', 'Notification', [
                    'timeOut' => 3000
                ]);
                return response()->json($resource);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Meal $meal)
    {
        $model = new Meal(); 
        $validation = Validator::make($request->all(), $model->update_rules);
        $resource = $meal->update([
            'name' => $request->name,
            'detail' => $request->detail,
            'price' => $request->price
        ]);
        toastr()->success('Record Updated successfully!', 'Success', [
            'timeOut' => 3000
        ]);
        
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meal $meal)
    {
        $meal->delete();
        toastr('Record deleted!','success','sucess');
    }
}
