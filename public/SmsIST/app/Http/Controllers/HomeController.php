<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Models\Admin\Modules;
use App\Models\Admin\Route;
use App\Models\Admin\UserRights;
use App\Models\Booking\Booking;
use App\Models\User\Rights;
use App\Models\User\UserGroups;
use App\Menu\Meal;
use App\Menu\Menu;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        //set session
        $this->getUser();
    }


    public function test(){
       return  Session::get('userData');
    }

    public function getUser(){
        $user = Auth::user();
        Session::put('userData',  $user);
        return true;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
       
        $labels = [
            'label-info',
            'label label-primary',
            'label-success',
            'label-info',
            'label tar',
            'label orange',
            'label-warning',
            'label-danger',
            'label-info'
        ];
        return view('dashboard', [
            // 'menus' => $menus,
            'labels' => $labels,
            // 'meals' => $meals,
            'modules' => $modules,
            // 'user' => $user,
            // 'users' => $users,
        ]);
    }

    // admin login
    public function admin()
    {
        $modules = $this->getModules();

        $user_count = User::all()->count();
        $meal_count = Meal::all()->count();
        $booking_count = Booking::all()->count();
        $food_count = Menu::all()->count();
        $routes = Route::all();
        $user_roles = UserRights::all();

        $user_count = User::all()->count();
        $meal_count = Meal::all()->count();
        $booking_count = Booking::all()->count();
        $food_count = Menu::all()->count();

        return view('dashboard-admin', [
            'user_count' => $user_count,
            'meal_count' => $meal_count,
            'booking_count' => $booking_count,
            'food_count' => $food_count,
            'modules' => $modules,
            'user_roles' => $user_roles,
            'routes' => $routes,

            'user_count' => $user_count,
            'meal_count' => $meal_count,
            'booking_count' => $booking_count,
            'food_count' => $food_count
        ]);
    }

    public function getGroupRights($id)
    {
        $user_group = UserGroups::with('rights')->find($id);
        $user_rights = array(
            $user_group->rights
        );
        $rights = [];
        // create right list
        foreach ($user_rights[0] as $item) {
            $rights[] = $item['title'];
        }
        return $rights;
    }

    /*
     * For listing my Modules
     */
    public static function getModules()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->orderBy('id')
            ->get();
        return $modules;
    }

    /*
     * For listing my Gates
     */
    public function getGates()
    {
        $gates = Gate::abilities();
        return json_encode($gates);
    }

    /*
     * For listing my paermission
     */
    public function getPermission()
    {
        $permissions = [];
        $rightsArray = Rights::all()->pluck('title');
        foreach ($rightsArray as $right) {
            $permissions[$right] = true;
        }
        $rights['all'] = $permissions;
        return json_encode($rights);
    }

    public function getRoles()
    {
        $users = User::with('roles')->get();
        return Response::json($users);
    }

    public function resetPassword(Request $request)
    {
        $request->validate($this->passResetRules(), $this->validationErrorMessages());
        $user = User::findOrFail($request->id);
        $password = $request->password;
        $user->password = Hash::make($password);
        $user->password_change_at = now();
        $user->setRememberToken(Str::random(60));
        
        \App\Models\PasswordReset::create([
            'email' => $user->email,
            'token' => Hash::make(Str::random(60)),
            'created_at' => now(),
        ]);
        
        if($user->save()){
            return $user;
        }
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function passResetRules()
    {
        return [
            'id' => 'required',
//             'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:4'
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }

    public function showChangePasswordForm()
    {
        $user = Auth::user();
        return view('reset')->with([
            'email' => $user->email,
            'id' => $user->id,
        ]);
    }

    
    public function users(){
        $users = User::all();
        return json_encode($users);
    }
}
