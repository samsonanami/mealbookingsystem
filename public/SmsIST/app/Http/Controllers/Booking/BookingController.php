<?php
namespace App\Http\Controllers\Booking;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use App\Models\Booking\Booking;
use App\Menu\Meal;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    
    /*
     * Authorizing resourse
     */
    public function __construct(){
        // $this->authorizeResource(Booking::class, 'booking');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
        ->active(1)
        ->get();
        $user = Auth::user();
        $user_id = Auth::user()->id;
        $user_bookings = Booking::all();
        $bookings = Booking::with('user', 'owners', 'student', 'visitor', 'caterer')->get();
        $meals = Meal::all();
        return view('book', [
            'bookings' => $bookings,
            'meals' => $meals,
            'user_bookings' => $user_bookings,
            'user' => $user,
            'modules'=> $modules,
        ]);
    }

    public function myBooking()
    {
        $modules = Modules::with('children')->root(1)
        ->active(1)
        ->get();
        $user = Auth::user();
        $user_id = Auth::user()->id;
        $user_bookings = Booking::where([
            'owner' => $user_id
        ])->get();
        $bookings = Booking::all();
        $meals = Meal::all();
        return view('book', [
            'bookings' => $bookings,
            'meals' => $meals,
            'user_bookings' => $user_bookings,
            'user' => $user,
            'modules' => $modules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Booking();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        } else {
            $resource = Booking::create($request->all());
            toastr()->success('Record saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        $model = new Booking();
        $validation = Validator::make($request->all(), $model->rules);
        $resource = $booking->update([
            'date' => $request->date,
            'user_id' => $request->user_id,
            'owner' => $request->owner,
            'created_by' => $request->created_by
        ]);
        toastr()->success('Record Updated successfully!', 'Success', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();
        toastr()->success('Record deleted!', 'Success', [
            'timeOut' => 1500
        ]);
        return response()->json($booking);
    }

    public function book(Request $request){
        $model = new Booking();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            toastr()->error('Transacton completed successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        } else {
            // check multiple days
            if ($request->start !== $request->end) {
                $start = date_create($request->start);
                $end = date_create($request->end);
                $interval = $end->diff($start);
                $interval_days =  $interval->format('%a');

                for ($i = 0; $i < $interval_days+1; $i ++) {
                    $request->end = $request->start;
                    // do the insert
                    $resource = Booking::create([
                        'name' => $request->name,
                        'start' => $request->start,
                        'end' => $request->end,
                        'user_id' => $request->user_id,
                        'owner' => $request->owner,
                        'created_by' => $request->created_by
                    ]);
                    // reset start
                    $request->start = date('Y-m-d', strtotime('+1 day', strtotime($request->start)));
                }
                return response()->json($request->end);
            } else {
                $resource = Booking::create($request->all());
            }
        }
        
        toastr()->success('Transacton completed successfully!', 'Notification', [
            'timeOut' => 3000
        ]);

        return response()->json($resource);
    }

    // public function bookAdmin(Request $request){
    //
    //     $model = new Booking();
    //     $validator = Validator::make($request->all(), $model->rules);
    //     if ($validator->fails()) {
    //         return Response::json(array(
    //             'errors' => $validator->getMessageBag()->toarray()
    //         ));
    //     } else {
    //
    //       //Get group members
    //       if($request->group==0){
    //         return Response::json(array(
    //             'errors' => 'Group is required'
    //         ));
    //       }elseif ($request->group==1) {
    //         $owners = Caterers::active(1)->get('')
    //       }
    //       value="1">Caterers</option>
    //       <option label="" value="2">Users</option>
    //       <option label="" value="3">Visitors</option>
    //       <option label="" value="4">Student</option>
    //       foreach ($request->group as $key => $value) {
    //         // code...
    //       }
    //
    //         // check multiple days
    //         if ($request->start !== $request->end) {
    //             $start = date_create($request->start);
    //             $end = date_create($request->end);
    //             $interval = $end->diff($start);
    //             $interval_days =  $interval->format('%a');
    //
    //             for ($i = 0; $i < $interval_days; $i ++) {
    //                 $request->end = $request->start;
    //                 // do the insert
    //                 $resource = Booking::create([
    //                     'name' => $request->name,
    //                     'detail' => $request->detail,
    //                     'start' => $request->start,
    //                     'end' => $request->end,
    //                     'user_id' => $request->user_id,
    //                     'owner' => $request->owner,
    //                     'created_by' => $request->created_by
    //                 ]);
    //                 // reset start
    //                 $request->start = date('Y-m-d', strtotime('+1 day', strtotime($request->start)));
    //             }
    //             return response()->json($request->end);
    //         } else {
    //             $resource = Booking::create($request->all());
    //         }
    //     }
    //     toastr()->success('Record saved successfully!', 'Notification', [
    //         'timeOut' => 3000
    //     ]);
    //     return response()->json($resource);
    // }

}
