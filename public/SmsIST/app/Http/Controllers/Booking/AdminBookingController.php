<?php

namespace App\Http\Controllers\Booking;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Http\Controllers\Controller;
use App\Models\Admin\Caterers;
use App\Models\Admin\Modules;
use App\Models\Admin\Student;
use App\Models\Admin\Visitors;
use App\Models\Booking\Booking;
use App\Menu\Meal;
use Illuminate\Support\Facades\Auth;
use App\Models\Booking\AdminBooking;

class AdminBookingController extends Controller
{

    /*
     * Authorizing resourse
     */
    public function __construct()
    {
        // $this->authorizeResource(AdminBooking::class, 'admin-booking');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
        $user = Auth::user();
        $user_id = Auth::user()->id;
        $user_bookings = AdminBooking::where([
            'owner' => $user_id
        ])->get();
        $bookings = AdminBooking::with('user', 'owner', 'student', 'visitor', 'caterer')->get();
        $meals = Meal::all();
        return view('book_admin', [
            'bookings' => $bookings,
            'meals' => $meals,
            'user_bookings' => $user_bookings,
            'user' => $user,
            'modules' => $modules
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new AdminBooking();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        } else {
            $group = $request->group;
            switch ($group) {
                case 1:
                    $owners = Caterers::active(1)->get();
                    break;
                case 2:
                    $owners = User::active(1)->get();
                    break;
                case 3:
                    $owners = Visitors::active(1)->get();
                    break;
                case 4:
                    $owners = Student::active(1)->get();
                    break;
                default:
                    return response()->json([
                        'errors' => ['group', 'You must select atleast one recepient']
                    ]);
            }
            // insert bookings
            foreach ($owners as $owner) {
                if ($request->group == 2) {
                    $userName = $owner->name;
                } else {
                    $userName = $owner->title;
                }
                $resource = Booking::create([
                    'title' => $userName,
                    'user_id' => $request->user_id,
                    'start' => $request->start,
                    'end' => $request->end,
                    'owner' => $owner->id,
                    'created_by' => $request->created_by,
                    'group' => $request->group
                ]);
            }
            toastr()->success('Record saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }
    public function bookForUser(Request $request)
    {
        $model = new AdminBooking();
        $validator = Validator::make($request->all(), $model->rulesForSingleUser);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        } else {
            $resource = Booking::create([
                'title' => 'Booked',
                'user_id' => $request->user_id,
                'start' => $request->start,
                'end' => $request->end,
                'owner' => $request->user_id,
                'created_by' => $request->created_by,
                'group' => $request->group
            ]);

            toastr()->success('Record saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminBooking $booking)
    {
        $model = new AdminBooking();
        $validation = Validator::make($request->all(), $model->rules);
        $resource = $booking->update([
            'date' => $request->date,
            'user_id' => $request->user_id,
            'owner' => $request->owner,
            'created_by' => $request->created_by
        ]);
        toastr()->success('Record Updated successfully!', 'Success', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminBooking $booking)
    {
        $booking->delete();
        toastr()->success('Record deleted!', 'Success', [
            'timeOut' => 1500
        ]);
        return response()->json($booking);
    }

    public function book(Request $request)
    {
        $model = new AdminBooking();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        } else {
            // check multiple days
            if ($request->start !== $request->end) {
                $start = date_create($request->start);
                $end = date_create($request->end);
                $interval = $end->diff($start);
                $interval_days = $interval->format('%a');

                for ($i = 0; $i < $interval_days; $i++) {
                    $request->end = $request->start;
                    // do the insert
                    $resource = AdminBooking::create([
                        'name' => $request->name,
                        'detail' => $request->detail,
                        'start' => $request->start,
                        'end' => $request->end,
                        'user_id' => $request->user_id,
                        'owner' => $request->owner,
                        'created_by' => $request->created_by
                    ]);
                    // reset start
                    $request->start = date('Y-m-d', strtotime('+1 day', strtotime($request->start)));
                }
                return response()->json($request->end);
            } else {
                $resource = AdminBooking::create($request->all());
            }
        }
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }
}
