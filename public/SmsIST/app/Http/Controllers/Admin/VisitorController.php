<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Admin\Caterers;
use App\Models\Admin\Modules;
use App\Models\Admin\Visitors;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visitors = Visitors::all();
        $modules = Modules::with('children')->root(1)->active(1)->get();
        return view('visitor',[
            'visitors'=>$visitors,
            'modules'=> $modules,
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('visitor_create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Visitors();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
            else {
                $resource = Visitors::create($request->all());
            }
            toastr()->success('Visitor created successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('visitor_show');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('visitor_edit');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitors $visitor)
    {
        $model = new Visitors();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
            else {
                $resource = $visitor->update($request->all());
            }
            toastr()->success('Record saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitors $visitor)
    {
        $visitor->delete();
        toastr('Record deleted!','success','sucess');
    }
}
