<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\ContactGroup;
use App\Models\Admin\Modules;
use App\Models\Admin\Student;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ContactGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact_groups = ContactGroup::all();
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
            
        return view('contact_group', [
            'contact_groups' => $contact_groups,
            'modules' => $modules
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('contact_group_create', [
            'students' => $students,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new ContactGroup();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource = ContactGroup::create($request->all());
        }

        //attach relation
        $resource->students()->sync($request->students);
        toastr()->success('Group created successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('contact_group_show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Student::all();
        $item = ContactGroup::findOrFail($id);
        return view('contact_group_edit', [
            'students' => $students,
            'item'=> $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $resource = ContactGroup::findOrFail($id);
        $model = new ContactGroup();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource->update($request->all());
        }
        //attach relation
        $resource->students()->sync($request->students);

        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact_group = ContactGroup::findOrFail($id);
        $contact_group->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }
}
