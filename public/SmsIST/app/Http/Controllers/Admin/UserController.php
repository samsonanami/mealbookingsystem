<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use App\Models\User\Role;
use App\User;
use App\Imports\UsersImport;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{

    /*
     * Authorizing resourse
     */
    public function __construct()
    {
        //         $this->authorizeResource(User::class, 'user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->get();
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
        foreach ($modules as $module) {
            $module['permKey'] = 'view-' . $module->title;
        }

        return view('users', [
            'users' => $users,
            'modules' => $modules
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::orderBy('title')->pluck('title', 'id');
        return view('user_create', [
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new User();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $request->password = Hash::make($request->password);
            $resource = User::create([
                'name' => $request->name,
                'payroll' => $request->payroll,
                'email' => $request->email,
                'phone' => $request->phone,
                'username' => $request->username,
                'active' => $request->active,
                'password' => $request->password
            ]);
            $resource->roles()->attach($request->role);
        }
        toastr()->success('User saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user_show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::orderBy('title')->pluck('title', 'id');
        return view('user_edit', [
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $model = new User();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        } else {

            if (isset($request->password)) {
                $request->password = Hash::make($request->password);
                $user->update([
                    'name' => $request->name,
                    'username' => $request->username,
                    'phone' => $request->phone,
                    'payroll' => $request->payroll,
                    'active' => $request->active,
                    'password' => $request->password,
                    'password_changed_at' => null,
                ]);
            } else {
                $user->update([
                    'name' => $request->name,
                    'username' => $request->username,
                    'phone' => $request->phone,
                    'payroll' => $request->payroll,
                    'active' => $request->active,
                    'password_changed_at' => null,
                ]);
            }



            $role = $user->roles()
                ->where('role_id', $request->role)
                ->first();

            if (is_null($role)) {
                $user->roles()->attach($request->role);
            } else {
                $user->roles()->detach();
                $user->roles()->attach($request->role);
            }
        }
        toastr()->success('User saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }


    public function createImport()
    {
        return view('user_import');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'require|mimes:xml,xlsx'
        ]);

        $data = Excel::import(new UsersImport, $request->file('import_file'));

        if ($data) {
            return 'success';
        } else {
            return Response::json(array(
                'errors' => $data
            ));
        }
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'mealbs_users_export.xlsx');
    }
}
