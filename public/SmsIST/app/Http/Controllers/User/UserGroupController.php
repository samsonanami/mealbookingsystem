<?php
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Models\Admin\Modules;
use App\Models\User\Rights;
use Illuminate\Support\Facades\Validator;
use App\Models\User\UserGroups;
use App\Models\User\GroupRights;
use App\Http\Controllers\HomeController;

class UserGroupController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
        ->active(1)
        ->get();
        $resources = UserGroups::all();
        return view('user_group', [
            'resources' => $resources,
            'modules' => $modules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Rights::all();
        return view('user_group_create', [
            'rights' => $rights
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new UserGroups();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        $resource = UserGroups::create($request->all());
        $array = $request->rights;
        // insert rights
        for($i=1; $i<count($array); $i++) {
                $group_right = GroupRights::create([
                    'id_group' => $resource->id,
                    'id_right' => $array[$i],
                ]);
        }
        
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rights = Rights::all();
        $base_controller =  new HomeController();
        $given_rights = $base_controller->getGroupRights($id);
        
        return view('user_group_edit', [
            'rights' => $rights,
            'given_rights' => $given_rights,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserGroups $user_group)
    {
        $model = new UserGroups();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails()) {
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        }
        $resource = $user_group->update($request->all());
        
        //Clear all rights
        $initial_rights = GroupRights::where('id_group',$user_group->id)->get();
        
//         return json_encode($initial_rights);
        foreach ($initial_rights as $item){
            $item->delete();
        }
        
        $array = $request->rights;
        // insert rights
        for($i=1; $i<count($array); $i++) {
            $group_right = GroupRights::create([
                'id_group' => $user_group->id,
                'id_right' => $array[$i],
            ]);
        }
        
        toastr()->success('Record saved successfully!', 'Notification', [
            'timeOut' => 3000
        ]);
        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserGroups $user_group)
    {
        $user_group->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }
}
