<?php

namespace App\Http\Controllers\SMS;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use AfricasTalking\SDK\AfricasTalking;
use App\Models\SMS\Sms;
use App\Models\Admin\Caterers;
use App\Models\Admin\ContactGroup;
use App\User;
use App\Models\Admin\Modules;
use App\Models\Admin\Visitors;
use App\Models\Admin\Student;

class SmsController extends Controller
{

    public $application;
    public $from;
    public $sms;
    public $voice;

    /*
     * Authorizing resourse
     */
    public function __construct()
    {
        // $this->authorizeResource(Sms::class, 'sms');

        // add AT API Key
        $userName = "isteducation";
        $apiKey = "a35aadbe3fdf7e39fee4e6c3bfcd1c30feda4baef844958a1fc7f0928644efd9";
        $AT = new AfricasTalking($userName, $apiKey);
        $this->from = "IST";
        $this->sms = $AT->sms();
        $this->voice = $AT->voice();
        $this->application = $AT->application();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();

        $messages = Sms::all();
        $model = new Sms();
        $colors = $model->colors;
        $applicationData = $this->getApplicationData();
        $balance = $applicationData->getData()->data->UserData->balance;
        $messagesData = $this->sms->fetchMessages();
        $inbox_sms = $messagesData['data']->SMSMessageData->Messages;
        $sent_messages = Sms::sent(1)->get();

        $contact_groups = ContactGroup::active(1)->get();
        $templates = Sms::template(1)->get();

        return view('inbox', [
            'messages' => $messages,
            'colors' => $colors,
            'modules' => $modules,
            'balance' => $balance,
            'inbox_sms' => $inbox_sms,
            'contact_groups' => $contact_groups,
            'templates' => $templates,
            'sent_messages' => $sent_messages,
        ]);
    }

    public function templates()
    {
        $modules = Modules::with('children')->root(1)
            ->active(1)
            ->get();
        $messages = Sms::template(1)->get();

        $model = new Sms();
        $colors = $model->colors;
        $applicationData = $this->getApplicationData();
        $balance = $applicationData->getData()->data->UserData->balance;
        $messagesData = $this->sms->fetchMessages();
        $inbox_sms = $messagesData['data']->SMSMessageData->Messages;
        return view('sms', [
            'messages' => $messages,
            'colors' => $colors,
            'modules' => $modules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Sms();
        $validator = Validator::make($request->all(), $model->rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource = Sms::create($request->all());
            toastr()->success('Sms template saved successfully!', 'Notification', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sms $sm)
    {
        $model = new Sms();
        $validator = Validator::make($request->all(), $model->update_rules);
        if ($validator->fails())
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toarray()
            ));
        else {
            $resource = $sm->update([
                'title' => $request->title,
                'detail' => $request->detail
            ]);
            toastr()->success('Record Updated successfully!', 'Success', [
                'timeOut' => 3000
            ]);
            return response()->json($resource);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sms $sm)
    {
        $sm->delete();
        toastr('Record deleted!', 'success', 'sucess');
    }

    public function getApplicationData()
    {
        $data = $this->application->fetchApplicationData();
        return response()->json($data);
    }

    public function inboxSMS()
    {
        $messagesData = $this->sms->fetchMessages();
        $inbox_sms = $messagesData['data']->SMSMessageData->Messages;
        return view('sms_inbox_list', [
            'messages' => $inbox_sms
        ]);
    }

    public function sentSMS()
    {
        $messages = Sms::sent(1)->get();
        return view('sms_sent_list', [
            'messages' => $messages
        ]);
    }

    public function composeSMS()
    {
        $messages = Sms::sent(1)->get();
        $contact_groups = ContactGroup::all();
        $students = Student::all();
        $templates = Sms::template(1)->get();

        return view('sms_compose', [
            'messages' => $messages,
            'contact_groups' => $contact_groups,
            'students' => $students,
            'templates' => $templates
        ]);
    }

    public function draftSMS()
    {
        return view('sms_draft_list');
    }

    public function trashSMS()
    {
        return view('sms_trash_list');
    }

    public function sendSms(Request $request)
    {
        if ($request->recepient == 0) {
            return response()->json([
                'error' => 'No recepient selected'
            ]);
        } elseif ($request->template == 0) {
            return response()->json([
                'error2' => 'No template selected'
            ]);
        }
        $group = ContactGroup::with('students')->findOrFail($request->recepient);
        $template = Sms::findOrFail($request->template);
        $recipients = $group->students;

        foreach ($recipients as $recipient) {
            $result = $this->send($template->detail, $recipient->phone);
            $message = $result->getData()->data->SMSMessageData->Message;
            $status = $result->getData()->data->SMSMessageData->Recipients[0]->status;
            $messageId = $result->getData()->data->SMSMessageData->Recipients[0]->messageId;
            $cost = $result->getData()->data->SMSMessageData->Recipients[0]->cost;
            $statusCode = $result->getData()->data->SMSMessageData->Recipients[0]->statusCode;
            $number = $result->getData()->data->SMSMessageData->Recipients[0]->number;
            Sms::create([
                'title' => $template->title,
                'Message' => $message,
                'status' => $status,
                'messageId' => $messageId,
                'cost' => $cost,
                'statusCode' => $statusCode,
                'number' => $number,
                'sent' => 1,
                'detail' => $template->detail,
            ]);
        }
        return response()->json([
            'success' => 'Messages sent successfuly!'
        ]);
    }

    public function sendSMSFromCompose(Request $request)
    {
        if (empty($request->message)) {
            return response()->json([
                'error' => 'No message body!'
            ]);
        } elseif (empty($request->recepient_single) && empty($request->recepient_input) && $request->recepient == 0) {
            return response()->json([
                'error2' => 'No recepient selected/input'
            ]);
        }

        // send for multiple recepient
        if ($request->recepient != 0) {
            $group = ContactGroup::with('students')->findOrFail($request->recepient);
            $recipients = $group->students;
            foreach ($recipients as $recipient) {
                $result = $this->send($request->message, $recipient->phone);
                $message = $result->getData()->data->SMSMessageData->Message;
                $status = $result->getData()->data->SMSMessageData->Recipients[0]->status;
                $messageId = $result->getData()->data->SMSMessageData->Recipients[0]->messageId;
                $cost = $result->getData()->data->SMSMessageData->Recipients[0]->cost;
                $statusCode = $result->getData()->data->SMSMessageData->Recipients[0]->statusCode;
                $number = $result->getData()->data->SMSMessageData->Recipients[0]->number;
                Sms::create([
                    'title' => 'New message',
                    'Message' => $message,
                    'status' => $status,
                    'messageId' => $messageId,
                    'cost' => $cost,
                    'statusCode' => $statusCode,
                    'number' => $number,
                    'sent' => 1,
                    'detail' => $request->message,
                ]);
            }
        }
        // send for single recepient
        if (isset($request->recepient_single)) {
            $result = $this->send($request->message, $request->recepient_single);
            $message = $result->getData()->data->SMSMessageData->Message;
            $status = $result->getData()->data->SMSMessageData->Recipients[0]->status;
            $messageId = $result->getData()->data->SMSMessageData->Recipients[0]->messageId;
            $cost = $result->getData()->data->SMSMessageData->Recipients[0]->cost;
            $statusCode = $result->getData()->data->SMSMessageData->Recipients[0]->statusCode;
            $number = $result->getData()->data->SMSMessageData->Recipients[0]->number;
            Sms::create([
                'title' => 'New message',
                'Message' => $message,
                'status' => $status,
                'messageId' => $messageId,
                'cost' => $cost,
                'statusCode' => $statusCode,
                'number' => $number,
                'sent' => 1,
                'detail' => $request->message,
            ]);
        }

        // send for single recepient input
        if (isset($request->recepient_input)) {
            $result = $this->send($request->message, $request->recepient_input);
            $message = $result->getData()->data->SMSMessageData->Message;
            $status = $result->getData()->data->SMSMessageData->Recipients[0]->status;
            $messageId = $result->getData()->data->SMSMessageData->Recipients[0]->messageId;
            $cost = $result->getData()->data->SMSMessageData->Recipients[0]->cost;
            $statusCode = $result->getData()->data->SMSMessageData->Recipients[0]->statusCode;
            $number = $result->getData()->data->SMSMessageData->Recipients[0]->number;
            Sms::create([
                'title' => 'New message',
                'Message' => $message,
                'status' => $status,
                'messageId' => $messageId,
                'cost' => $cost,
                'statusCode' => $statusCode,
                'number' => $number,
                'sent' => 1,
                'detail' => $request->message,
            ]);
        }

        return response()->json([
            'success' => 'Messages sent successfuly!'
        ]);
    }

    public function send($message, $recepient)
    {
        // add AT API Key
        $userName = "eboard";
        $apiKey = "c83e6a8a02b740383c85ff37e94d76c8242b4ddb418ecb8b2b150562df64a348";
        $AT = new AfricasTalking($userName, $apiKey);
        $from = "eHorizon";

        $sms = $AT->sms();
        $response = $sms->send([
            "to" => $recepient,
            "from" => $from,
            "message" => $message
        ]);

        return response()->json($response);
    }
}
