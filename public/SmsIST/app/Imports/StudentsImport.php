<?php

namespace App\Imports;

use App\Models\Admin\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Student([
            'title'=>$row[1],
            'school'=>$row[2],
            'phone'=>$row[3],
            'email'=>$row[4],
            'address'=>$row[5],
            'active'=> $row[6],
        ]);
    }

}
