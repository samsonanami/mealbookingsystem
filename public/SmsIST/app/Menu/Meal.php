<?php

namespace App\Menu;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    protected $fillable = ['name', 'price', 'detail'];
    
    public $rules = [
        'name' => 'required|string|max:100|unique:meals',
        'price' => 'required',
        'detail' => ''
    ];
    
    public $update_rules = [
        'name' => 'required|string|max:100',
        'price' => 'required',
        'detail' => ''
    ];
}
