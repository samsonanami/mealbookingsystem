<?php
namespace App\Policies;

use App\User;
use App\Models\Booking\AdminBooking;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminBookingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any admin bookings.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess([
            'view-admin-booking'
        ]);
    }

    /**
     * Determine whether the user can view the admin booking.
     *
     * @param \App\User $user
     * @param \App\Models\Booking\AdminBooking $adminBooking
     * @return mixed
     */
    public function view(User $user, AdminBooking $adminBooking)
    {
        return $user->hasAccess([
            'view-admin-booking'
        ]);
    }

    /**
     * Determine whether the user can create admin bookings.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess([
            'create-admin-booking'
        ]);
    }

    /**
     * Determine whether the user can update the admin booking.
     *
     * @param \App\User $user
     * @param \App\Models\Booking\AdminBooking $adminBooking
     * @return mixed
     */
    public function update(User $user, AdminBooking $adminBooking)
    {
        return $user->hasAccess([
            'update-admin-booking'
        ]);
    }

    /**
     * Determine whether the user can delete the admin booking.
     *
     * @param \App\User $user
     * @param \App\Models\Booking\AdminBooking $adminBooking
     * @return mixed
     */
    public function delete(User $user, AdminBooking $adminBooking)
    {
        return $user->hasAccess([
            'delete-admin-booking'
        ]);
    }

    /**
     * Determine whether the user can restore the admin booking.
     *
     * @param \App\User $user
     * @param \App\Models\Booking\AdminBooking $adminBooking
     * @return mixed
     */
    public function restore(User $user, AdminBooking $adminBooking)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the admin booking.
     *
     * @param \App\User $user
     * @param \App\Models\Booking\AdminBooking $adminBooking
     * @return mixed
     */
    public function forceDelete(User $user, AdminBooking $adminBooking)
    {
        //
    }
}
