<?php

namespace App\Policies;

use App\User;
use App\Models\User\Rights;
use Illuminate\Auth\Access\HandlesAuthorization;

class RightsPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any rights.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess([
            'view-rights'
        ]);
    }

    /**
     * Determine whether the user can view the rights.
     *
     * @param  \App\User  $user
     * @param  \App\Models\User\Rights  $rights
     * @return mixed
     */
    public function view(User $user, Rights $rights)
    {
        return $user->hasAccess([
            'view-rights'
        ]);
    }

    /**
     * Determine whether the user can create rights.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess([
            'create-rights'
        ]);
    }

    /**
     * Determine whether the user can update the rights.
     *
     * @param  \App\User  $user
     * @param  \App\Models\User\Rights  $rights
     * @return mixed
     */
    public function update(User $user, Rights $rights)
    {
        return $user->hasAccess([
            'update-rights'
        ]);
    }

    /**
     * Determine whether the user can delete the rights.
     *
     * @param  \App\User  $user
     * @param  \App\Models\User\Rights  $rights
     * @return mixed
     */
    public function delete(User $user, Rights $rights)
    {
        return $user->hasAccess([
            'delete-rights'
        ]);
    }

    /**
     * Determine whether the user can restore the rights.
     *
     * @param  \App\User  $user
     * @param  \App\Models\User\Rights  $rights
     * @return mixed
     */
    public function restore(User $user, Rights $rights)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the rights.
     *
     * @param  \App\User  $user
     * @param  \App\Models\User\Rights  $rights
     * @return mixed
     */
    public function forceDelete(User $user, Rights $rights)
    {
        //
    }
}
