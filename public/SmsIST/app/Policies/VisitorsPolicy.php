<?php
namespace App\Policies;

use App\User;
use App\Models\Admin\Visitors;
use Illuminate\Auth\Access\HandlesAuthorization;

class VisitorsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any visitors.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess([
            'view-visitor'
        ]);
    }

    /**
     * Determine whether the user can view the visitors.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Visitors $visitors
     * @return mixed
     */
    public function view(User $user, Visitors $visitors)
    {
        return $user->hasAccess([
            'view-visitor'
        ]);
    }

    /**
     * Determine whether the user can create visitors.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess([
            'create-visitor'
        ]);
    }

    /**
     * Determine whether the user can update the visitors.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Visitors $visitors
     * @return mixed
     */
    public function update(User $user, Visitors $visitors)
    {
        return $user->hasAccess([
            'update-visitor'
        ]);
    }

    /**
     * Determine whether the user can delete the visitors.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Visitors $visitors
     * @return mixed
     */
    public function delete(User $user, Visitors $visitors)
    {
        return $user->hasAccess([
            'delete-visitor'
        ]);
    }

    /**
     * Determine whether the user can restore the visitors.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Visitors $visitors
     * @return mixed
     */
    public function restore(User $user, Visitors $visitors)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the visitors.
     *
     * @param \App\User $user
     * @param \App\Models\Admin\Visitors $visitors
     * @return mixed
     */
    public function forceDelete(User $user, Visitors $visitors)
    {
        //
    }
}
