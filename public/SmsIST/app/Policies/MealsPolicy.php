<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MealsPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any meals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the meals.
     *
     * @param  \App\User  $user
     * @param  \App\Menu\Meals  $meals
     * @return mixed
     */
    public function view(User $user, Meals $meals)
    {
        //
    }

    /**
     * Determine whether the user can create meals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the meals.
     *
     * @param  \App\User  $user
     * @param  \App\Menu\Meals  $meals
     * @return mixed
     */
    public function update(User $user, Meals $meals)
    {
        //
    }

    /**
     * Determine whether the user can delete the meals.
     *
     * @param  \App\User  $user
     * @param  \App\Menu\Meals  $meals
     * @return mixed
     */
    public function delete(User $user, Meals $meals)
    {
        //
    }

    /**
     * Determine whether the user can restore the meals.
     *
     * @param  \App\User  $user
     * @param  \App\Menu\Meals  $meals
     * @return mixed
     */
    public function restore(User $user, Meals $meals)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the meals.
     *
     * @param  \App\User  $user
     * @param  \App\Menu\Meals  $meals
     * @return mixed
     */
    public function forceDelete(User $user, Meals $meals)
    {
        //
    }
}
