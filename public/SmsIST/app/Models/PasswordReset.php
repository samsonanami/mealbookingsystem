<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{

    protected $table = 'password_resets';
    
    public $timestamps =  false;
    
    protected $fillable = [
        'email',
        'token',
        'updated_at'
    ];
    
    public function scopeEmail($query, $value){
        return $query->where(['email'=>$value, 'created_at'=>null]);
    }
}
