<?php

namespace App\Models\SMS;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $primaryKey = 'id';
    
    protected $fillable = ['title','number', 'cost', 'messageId','Message','detail','inbox','sent','failed','trash', 'draft', 'template', 'status','statusCode'];
    
    public $rules = [
        'title' => 'required|string|max:20|unique:sms',
        'detail' => 'required|string|max:255',
        'number' => 'nullable|string|max:255',
        'cost' => 'nullable|string|max:255',
        'messageId' => 'nullable|string|max:255',
        'Message' => 'nullable|string|max:255',
        'inbox' => 'nullable|string|max:255',
        'sent' => 'nullable|string|max:255',
        'failed' => 'nullable|string|max:255',
        'trash' => 'nullable|string|max:255',
        'draft' => 'nullable|string|max:255',
        'template' => 'nullable|string|max:255',
        'status' => 'nullable|string|max:255',
    ];
    public $update_rules = [
        'title' => 'required|string|max:20',
        'detail' => 'required|string|max:255',
        'number' => 'nullable|string|max:255',
        'cost' => 'nullable|string|max:255',
        'messageId' => 'nullable|string|max:255',
        'Message' => 'nullable|string|max:255',
        'inbox' => 'nullable|string|max:255',
        'sent' => 'nullable|string|max:255',
        'failed' => 'nullable|string|max:255',
        'trash' => 'nullable|string|max:255',
        'draft' => 'nullable|string|max:255',
        'template' => 'nullable|string|max:255',
        'status' => 'nullable|string|max:255',
    ];
    
    public $colors = [
        'red',
        'green',
        'blue',
        'yellow',
        'purple',
    ];

    public function scopeSent($query, $value){
        return $query->where('sent',$value)->orderBy('created_at', 'desc');
    }

    public function scopeTemplate($query, $value){
        return $query->where('template',$value);
    }
    public function scopeDraft($query, $value){
        return $query->where('draft',$value);
    }
}
