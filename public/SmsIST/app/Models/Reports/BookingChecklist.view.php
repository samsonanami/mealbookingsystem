<?php

use koolreport\widgets\google\BarChart;
use \koolreport\widgets\koolphp\Table;
?>
<style>
    .cssHeader {
        background-color: #e9ffe8;
    }

    .cssItem {
        background-color: #fdffe8;
    }

    .page_break {
        page-break-before: always;
    }
</style>

<?php
Table::create([
    "dataSource" => $this->dataStore("bookings_data"),
    "cssClass" => array(
        "th" => "cssHeader",
        "tr" => "cssItem",
        "table" => "table table-bordered table-striped",
    ),
    // "paging" => array(
    //     "pageSize" => 15,
    //     "pageIndex" => 0,
    // ),
    "showFooter" => true,
    "columns" => array(
        "User Name" => array(
            "cssStyle" => "text-align:left",
            "footerText" => "<b>Totals</b>"
        ),
        "Booked For",
        "Payroll",
        "Monday" => array(
            // "cssStyle" => "text-align:right",
            "footer" => "sum",
            "footerText" => "<b>@value</b>",
            "formatValue" => function ($value, $row) {
                $count = $value;
                $color = $value == 1 ? "#718c00" : "#e83e8c";
                $icon = $value == 1 ? "fa fa-check" : "fa fa-minu";
                return "<span style='color:$color'><i class='$icon'></i>&nbsp; $count</span>";
            }
        ),
        "Tuesday" => array(
            // "cssStyle" => "text-align:right",
            "footer" => "sum",
            "footerText" => "<b>@value</b>",
            "formatValue" => function ($value, $row) {
                $count = $value;
                $color = $value == 1 ? "#718c00" : "#e83e8c";
                $icon = $value == 1 ? "fa fa-check" : "fa fa-minu";
                return "<span style='color:$color'><i class='$icon'></i>&nbsp; $count</span>";
            }
        ),
        "Wednesday" => array(
            // "cssStyle" => "text-align:right",
            "footer" => "sum",
            "footerText" => "<b>@value</b>",
            "formatValue" => function ($value, $row) {
                $count = $value;
                $color = $value == 1 ? "#718c00" : "#e83e8c";
                $icon = $value == 1 ? "fa fa-check" : "fa fa-minu";
                return "<span style='color:$color'><i class='$icon'></i>&nbsp; $count</span>";
            }
        ),
        "Thursday" => array(
            // "cssStyle" => "text-align:right",
            "footer" => "sum",
            "footerText" => "<b>@value</b>",
            "formatValue" => function ($value, $row) {
                $count = $value;
                $color = $value == 1 ? "#718c00" : "#e83e8c";
                $icon = $value == 1 ? "fa fa-check" : "fa fa-minu";
                return "<span style='color:$color'><i class='$icon'></i>&nbsp; $count</span>";
            }
        ),
        "Friday" => array(
            // "cssStyle" => "text-align:right",
            "footer" => "sum",
            "footerText" => "<b>@value</b>",
            "formatValue" => function ($value, $row) {
                $count = $value;
                $color = $value == 1 ? "#718c00" : "#e83e8c";
                $icon = $value == 1 ? "fa fa-check" : "fa fa-minu";
                return "<span style='color:$color'><i class='$icon'></i>&nbsp; $count</span>";
            }
        ),
        "Total Price" => array(
            // "cssStyle" => "text-align:right",
            // "prefix" => "Ksh. ",
            "footer" => "sum",
            "footerText" => "<b> @value</b>"
        )
    ),
]);
?>
<br>

<!-- My Dynamic Modal -->
<div id="create" class="modal full-modal fade dynamic-modal in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog full-modal-dialog">
        <div class="modal-content full-modal-content">
            <div class="modal-header full-modal-header">
                <span class=" text-info pull-left">
                    <h5><b>{{ env('APP_NAME') }}</b></h5>
                </span>
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body full-modal-body">
                <div class="row">

                    <!-- end of form -->
                </div>
            </div>
        </div>
    </div>
    <!-- End modal -->