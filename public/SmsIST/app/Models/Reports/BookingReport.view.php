<?php
use koolreport\widgets\google\BarChart;
use \koolreport\widgets\koolphp\Table;
?>
<style>
    .cssHeader {
        background-color: #e9ffe8;
    }

    .cssItem {
        background-color: #fdffe8;
    }

    .page_break {
        page-break-before: always;
    }
</style>

<?php
Table::create([
    "dataSource" => $this->dataStore("bookings_data"),
    "cssClass" => array(
        "th" => "cssHeader",
        "tr" => "cssItem",
        "table" => "table table-bordered table-striped",
    ),
    // "paging" => array(
    //     "pageSize" => 15,
    //     "pageIndex" => 0,
    // ),
    "showFooter" => true,
    "columns" => array(
        "User Name",
        "Booked For",
        "Payroll",
        "Plates" => array(
            "cssStyle" => "text-align:right",
            "footer" => "count",
            "footerText" => "<b>Plates Count :</b> @value"
        ),
        "Total Price" => array(
            "cssStyle" => "text-align:right",
            "prefix" => "Ksh. ",
            "footer" => "sum",
            "footerText" => "<b>Total:</b> @value"
        )
    ),
]);
?>
<br>

<!-- My Dynamic Modal -->
<div id="create" class="modal full-modal fade dynamic-modal in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog full-modal-dialog">
        <div class="modal-content full-modal-content">
            <div class="modal-header full-modal-header">
                <span class=" text-info pull-left">
                    <h5><b>{{ env('APP_NAME') }}</b></h5>
                </span>
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body full-modal-body">
                <div class="row">

                    <!-- end of form -->
                </div>
            </div>
        </div>
    </div>
    <!-- End modal -->