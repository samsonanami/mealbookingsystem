<?php

namespace App\Models\User;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserGroups extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['title','detail'];
    
    public $rules = [
        'title'=> 'string|unique:rights',
        'detail' => 'string',
    ];
    public $update_rules = [
        'title'=> 'string',
        'detail' => 'string',
    ];
    
    public function user(){
        return $this->belongsToMany(User::class);
    }
    
    public function rights(){
        return $this->belongsToMany(Rights::class, 'group_rights',  'id_group', 'id_right');
    }

}
