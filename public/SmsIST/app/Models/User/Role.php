<?php

namespace App\Models\User;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'title', 'slug', 'permissions',
    ];
    
    protected $casts = [
        'permissions' => 'array',
    ];
    
    public $rules = [
        'title' => 'string|required|unique:roles',
        'slug' => 'string|required|unique:roles',
        'permissions' => 'array',
    ];
    
    public $update_rules = [
        'title' => 'string|required',
        'slug' => 'string|required',
        'permissions' => 'array',
    ];
    
    public function users(){
        return $this->belongsToMany(User::class, 'role_users');
    }
    
    public function hasAccess(array $permissions) : bool
    {
        foreach ($permissions as $permission){
            if($this->hasPermission($permission))
                return true;
        }
        return false;
    }
    
    private function hasPermission(string $permission) : bool
    {
        return $this->permissions[$permission] ?? false;
    }
}
