<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $primaryKey = 'id';
    
    protected $fillable = ['title', 'school', 'address', 'phone', 'email', 'active'];
    
    public $rules = [
        'title'=>'string|unique:students',
        'school'=>'string',
        'phone'=>'required|string|unique:students',
        'email'=>'string|nullable|max:200',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ];
    
    
    public $update_rules = [
        'title'=>'string',
        'school'=>'string',
        'phone'=>'required|string',
        'email'=>'string|nullable|max:200',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ]; 
    
    public function scopeActive($query, $value){
        return $query->where('active',$value);
    }

    public function contactGroups()
    {
        return $this->belongsToMany(ContactGroup::class, 'student_group');
    }
        
}
