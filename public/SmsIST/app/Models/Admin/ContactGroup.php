<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ContactGroup extends Model
{
    protected $primaryKey = 'id';
    
    protected $fillable = ['title', 'details', 'address','active'];
    
    public $rules = [
        'title'=>'string|unique:students',
        'details'=>'string|max:250',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ];
    
    
    public $update_rules = [
        'title'=>'string',
        'details'=>'string|max:250',
        'address'=>'string|max:200',
        'active'=> 'integer',
    ]; 
    
    public function scopeActive($query, $value){
        return $query->where('active',$value);
    }

    public function students(){
        return $this->belongsToMany(Student::class, 'student_group');
    }
}
