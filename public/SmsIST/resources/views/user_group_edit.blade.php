<form role="form">
    @csrf
    <div class="form-group">
        <label for="id">ID<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="id" name="id" disabled>
    </div>
    <div class="form-group">
        <label for="title">Title<i class="text-danger">*</i></label>
        <input type="title" class="form-control" id="title" name="title" placeholder="Enter title ...">
        <label class="error-title"></label>
    </div>
    
    <div class="form-group">
        <label for="detail">Detail</label>
        <textarea class="form-control" id="detail" name="detail" placeholder="Describe here ..."></textarea>
        <label class="error-detail"></label>
    </div>
    
    <div class="form-group">
        <h4>Group Rights</h4>
        <hr>
            @foreach($rights as $right)
            <div class="checkbox">
            <label  class="form-group" >
                <input type="checkbox" value="{{$right->id}}"
                @if(in_array($right->title,$given_rights))
                	checked
                @endif
                >
                &nbsp; {{$right->detail}}
            </label>
            </div>
            @endforeach
            <br>
    </div>


    <div class="text-right">
        <button type="button" id="add_right" class="btn btn-primary"><i class="fa fa-check" ></i>Submit</button>
    </div>
    </form>


<script type="text/javascript">
// Craete right

$("#add_right").click(function() {

	var rights= { 'group_rights' : []};

    $(":checked").each(function() {
      rights['group_rights'].push($(this).val());
    });
    console.log(rights);

	
	var url = '{{ route("user_group.update", ["right"=>":right"]) }}';
    $.ajax({
        type: 'PATCH',
        url: url.replace(":right", $('input[name=id]').val()),
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=title]').val(),
            'detail': $('textarea[name=detail]').val(),
            'rights': rights['group_rights'],
        },
        success: function(data) {
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden').addClass('text-danger');
                    $('#title').addClass('has-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden').addClass('text-danger');
                    $('#detail').addClass('has-error');
                    $('.error-detail').text(data.errors.detail);
                }
            } else {
                console.log(data);
                location.reload();
            }
        },
    });
});
</script>