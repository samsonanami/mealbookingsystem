<section class="panel">
    <header class="panel-heading wht-bg">
        <h4 class="gen-case"> Compose Message
            <div class="compose-btn pull-right">
                <button class="btn btn-success btn-round btn-sm send_btn1"><i class="fa fa-check"></i>&nbsp;Send&nbsp;</button>
                <button class="btn btn-warning btn-round btn-sm discard_btn1"><i class="fa fa-times"></i> Discard </button>
                <button class="btn btn-default btn-round  btn-sm draft_btn1"><i class="fa fa-file"></i> Draft </button>
            </div>
        </h4>
    </header>
    <div class="panel-body">
        <div class="">
            <div class="col-sm-3">
                <label> Select Group</label>
                <select id="recepient_group" name="recepient_group" class="form-control">
                    <option value="0">Select group ...</option>
                    @foreach($contact_groups as $group)
                    <option value="{{$group->id}}">{{$group->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-3">
                <label for="to" class="">Select Recepient:</label>
                <select id="recepient_single" name="recepient_single" class="form-control">
                    <option value="0">Select reciever ...</option>
                    @foreach($students as $student)
                    <option value="{{$student->id}}">{{$student->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-3 clearfix">
                <div class="form-group">
                    <label for="create_email">Type Phone #</label>
                    <input type="text" class="form-control" id="recepient_input" name="recepient_input" placeholder="e.g +254717044512">
                    <label class="error-recepient_individual"></label>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label for="create_email">Select Template </label>
                    <select id="template1" name="template1" class="template1 form-control">
                        <option value="0">Select template ...</option>
                        @foreach($templates as $template)
                        <option value="{{$template->detail}}" data-detail="{{$template->detail}}">{{$template->title}}</option>
                        @endforeach
                    </select>
                    <label class="error-template label text-danger"></label>
                </div>
            </div>

            <div class="compose-editor">
                <textarea class="wysihtml5 form-control message_body" id="message_body" name="message_body" rows="9"></textarea>
            </div>
            <div class="compose-btn">
                <button class="btn btn-success btn-round btn-sm send_btn1"><i class="fa fa-check"></i>&nbsp;Send&nbsp;</button>
                <button class="btn btn-warning btn-round btn-sm discard_btn1"><i class="fa fa-times"></i> Discard </button>
                <button class="btn btn-default btn-round  btn-sm draft_btn1"><i class="fa fa-file"></i> Draft </button>
            </div>

            <!-- alert -->
            <div class="alert alert-block fade in hidden">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign"></i>
                    <label id="error-header"></label>
                </h4>
                <label id="error-message"></label>
            </div>


            </form>
        </div>
    </div>
</section>

<script>
    $("select[name='template1']").change(function() {
        var data = $(this).val();
        $("textarea[name='message_body']").val(data);
    });

    $("select[name='template1']").change(function() {
        var data = $(this).val();
        $("textarea[name='message_body']").val(data);
    });

    // Create
    $(".send_btn1").click(function() {
        console.log($('textarea[name=message_body]').val());
        if (confirm("Are you SURE you want to send the MESSAGE(s)?")) {

            $('.alert').removeClass('hidden');
            $('.alert').addClass('alert-info');
            $('#error-header').text("Processing");
            $('#error-message').text("Sending messages, please wait ...");
            $('#page-spinner-bar').fadeOut(1000);
            $('#page-spinner-bar').fadeIn(1000);

            var test = $('input[name=recepient_group]').val();
            console.log(test);

            $.ajax({
                type: 'POST',
                url: '{{ route("sms_send_from_compose") }}',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'message': $('textarea[name=message_body]').val(),
                    'recepient': $('select[name=recepient_group]').val(),
                    'recepient_single': $('input[name=recepient_single]').val(),
                    'recepient_input': $('input[name=recepient_input]').val(),
                },
                ajaxSend: function() {},
                success: function(data) {
                    console.log(data);
                    if (data.error) {
                        // location.reload();
                        $('.alert').removeClass('hidden');
                        $('.alert').addClass('alert-danger');
                        $('#error-header').text("Error");
                        $('#error-message').text(data.error);
                        $('#page-spinner-bar').fadeOut(1000);
                        $('.alert').fadeOut(3000);
                    }
                    else if (data.error2) {
                        // location.reload();
                        $('.alert').removeClass('hidden');
                        $('.alert').addClass('alert-danger');
                        $('#error-header').text("Error");
                        $('#error-message').text(data.error2);
                        console.log(data.error);
                        $('#page-spinner-bar').fadeOut(1000);
                        $('.alert').fadeOut(3000);
                    } else {
                        console.log(data);
                        $('.alert').removeClass('hidden');
                        $('.alert').removeClass('alert-danger');
                        $('.alert').removeClass('alert-info');
                        $('.alert').addClass('alert-success');
                        $('#error-header').text("Success!");
                        $('#error-message').text('Message(s) sent successfully!');
                        $('#page-spinner-bar').fadeOut(1000);
                        $('.alert').fadeOut(3000);
                    }
                },
                default: function() {
                    $('#page-spinner-bar').fadeOut(1000);
                    $('.alert').fadeOut(3000);
                },
                error: function() {
                    $('#page-spinner-bar').fadeOut(1000);
                    $('.alert').fadeOut(3000);
                }
            });
            window.location.reload();
        }
    });
</script>