@extends('layouts.main') @section('content')
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Contact Group View<span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> <a href="javascript:;" class="fa fa-cog"></a> <a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="pull-right">
									<button type="button" id="new_contact" class="btn bg-success btn-round" data-toggle="modal" data-target="#dynamic-modal" data-path="{{ route('contact-group.create') }}">
										<i class="fa fa-plus"></i> New Group
									</button>
								</div>
							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered" id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Details</th>
										<th>Address</th>
										<th>Active</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($contact_groups as $contact_group)
									<tr class="">
										<td>{{$contact_group->id}}</td>
										<td>{{$contact_group->title}}</td>
										<td>{{$contact_group->details}}</td>
										<td>{{$contact_group->address}}</td>
										<td>{{$contact_group->active}}</td>
										<td class="text-right">
											<a href="#" class="edit-modal btn bg-info btn-round" data-id="{{$contact_group->id}}" data-title="{{$contact_group->title}}" data-details="{{$contact_group->details}}" data-phone="{{$contact_group->phone}}" data-address="{{$contact_group->address}}" data-active="{{$contact_group->active}}" data-path="{{ route('contact-group.edit',['contact_group'=>$contact_group->id]) }}" data-toggle="modal" data-target="#dynamic-modal">
												<i class="fa fa-pencil"></i>
											</a> <a href="#" class="delete-modal btn bg-danger btn-round" data-id="{{$contact_group->id}}" data-title="{{$contact_group->title}}" data-details="{{$contact_group->details}}" data-phone="{{$contact_group->phone}}" data-address="{{$contact_group->address}}" data-active="{{$contact_group->active}}" data-phone="{{$contact_group->phone}}" data-detail="{{$contact_group->active}}">
												<i class="fa fa-trash-o"></i>
											</a></td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>

			</div>
		</div>
		<!-- page end-->
	</section>

</section>
<!--main content end-->


<!-- My Dynamic Modal -->
<div id="dynamic-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<span class=" text-info pull-left">{{ config('app.name')}}</span>
				<button type="button" class="btn bg-danger btn-md pull-right btn-round" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body clearfix">
				<!-- Body content will come here -->
			</div>
			<div class="modal-footer">
				{{env('APP_NAME')}}
			</div>
		</div>
	</div>
</div>
<!-- End modal -->


<script type="text/javascript">
	//create new
	$("#new_contact").click(function() {
		$.ajax({
			type: 'GET',
			url: $(this).data('path'),
			success: function(data) {
				$('#dynamic-modal div.modal-body').html(data);
			},
		});
	});


	//export 
	$("#export").click(function() {
		window.location = $(this).data('path');
	});


	//import 
	$("#import").click(function() {
		$.ajax({
			type: 'GET',
			url: $(this).data('path'),
			success: function(data) {
				$('#dynamic-modal div.modal-body').html(data);
			},
		});
	});

	//form Delete function
	$(document).on('click', '.delete-modal', function() {
		$('#dynamic-modal').addClass('modal-danger');
		$('.modal-body').html('<h4 class="text-danger">Delete this item? ' + $(this).data('id') + ':' + $(this).data('title') + '</h4><input type="hidden" id="contact_group_id">');
		$('.modal-footer').html('<button type="button" id="calcel" data-dismiss="modal" class="btn btn-default pull-left" >Exit</button> <button type="button" id="delete" class="delete btn btn-danger pull-right" >Delete</button>');
		$('#dynamic-modal').modal('show');
		$('#contact_group_id').val($(this).data('id'));
	});

	$('.modal-footer').on('click', '.delete', function() {
		var url = '{{ route("contact-group.destroy", ["id"=>":id"]) }}';
		$.ajax({
			type: 'DELETE',
			url: url.replace(":id", $('#contact_group_id').val()),
			data: {
				'_token': $('input[name=_token]').val(),
				'id': $('#contact_group_id').val(),
			},
			success: function(data) {
				location.reload();
			}
		});
	});
	//Action Edit/Update
	$(document).on('click', '.edit-modal', function() {
		var id = $(this).data('id');
		var title = $(this).data('title');
		var details = $(this).data('details');
		var address = $(this).data('address');
		var active = $(this).data('active');

		$.ajax({
			type: 'GET',
			url: $(this).data('path'),
			success: function(data) {
				$('#dynamic-modal div.modal-body').html(data);
			},
		});
	});

	//Show function
	$(document).on('click', '.show-modal', function() {
		var id = $(this).data('id');
		var title = $(this).data('title');
		var details = $(this).data('details');
		var address = $(this).data('address');
		var active = $(this).data('active');

		$.ajax({
			type: 'GET',
			url: $(this).data('path'),
			success: function(data) {
				$('#dynamic-modal div.modal-body').html(data);
				$('#id').text(id);
				$('#title').text(title);
				$('#address').text(address);
				$('#active').text(active);
				$('#details').text(details);
			},
		});
	});
</script>
@endsection