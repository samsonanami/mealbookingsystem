<form role="form">
    @csrf
    <div class="form-group">
        <label for="name">User Name<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="create_name" name="create_name" placeholder="Enter name ...">
        <label class="error-name"></label>
    </div>
     <div class="form-group">
        <label for="create_payroll">Payroll<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="create_payroll" name="create_payroll">
        <label class="error-create_payroll"></label>
    </div>
    
    <div class="form-group">
        <label for="create_email">Email</label>
        <input type="text" class="form-control" id="create_email" name="create_email" placeholder="email">
        <label class="error-create_email"></label>
    </div>

    <div class="form-group">
        <label for="create_username">Username</label>
        <input type="text" class="form-control" id="create_username" name="create_username">
        <label class="error-create_username"></label>
    </div>

    <div class="form-group">
        <label for="create_phone">Phone</label>
        <input type="text" class="form-control" id="create_phone" name="create_phone" placeholder="+254781..">
        <label class="error-create_phone"></label>
    </div>
    <div class="form-group">
        <label for="create_password">Password</label>
        <input type="password" class="form-control" id="create_password" name="create_password" placeholder="">
        <label class="error-create_password"></label>
    </div>
    <div class="form-group">
        <label for="password-confirm">{{ __('Confirm Password') }}</label>
        <input id="password-confirm" type="password" class="form-control" name="create_password_confirmation" required autocomplete="new-password">
    </div>
    
    <div class="form-group">
        <label for="create_role">User Role<i class="text-danger">*</i></label>
        <select id="create_role" class="form-control" name="create_role" required>
        @foreach($roles as $id => $role)
            <option value="{{$id}}">{{$role}}</option>
        @endforeach
        </select>
        <label class="error-create_role"></label>
    </div>
    
    <div class="form-group">
        <label for="create_active">Active<i class="text-danger">*</i></label>
        <select class="form-control" id="create_active" name="create_active">
            <option value=1 selected>Yes</option>
            <option value="0">No</option>
        </select>
        <label class="error-create_active"></label>
    </div>

    <button type="button" data-dismiss="modal" id="close" class="btn btn-default pull-left"><i class="fa fa-times" ></i>Exit</button>
    <button type="button" id="add_user" class="btn btn-success pull-right"><i class="fa fa-check" ></i>Submit</button>
    <br><br>
</form>


<script type="text/javascript">


//Craete module

$("#add_user").click(function() {
    console.log('clicked submit');
    var test = $('input[name=create_username]').val();
    console.log(test);
    $.ajax({
        type: 'POST',
        url: '{{ route('user.store') }}',
        data: {
            '_token': $('input[name=_token]').val(),
            'name': $('input[name=create_name]').val(),
            'username': $('input[name=create_username]').val(),
            'payroll': $('input[name=create_payroll]').val(),
            'email': $('input[name=create_email]').val(),
            'phone': $('input[name=create_phone]').val(),
            'role': $('select[name=create_role]').val(),
            'active': $('select[name=create_active]').val(),
            'password': $('input[name=create_password]').val(),
            'password_confirmation': $('input[name=create_password_confirmation]').val(),
        },
        success: function(data) {
            if (data.errors) {
                console.log(data);
                if (data.errors.name) {
                    $('.error-create_name').removeClass('hidden').addClass('text-danger');
                    $('#create_name').addClass('has-error');
                    $('.error-create_name').text(data.errors.create_name);
                } else if (data.errors.create_email) {
                    $('.error-create_email').removeClass('hidden').addClass('text-danger');
                    $('#create_email').addClass('has-error');
                    $('.error-create_email').text(data.errors.create_email);
                }else if (data.errors.create_phone) {
                    $('.error-create_phone').removeClass('hidden').addClass('text-danger');
                    $('#create_phone').addClass('has-error');
                    $('.error-create_phone').text(data.errors.create_phone);
                }else if (data.errors.create_role) {
                    $('.error-create_role').removeClass('hidden').addClass('text-danger');
                    $('#create_role').addClass('has-error');
                    $('.error-create_role').text(data.errors.create_role);
                }else if (data.errors.create_payroll) {
                    $('.error-create_payroll').removeClass('hidden').addClass('text-danger');
                    $('#create_payroll').addClass('has-error');
                    $('.error-create_payroll').text(data.errors.create_payroll);
                }else if (data.errors.create_password) {
                    $('.error-create_password').removeClass('hidden').addClass('text-danger');
                    $('#create_password').addClass('has-error');
                    $('.error-create_password').text(data.errors.create_password);
                }else if (data.errors.create_username) {
                    $('.error-create_username').removeClass('hidden').addClass('text-danger');
                    $('#create_username').addClass('has-error');
                    $('.error-create_username').text(data.errors.create_username);
                }
            } else {
                console.log(data);
                location.reload();
            }
        },
    });
});



</script>