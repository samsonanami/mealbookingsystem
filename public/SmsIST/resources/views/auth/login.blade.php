@extends('layouts.login')

@section('content')

<div class="container">

    <!-- Status and notifications -->

    @if (session('status'))
    @php
    toastr()->success(session('status'), 'Notification', [
    'timeOut' => 3000
    ]);
    @endphp
    @endif

    @error('email')
    @php
    toastr()->error($message, 'Notification', [
    'timeOut' => 3000
    ]);
    @endphp
    @enderror

    @error('password')
    @php
    toastr()->error($message, 'Notification', [
    'timeOut' => 3000
    ]);
    @endphp
    @enderror

    <form class="form-signin" method="POST" action="{{ route('login') }}">
        @csrf
        <h2 class="form-signin-heading">
            <a target="_blank" href="http://www.stl-horizon.com">
                <img class="margin10" alt="" height="64px" src="{{ asset('bucket/images/STL Logo_.png') }}" />
            </a>
            <h4 class="text-primary text-center"><strong>{{ config('app.name') }}</strong></h4>
            <!-- <h5 class="text-primary text-center text-capitalize"> Admin login</h5> -->
        </h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <label for="email" class="col-form-label text-left">{{ __('Username / Email') }}</label>
                <input id="email" type="text" class="form-control {{ $errors->has('email') ? ' has-warning' : '' }}" name="email" placeholder="Username" value="{{ old('email') }}">
                <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' has-warning' : '' }}" name="password" placeholder="Password">
            </div>
            <div class="form-check col-md-12">
                <input class="form-check-input" type="checkbox" name="remember" id="remember">
                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
                <a class="pull-right" data-toggle="modal" href="#myModal"> Forgot Password?</a>
            </div>
            <span>
                <button type="submit" id="submit" class="btn btn-l btn-round bg-primary btn-block" name="submit"><i class="fa fa-check"></i> Sign in</button>
            </span>
            <br>
            <div class="text-center copyright">
                <!-- <img class="margin10" alt="" height="80" src="{{ asset('stl/theme/img/STL Logo_.png') }}" /><br> -->
                <a target="_blank" href="http://stl-horizon.com">{{date('Y')}} &copy Software Technologies Ltd.</a>
            </div>
            <br>

        </div>
    </form>

    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <form class="form-signin-reset" method="POST" action="{{ route('password.email') }}">
            @csrf
            <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Forgot Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>

                        <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" type="submit">Send Password Reset Link</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal -->

</div>


</div>


@endsection