@extends('layouts.main')
@section('content')

<!--main content start-->
<section id="main-content">
	<section class="wrapper">
	<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Messages Templates View <span class="tools pull-right"> <a
							href="javascript:;" class="fa fa-chevron-down"></a> <a
							href="javascript:;" class="fa fa-cog"></a> <a href="javascript:;"
							class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
								@can('create-sms')
									<button type="button" id="editabe-sample_new"
										class="create-modal btn btn-round btn-success">
										<i class="fa fa-plus"></i> New Template
									</button>
								@endcan
								</div>

							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Detail</th>
										<th>Date Created</th>
										<th>Late Updated</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($messages as $message)
									<tr class="">
										<td>{{$message->id}}</td>
										<td>{{$message->title}}</td>
										<td>{{$message->detail}}</td>
										<td>{{$message->created_at}}</td>
										<td>{{$message->updated_at}}</td>
										<td class="text-right"><a href="#"
											class="show-modal btn btn-info btn-round"
											data-id="{{$message->id}}" data-title="{{$message->title}}"
											data-detail="{{$message->detail}}"
											data-created_at="{{$message->created_at}}"
											data-updated_at="{{$message->updated_at}}"> <i
												class="fa fa-eye"></i>
										</a>
										@can('update-sms')
										<a href="#" class="edit-modal btn btn-warning btn-round"
											data-id="{{$message->id}}" data-title="{{$message->title}}"
											data-detail="{{$message->detail}}"
											data-created_at="{{$message->created_at}}"
											data-updated_at="{{$message->updated_at}}"> <i
												class="fa fa-pencil"></i>
										</a>
										@endcan
										@can('delete-sms')
										<a href="#" class="delete-modal btn btn-danger btn-round"
											data-id="{{$message->id}}" data-title="{{$message->title}}"
											data-detail="{{$message->detail}}"
											data-created_at="{{$message->created_at}}"
											data-updated_at="{{$message->updated_at}}"> <i
												class="fa fa-trash-o"></i>
										</a>
										@endcan
										</td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
		</div>
		<!-- page end-->

	</section>
</section>
<!--main content end-->


<!-- Modals -->


<div id="create" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close  btn-white" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Title :</label>
						<div class="col-sm-10">
							<input id="name" type="text" class="form-control input-name"
								id="name" name="name" placeholder="title here">
							<p class="error-title hidden parsley-required" role="alert">
						
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Body :</label>
						<div class="col-sm-10">
							<textarea id="detail" class="form-control input-detail"
								id="detail" name="detail" placeholder="Description here"
								rows="6" required></textarea>
							<p class="error-detail hidden parsley-required" role="alert">
						
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-round" type="submit" id="add">
					<span class="fa fa-check">&nbsp; </span>Submit
				</button>
				<button class="btn btn-white pull-left btn-round" type="button"
					data-dismiss="modal">
					<span class="fa fa-times"></span>&nbsp; Exit
				</button>
			</div>
		</div>
	</div>
</div>
</div>



<div id="show" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close btn-white" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">ID :</label> <span id="i" class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Name :</label> <span id="nam" class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Detail :</label> <span id="det" class="text-primary"></b>
				
				</div>
				<div class="form-group">
					<label for="">Created Date :</label> <span id="crd"
						class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Last Updated :</label> <span id="lsu"
						class="text-primary"></span>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="modal">
					<div class="form-group">
						<label class="control-label col-sm-2" for="id">ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="fid" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="name">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nam1">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Detail</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="det1" rows="6"></textarea>
						</div>
					</div>
				</form>


				<div class="deleteContent">
					Are You sure want to delete <span class="title"></span>? <span
						class="hidden id"></span>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn actionBtn" data-dismiss="modal">
					<span id="footer_action_button"></span>
				</button>
				<button type="button" class="btn btn-white btn-round pull-left"
					data-dismiss="modal">
					Exit
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).on('click', '.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Create Message Template');
});

//Action Create
$("#add").click(function() {
	var url = "{{ route('sms.store') }}";
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=name]').val(),
			'detail': $('textarea[name=detail]').val(),
			'template': 1,
        },
        success: function(data) {
            console.log('Submit successful');
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden');
                    $('.error-title').addClass('text-danger');
                    $('.input-title').addClass('parsley-error');
                    $('.error-title').text(data.errors.name);
                }else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.title);
                }
            } else {
                location.reload();
            }
        },
    });
    $('.input-name').removeClass('parsley-error');
    $('.error-name').addClass('hidden');
    $('.input-detail').removeClass('parsley-error');
    $('.error-detail').addClass('hidden');
});

// Action Edit/Update
$(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update");;
    $('.actionBtn').addClass('btn-success btn-round');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Update template');
    $('.deleteContent').hide();
    $('.form-horizontal').show();

    $('#fid').val($(this).data('id'));
    $('#nam1').val($(this).data('title'));
    $('#det1').text($(this).data('detail'));
    $('#crd1').text($(this).data('created_at'));
    $('#lsu1').text($(this).data('updated_at'));
    $('#myModal').modal('show');

});

$('.modal-footer').on('click', '.edit', function() {
	console.log('Samdoh edit button click');
	var url = '{{ route("sms.update",["sm"=> ":sm" ]) }}';
	var id = $("#fid").val();
    $.ajax({
        type: 'PUT',
        url: url.replace(":sm", id),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'title': $("#nam1").val(),
			'detail': $('#det1').val(),
			'template': 1,
        },
        success: function(data) {
				console.log(data);
				
            if (data.errors) {
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden');
                    $('.error-title').addClass('text-danger');
                    $('.input-title').addClass('parsley-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.detail);
                }
            } else {
                location.reload();
            }
        }
    });
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('#footer_action_button').removeClass('glyphicon-check');
    $('#footer_action_button').addClass('glyphicon-trash');
    $('.actionBtn').removeClass('btn-primary');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: '/sms/' + $('.id').text(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
            $('.post' + $('.id').text()).remove();
            location.reload();
        }
    });
});

// Show function
$(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#i').text($(this).data('id'));
    $('#nam').text($(this).data('name'));
    $('#tit').text($(this).data('title'));
    $('#det').text($(this).data('detail'));
    $('#crd').text($(this).data('created_at'));
    $('#lsu').text($(this).data('updated_at'));
    $('.modal-title').text('Show SMS');
});


// Create
$("#sendSms").click(function() {
    console.log("send sms clicked");
    var messages= { 'messages' : []};
    var group = $('#recepient option:selected').val();

    $(":checked").each(function() {
    	messages['messages'].push($(this).val());
    });
    
    if (confirm("Are you SURE you want to send the MESSAGE(s)?")) { 
		
	$('#page-spinner-bar').fadeIn(1000);
	
        $.ajax({
            type: 'POST',
            url: '{{ route('sms.send') }}',
            data: {
                '_token': $('input[name=_token]').val(),
                'messages': messages['messages'],
                'group': group,
            },
            success: function(data) {
                console.log(data);
                if (data.error) {
                    // location.reload();
                    $('.alert').removeClass('hidden');
                	$('.alert').removeClass('alert-success');
                	$('.alert').addClass('alert-danger');
                	$('#error-header').text("Error");
                	$('#error-message').text(data.error);
                    console.log(data.error);
					$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
                }if (data.error2) {
                    // location.reload();
                    $('.alert').removeClass('hidden');
                	$('.alert').removeClass('alert-success');
                	$('.alert').addClass('alert-danger');
                	$('#error-header').text("Error");
					$('#error-message').text(data.error2);
                    console.log(data.error);
					$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
                } else {
					console.log(data);
					$('.alert').removeClass('hidden');
                	$('.alert').removeClass('alert-danger');
                	$('.alert').addClass('alert-success');
                	$('#error-header').text("Success!");
					$('#error-message').text('Message(s) sent successfully!');
					$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
				}
			},
			default: function(){
				$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
			},
			error: function(){
				$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
			}
        });
    }
});
</script>


@endsection
