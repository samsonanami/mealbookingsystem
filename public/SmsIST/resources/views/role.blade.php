@extends('layouts.main') @section('content')
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			{!! session('success') !!}
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						User Group View <span class="tools pull-right"> <a
							href="javascript:;" class="fa fa-chevron-down"></a> <a
							href="javascript:;" class="fa fa-cog"></a> <a href="javascript:;"
							class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
								@can('create-role')
									<button type="button" id="newresource"
										class="btn btn-primary red-tooltip"
										data-show="tooltip"
										data-placement="top"
                                       	title="New group"
                                       	data-original-title="Button"
										data-toggle="modal"
										data-target="#dynamic-modal"
										data-path="{{ route('role.create') }}">
										<i class="fa fa-plus"></i> New role
									</button>
								@endcan
								</div>
								<div class="btn-group pull-left">
									<button class="btn btn-default dropdown-toggle"
										data-toggle="dropdown">
										Tools <i class="fa fa-angle-down"></i>
									</button>

								</div>
							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Slug</th>
										<th>Permissions</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($resources as $resource)
									<tr class="">
										<td>{{$resource->id}}</td>
										<td>{{$resource->title}}</td>
										<td>{{$resource->slug}}</td>
										<td>
    										@foreach($resource->permissions as $key=>$value)
    											<label class="label label-danger">
    											{{$key}}
    											</label>	&nbsp;								
    										@endforeach
											</ol>
										</td>

										<td class="text-right">
										@can('update-role')
										<a href="#"
											class="edit-modal btn btn-white"
											data-back_path = {{ route('role.index') }}
											data-id="{{$resource->id}}" data-title="{{$resource->title}}"
											data-slug="{{$resource->slug}}"
											data-path="{{ route('role.edit',['resource'=>$resource->id]) }}"
											data-toggle="modal" data-target="#dynamic-modal"><i
												class="fa fa-pencil"></i>
										</a>
										@endcan
										@can('delete-role')
										<a href="#"
											class="delete-modal btn btn-danger"
											data-toggle="modal"
											data-target="#dynamic-modal"
											data-back_path = "{{ route('role.index') }}"
											data-id="{{$resource->id}}" 
											data-title="{{$resource->title}}"
											data-slug="{{$resource->slug}}"><i class="fa fa-trash-o"></i>
										</a>
										@endcan
										</td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>

			</div>
		</div>
		<!-- page end-->
	</section>

</section>
<!--main content end-->


<!-- My Dynamic Modal -->
<div id="dynamic-modal" class="modal fade" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <span class=" text-info pull-left">{{env('APP_NAME', "FUSION POS")}}</span>
				<button type="button"
					class="btn btn-danger btn-md pull-right btn-round"
					data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<!-- Body content will come here -->
			</div>
			<div class="modal-footer">{{env('APP_NAME')}}</div>
		</div>
	</div>
</div>
<!-- End modal -->


<script type="text/javascript">

$("#newresource").click(function () {
    $.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data);
        },
    });
});

//form Delete function
$(document).on('click', '.delete-modal', function() {
	$('#dynamic-modal').addClass('modal-danger');
	$('.modal-body').html('<h4 class="text-danger">Delete this item? ' + $(this).data('id') + ':' + $(this).data('email')+'</h4><input type="hidden" class="id">');
	$('.modal-footer').html('<button type="button" id="calcel" data-dismiss="modal" class="btn btn-primary" >Cancel</button> <button type="button" id="delete" class="delete btn btn-danger" >Delete</button>');
	$('.id').text($(this).data('id'));
});

$('.modal-footer').on('click', '.delete', function() {
	 var url = '{{ route("delete-role", ["id"=>":id"]) }}';
	var back_url = $('.edit-modal').data('back_path');
	
    $.ajax({
        type: 'POST',
        url: url.replace(":id", $('.id').text()),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
                toastr.success('Record deleted successfully!', 'Notification', {"timeOut":1500});
                window.location = back_url;
        }
    });
});

//Action Edit/Update
$(document).on('click', '.edit-modal', function() {
	var name = $(this).data('name');
	var id = $(this).data('id');
	var title = $(this).data('title');
	var slug = $(this).data('slug');

	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            console.log(data);
            $('#dynamic-modal div.modal-body').html(data);
            $('input[name=id]').val(id);
            $('input[name=title]').val(title);
            $('input[name=slug]').val(slug);
        },
    });
});

//Show function
$(document).on('click', '.show-modal', function() {
	var name = $(this).data('name');
	var id = $(this).data('id');
	var email = $(this).data('email');
	var role = $(this).data('role');
	var active = $(this).data('active');
	var phone = $(this).data('phone');

	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data);
            $('#id').text(id);
            $('#name').text(name);
            $('#email').text(email);
            $('#role').text(role);
            $('#active').text(active);
            $('#phone').text(phone);
        },
    });
});

</script>
@endsection
