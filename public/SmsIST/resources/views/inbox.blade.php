@extends('layouts.main')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-3">
                <section class="panel">
                    <div class="panel-body">
                        <div class="btn-group">
                            <button id="btn_compose" data-path="{{ route('sms_compose') }}" class="btn btn-danger btn-block btn-round ">
                                <i class="fa fa-pencil"></i>&nbsp; Compose Message
                            </button>
                        </div>
                        <ul class="nav nav-pills nav-stacked mail-nav">
                            <li><a id="inbox_sms" data-path="{{ route('sms_inbox') }}"> <i class="fa fa-inbox"></i> Inbox <span class="label label-danger pull-right inbox-notification">{{ count($inbox_sms)}}</span></a></li>
                            <li><a id="sent_sms" data-path="{{ route('sms_sent') }}"> <i class="fa fa-envelope-o"></i> Send Messages<span class="label label-success pull-right inbox-notification">{{ count($sent_messages) }}</span></a></li>
                            <!-- <li><a id="draft_sms" data-path="{{ route('sms_draft') }}"> <i class="fa fa-file-text-o"></i> Drafts <span class="label label-info pull-right inbox-notification">123</span></a></a></li>
                            <li><a id="trash_sms" data-path="{{ route('sms_trash') }}"> <i class="fa fa-trash-o"></i> Trash</a></li> -->
                        </ul>
                    </div>
                </section>

                <section class="panel">
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked labels-info ">
                            <li>
                                <h4>Statistics</h4>
                            </li>
                            <li> <a href="#"> <i class="fa fa-comments-o text-success"></i>{{ $balance }}
                                    <p>Current SMS Balance</p>
                                </a> </li>
                            <!-- <li> <a href="#"> <i class="fa fa-comments-o text-danger"></i> iRon <p>Busy with coding</p></a> </li>
                            <li> <a href="#"> <i class="fa fa-comments-o text-muted "></i> Anjelina Joli <p>I out of control</p></a></li>
                            <li> <a href="#"> <i class="fa fa-comments-o text-muted "></i> Samual Daren <p>I am not here</p></a></li>
                            <li> <a href="#"> <i class="fa fa-comments-o text-muted "></i> Tis man <p>I do not think</p></a>  </li> -->
                        </ul>

                        <div class="btn-group">
                            <a id="btn_home" class="btn btn-info btn-block btn-round " href="javascript:;">
                                <i class="fa fa-home"></i>&nbsp; Home
                            </a>
                        </div>
                    </div>
                </section>
            </div>
            <!-- start of right content -->
            <div class="col-sm-9 sms_content">


                <div class="row">
                    <div class="col-sm-12">
                        <section class="panel">
                            <header class="panel-heading">Send Bulk SMS</header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="alert alert-block fade in hidden">
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <h4>
                                            <i class="icon-ok-sign"></i>
                                            <label id="error-header"></label>
                                        </h4>
                                        <label id="error-message"></label>
                                    </div>

                                    <form class="form-horizontal bucket-form" method="get">
                                        <div class="col-md-3">
                                            <label class="btn btn-round btn-block bg-info"><i class="fa fa-users"></i> &nbsp; Step 1 :
                                                Select Recipients</label>
                                            <br>
                                            <select id="recepient" name="recepient" class="form-control">
                                                <option value="0">Select recepients ...</option>
                                                @foreach($contact_groups as $group)
                                                <option value="{{$group->id}}">{{$group->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="btn btn-round btn-block bg-info"><i class="fa fa-envelope"></i>&nbsp;Step 2:
                                                Select Message Template</label>
                                            </label>
                                            <div class="" style="margin-left: 50px;">
                                                <br>
                                                <select id="template" name="template" class=" form-control has-errors">
                                                    <option value="0">Select template ...</option>
                                                    @foreach($templates as $template)
                                                    <option value="{{$template->id}}">{{$template->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="btn btn-round btn-block bg-info">Step 3</p>
                                            <br>
                                            <div class="form-group">
                                                <div class="text-center">
                                                    @can('send-sms')
                                                    <button type="button" class="btn btn-success btn-round" id="sendSms" data-sms="">
                                                    <i class="fa fa-check"></i> Send Message
                                                    </button>
                                                    @endcan
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <br>

                            </div>
                        </section>
                    </div>
                </div>


            </div>
            <!-- end of right content -->
        </div>

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<!-- End Modals -->
<script type="text/javascript">
    $("#btn_compose").click(function() {
        $.ajax({
            type: 'GET',
            url: $(this).data('path'),
            success: function(data) {
                $('div.sms_content').html(data);
            },
        });
    });

    $("#btn_home").click(function() {
        window.location.reload();
    });

    $("#inbox_sms").click(function() {
        $.ajax({
            type: 'GET',
            url: $(this).data('path'),
            success: function(data) {
                $('div.sms_content').html(data);
            },
        });
    });


    $("#sent_sms").click(function() {
        $.ajax({
            type: 'GET',
            url: $(this).data('path'),
            success: function(data) {
                $('div.sms_content').html(data);
            },
        });
    });

    $("#draft_sms").click(function() {
        $.ajax({
            type: 'GET',
            url: $(this).data('path'),
            success: function(data) {
                $('div.sms_content').html(data);
            },
        });
    });
    $("#trash_sms").click(function() {
        $.ajax({
            type: 'GET',
            url: $(this).data('path'),
            success: function(data) {
                $('div.sms_content').html(data);
            },
        });
    });


    // Create
    $("#sendSms").click(function() {
        if (confirm("Are you SURE you want to send the MESSAGE(s)?")) {
           console.log($('select[name=template]').val());

            $('.alert').removeClass('hidden');
            $('.alert').addClass('alert-info');
            $('#error-header').text("Processing");
            $('#error-message').text("Sending messages, please wait ...");
            $('#page-spinner-bar').fadeOut(1000);
            $('#page-spinner-bar').fadeIn(1000);
            $.ajax({
                type: 'POST',
                url: '{{ route("sms.send") }}',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'template': $('select[name=template]').val(),
                    'recepient': $('select[name=recepient]').val(),
                },
                ajaxSend: function() {},
                success: function(data) {
                    console.log(data);
                    if (data.error) {
                        // location.reload();
                        $('.alert').removeClass('hidden');
                        $('.alert').addClass('alert-danger');
                        $('#error-header').text("Error");
                        $('#error-message').text(data.error);
                        $('#page-spinner-bar').fadeOut(1000);
                        $('.alert').fadeOut(3000);
                    } else if (data.error2) {
                        // location.reload();
                        $('.alert').removeClass('hidden');
                        $('.alert').addClass('alert-danger');
                        $('#error-header').text("Error");
                        $('#error-message').text(data.error2);
                        $('#page-spinner-bar').fadeOut(1000);
                        $('.alert').fadeOut(3000);
                        window.location.reload();
                    } else {
                        console.log(data);
                        $('.alert').removeClass('hidden');
                        $('.alert').removeClass('alert-danger');
                        $('#error-header').text("Success!");
                        $('#error-message').text('Message(s) sent successfully!');
                        $('#page-spinner-bar').fadeOut(1000);
                        $('.alert').fadeOut(3000);
                    }
                },
                default: function() {
                    $('#page-spinner-bar').fadeOut(1000);
                    $('.alert').fadeOut(3000);
                },
                error: function() {
                    $('#page-spinner-bar').fadeOut(1000);
                    $('.alert').fadeOut(3000);
                }
            });
            // window.location.reload();
        }
    });

    $(document).on('click', '.create-modal', function() {
        $('#create').modal('show');
        $('.form-horizontal').show();
        $('.modal-title').text('Create bulk booking');
    });

    //Action Create
    $('#add_booking').click(function() {
        var url = "{{route('booking_book')}}";
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                '_token': $('input[name=_token]').val(),
                'name': $('input[name=name]').val(),
                'detail': $('textarea[name=detail]').val(),
                'start': $('input[name=from]').val(),
                'end': $('input[name=to]').val(),
                'user_id': $('input[name=user_id]').val(),
                'owner': $('input[name=owner]').val(),
                'created_by': $('input[name=user_name]').val()
            },
            success: function(data) {
                console.log('booking POST successful');
                if (data.errors) {
                    if (data.errors.name) {
                        $('.error-name').removeClass('hidden');
                        $('.error-name').addClass('text-danger');
                        $('.input-name').addClass('parsley-error');
                        $('.error-name').text(data.errors.name);
                    } else if (data.errors.detail) {
                        $('.error-detail').removeClass('hidden');
                        $('.error-detail').addClass('text-danger');
                        $('.input-detail').addClass('parsley-error');
                        $('.error-detail').text(data.errors.title);
                    }
                } else {
                    console.log(data);
                    location.reload();
                }
                location.reload();
            },
            error: function(xhr, status, error) {
                if (xhr.status == 500) {
                    var errorMessage = "Booking already done for the selected date range";
                    $('.error-date').removeClass('hidden');
                    $('.error-date').addClass('text-danger');
                    $('.date').addClass('has-error');
                    $('.error-date').text(errorMessage);
                    alert('Error - ' + errorMessage);
                } else {
                    var errorMessage = xhr.status + ': ' + xhr.statusText + xhr.statusMessage
                    alert('Error - ' + errorMessage);
                }
            },
        });
    });

    // Action Edit/Update
    $(document).on('click', '.edit-modal', function() {
        $('#footer_action_button').text(" Update");
        $('#footer_action_button').removeClass('fa fa-trash');
        $('.actionBtn').addClass('btn-success pull-right');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Booking Edit');
        $('.deleteContent').hide();
        $('.form-horizontal').show();

        $('#fid').val($(this).data('id'));
        $('#nam1').val($(this).data('name'));
        $('#tit1').val($(this).data('title'));
        $('#det1').text($(this).data('detail'));
        $('#crd1').text($(this).data('created_at'));
        $('#lsu1').text($(this).data('updated_at'));
        $('#myModal').modal('show');

    });

    $('.modal-footer').on('click', '.edit', function() {
        console.log('Samdoh edit button click');
        $.ajax({
            type: 'PUT',
            url: '/booking/' + $("#fid").val(),
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#fid").val(),
                'name': $("#nam1").val(),
                'detail': $('#det1').val()
            },
            success: function(data) {
                if (data.errors) {
                    if (data.errors.name) {
                        $('.error-name').removeClass('hidden');
                        $('.error-name').addClass('text-danger');
                        $('.input-name').addClass('parsley-error');
                        $('.error-name').text(data.errors.name);
                    } else if (data.errors.detail) {
                        $('.error-detail').removeClass('hidden');
                        $('.error-detail').addClass('text-danger');
                        $('.input-detail').addClass('parsley-error');
                        $('.error-detail').text(data.errors.detail);
                    }
                } else {
                    $('.post' + data.id).replaceWith(" " +
                        "<tr class='post" + data.id + "'>" +
                        "<td>" + data.id + "</td>" +
                        "<td>" + data.detail + "</td>" +
                        "<td>" + data.created_at + "</td>" +
                        "<td><button class='show-modal btn btn-success' data-id='" + data.id +
                        "' data-body='" + data.body +
                        "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-success' data-id='" +
                        data.id + "' data-body='" + data.body +
                        "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger' data-id='" +
                        data.id + "' data-body='" + data.body +
                        "'><span class='glyphicon glyphicon-trash'></span></button></td>" +
                        "</tr>");
                    location.reload();
                }
            }
        });
    });

    // form Delete function
    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete Post');
        $('.id').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.title').html($(this).data('title'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'DELETE',
            url: '/booking/' + $('.id').text(),
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $('.id').text()
            },
            default: function(data) {
                $('.post' + $('.id').text()).remove();
                window.location.reload();
            }
        });
    });

    // Show function
    $(document).on('click', '.show-modal', function() {
        $('#show').modal('show');
        $('#i').text($(this).data('id'));
        $('#nam').text($(this).data('name'));
        $('#tit').text($(this).data('title'));
        $('#det').text($(this).data('detail'));
        $('#crd').text($(this).data('created_at'));
        $('#lsu').text($(this).data('updated_at'));
        $('.modal-title').text('Show Booking');
    });


    // $( "select[name='template1']" ).change(function(){
    //     console.log('selected');
    //     alert("Handler for .change() called.");
    // });
</script>



@endsection