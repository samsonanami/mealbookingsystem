@extends('layouts.form') @section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Booking Checklist Panel
                        <span class="tools pull-right">
                            <button id-="snap" class="snap btn btn-success btn-sm border" onclick="printDiv('salesReportIframe')">
                                <i class="fa fa-print" data-dismiss="modal"></i> &nbsp; Print
                            </button>
                            <button type="button" class="btn btn-sm btn-success border " data-toggle="modal" data-target="#input-modal">
                                <i class="fa fa-filter"></i>&nbsp; Input
                            </button>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table editable-table ">
                            <div class="col-md-12" id="salesReportIframe">
                                <!-- <iframe name="report_pane" id="salesReportIframe" width="100%" height="800px">Hello</iframe> -->
                                <!-- <div id="salesReportPrintArea"> -->
                                <div id="salesReportIframe" width="100%" height="100%">
                                    <div class="col-md-12" style="text-align:center;">
                                        <img class="margin10" alt="" height="42px" src="{{ asset('bucket/images/stl-logo.png') }}" />
                                        <h4><b>Booking Checklist</b></h4>
                                        <hr>
                                    </div>
                                    <?php $report->render(); ?>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <!-- page end-->

    </section>



    <!--Input Modal -->
    <div class="modal fade" id="input-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left" id="exampleModalLongTitle">Input Controls</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-8">
                        <h4 class="control-label">Week Starting Date</h4>
                        <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" class="input-append date dpYears">
                            <input type="text" readonly="" value="{{ today()->format('d-m-Y') }}" size="16" class="form-control input-large" name="start_date">
                            <span class="input-group-btn add-on">
                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        <button type="button" class="btn btn-success border btn-block load-sales-report-view" id="load-sales-report-view">
                            <i class="fa fa-refresh">&nbsp; </i>&nbsp; Refresh Report
                        </button>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-md border " data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Exit</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        // Craete module
        $(".load-sales-report-view").click(function() {
            console.log("btn clicked");
            $.ajax({
                type: 'POST',
                url: '{{ route("rpt-booking-checklist") }}',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'start_date': $('input[name=start_date]').val(),
                },
                success: function(data) {
                    console.log(data);
                    $('#salesReportIframe').html(data);
                }
            });
        });

        function printDiv(divName) {
            console.log(divName);
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
            location.reload();
        }
    </script>




    @endsection