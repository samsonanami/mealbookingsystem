<form role="form">
    @csrf
    <div class="form-group">
        <label for="id">ID<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="id" name="id" value="{{$item->id}}" disabled>
        <label class="error-id"></label>
    </div>
    <div class="form-group">
        <label for="title">Contact Group Name<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="title" name="title" value="{{$item->title}}">
        <label class="error-title"></label>
    </div>

    <div class="form-group">
        <label for="password">Details</label>
        <textarea class="form-control" id="details" name="details">{{$item->details}}</textarea>
        <label class="error-details"></label>
    </div>

    <div class="form-group">
        <label for="password">Address</label>
        <textarea class="form-control" id="address" name="address">{{$item->address}}</textarea>
        <label class="error-address"></label>
    </div>

    <div class="form-group">
        <label for="active">Active<i class="text-danger">*</i></label>
        <select class="form-control" id="active" name="active">
            <option value=1 @if($item->active == 1) selected @endif>Yes</option>
            <option value="0" @if($item->active == 0) selected @endif>No</option>
        </select>
        <label class="error-active"></label>
    </div>

    <h4>Contacts</h4>
    <span class="border border-success rounded col-md-12 clearfix">
        @foreach($students as $student)
        <div class="students">
            <label class="form-group">
                <input type="checkbox" value="{{$student->id}}"  @if($item->students->contains($student->id)) checked @endif>{{$student->title}}
            </label>
        </div>
        @endforeach
    </span>
    
    <button type="button" id="calcel" data-dismiss="modal" class="btn bg-default pull-left btn-round"><i class="fa fa-times"></i>Exit</button>
    <button type="button" id="add_contact_group" class="btn bg-success pull-right btn-round"><i class="fa fa-check"></i>Submit</button>

</form>


<script type="text/javascript">
    // Craete module

    $("#add_contact_group").click(function() {
        var url = '{{ route("contact-group.update", ["contact_group"=>":contact_group"]) }}';
        var selectedStudents = {
            "students": []
        };

        $(".students :checked").each(function() {
            selectedStudents['students'].push($(this).val());
        });
        var students = selectedStudents['students'];

        console.log(students);

        $.ajax({
            type: 'PATCH',
            url: url.replace(":contact_group", $('input[name=id]').val()),
            data: {
                '_token': $('input[name=_token]').val(),
                'title': $('input[name=title]').val(),
                'address': $('textarea[name=address]').val(),
                'details': $('textarea[name=details]').val(),
                'active': $('select[name=active]').val(),
                'students': students,
            },
            success: function(data) {
                if (data.errors) {
                    console.log(data.errors);
                    if (data.errors.title) {
                        $('.error-title').removeClass('hidden').addClass('text-danger');
                        $('#title').addClass('has-error');
                        $('.error-title').text(data.errors.title);
                    } else if (data.errors.details) {
                        $('.error-details').removeClass('hidden').addClass('text-danger');
                        $('#details').addClass('has-error');
                        $('.error-details').text(data.errors.details);
                    } else if (data.errors.address) {
                        $('.error-address').removeClass('hidden').addClass('text-danger');
                        $('#address').addClass('has-error');
                        $('.error-address').text(data.errors.address);
                    } else if (data.errors.active) {
                        $('.error-active').removeClass('hidden').addClass('text-danger');
                        $('#active').addClass('has-error');
                        $('.error-active').text(data.errors.active);
                    }
                } else {
                    console.log(data);
                    location.reload();
                }
            },
        });
    });
</script>