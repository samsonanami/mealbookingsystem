<section class="panel">
 	<header class="panel-heading wht-bg">
 		<h4 class="gen-case">Inbox Messages ({{count($messages)}})
 			<div class="pull-right">
 				<button type="button" id="export" class="btn bg-success btn-round" data-path="{{ route('student_export') }}">
 					<i class="fa fa-file"></i> Export Excel
 				</button>
 			</div>
 		</h4>
 	</header>
 	<div class="panel-body minimal">
 		<div class="table-inbox-wrap ">
 			<div class="col-sm-12">
 				<section class="panel">
 					<div class="panel-body">
 						<div class="adv-table editable-table ">

 							<div class="space15"></div>
 							<table class="table table-striped table-hover table-bordered" id="editable-sample">
 								<thead>
 									<tr>
 										<th>No</th>
 										<!-- <th>MessageId</th> -->
 										<th>Title</th>
 										<th>Phone</th>
 										<th>Cost</th>
 										<th>Status</th>
 										<th>Message</th>
 										<th>Date</th>
 									</tr>
 								</thead>
 								<tbody>
 									@foreach($messages as $message)
 									<tr class="">
 										<td>{{$message->id}}</td>
 										<!-- <td>{{$message->messageId}}</td> -->
 										<td>{{$message->title}}</td>
 										<td>{{$message->number}}</td>
 										<td>{{$message->cost}}</td>
 										<td>
 											@if($message->status == 'Success')
 											<label class="label btn-round label-success">{{$message->status}}</label>
 											@else
 											<label class="label btn-round label-danger">{{$message->status}}</label>
 											@endif
 										</td>
 										<td>{{$message->Message}}</td>
 										<td>{{$message->created_at->diffForHumans()}}</td>
 									</tr>
 									@endforeach
 								</tbody>
 							</table>
 						</div>
 					</div>
 				</section>

 			</div>
 		</div>
 </section>

 <!-- END JAVASCRIPTS -->
 <script>
 	jQuery(document).ready(function() {
 		EditableTable.init();
 	});
 </script>