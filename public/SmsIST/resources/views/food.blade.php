@extends('layouts.main') @section('content')
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->

		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Food Table <span class="tools pull-right"> <a
							href="javascript:;" class="fa fa-chevron-down"></a> <a
							href="javascript:;" class="fa fa-cog"></a> <a href="javascript:;"
							class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
									<button type="button" id="editabe-sample_new"
										class="create-modal btn btn-success">
										<i class="fa fa-plus"></i> Add New
									</button>
									<button type="button" id="editabe-sample_new"
										class="create-modal btn btn-success">
										<i class="fa fa-upload"></i> Import
									</button>
								</div>
								<div class="btn-group pull-left">
									<button class="btn btn-default dropdown-toggle"
										data-toggle="dropdown">
										Tools <i class="fa fa-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li><a href="#">Print</a></li>
										<li><a href="#">Save as PDF</a></li>
										<li><a href="#">Export to Excel</a></li>
									</ul>
								</div>
							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Detail</th>
										<th>Date Created</th>
										<th>Late Updated</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($foods as $food)
									<tr class="">
										<td>{{$food->id}}</td>
										<td>{{$food->name}}</td>
										<td>{{$food->detail}}</td>
										<td>{{$food->created_at}}</td>
										<td>{{$food->updated_at}}</td>
										<td class="text-right"><a href="#"
											class="show-modal btn btn-primary " data-id="{{$food->id}}"
											data-name="{{$food->name}}" data-detail="{{$food->detail}}"
											data-created_at="{{$food->created_at}}"
											data-updated_at="{{$food->updated_at}}"> <i class="fa fa-eye"></i>
										</a> <a href="#" class="edit-modal btn btn-info"
											data-id="{{$food->id}}" data-name="{{$food->name}}"
											data-detail="{{$food->detail}}"
											data-created_at="{{$food->created_at}}"
											data-updated_at="{{$food->updated_at}}"> <i
												class="fa fa-pencil"></i>
										</a> <a href="#" class="delete-modal btn btn-danger"
											data-id="{{$food->id}}" data-name="{{$food->name}}"
											data-detail="{{$food->detail}}"
											data-created_at="{{$food->created_at}}"
											data-updated_at="{{$food->updated_at}}"> <i
												class="fa fa-trash-o"></i>
										</a></td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
		</div>
		<!-- page end-->
	</section>
</section>
<!--main content end-->


<!-- Modals -->


<div id="create" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Name :</label>
						<div class="col-sm-10">
							<input id="name" type="text" class="form-control input-name"
								id="name" name="name" placeholder="food name here" required>
							<p class="error-name hidden parsley-required" role="alert">
						
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Detail :</label>
						<div class="col-sm-10">
							<textarea id="detail" class="form-control input-detail"
								id="detail" name="detail" placeholder="Description here"
								required></textarea>
							<p class="error-detail hidden parsley-required" role="alert">
						
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="submit" id="add">
					<span class="fa fa-check">&nbsp; </span>Submit
				</button>
				<button class="btn btn-info" type="button" data-dismiss="modal">
					<span class="fa fa-times"></span>&nbsp; Close
				</button>
			</div>
		</div>
	</div>
</div>
</div>



<div id="show" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">ID :</label> <span id="i" class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Name :</label> <span id="nam" class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Detail :</label> <span id="det" class="text-primary"></b>
				
				</div>
				<div class="form-group">
					<label for="">Created Date :</label> <span id="crd"
						class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Last Updated :</label> <span id="lsu"
						class="text-primary"></span>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="modal">
					<div class="form-group">
						<label class="control-label col-sm-2" for="id">ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="fid" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="name">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nam1">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Detail</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="det1"></textarea>
						</div>
					</div>
				</form>


				<div class="deleteContent">
					Are You sure want to delete <span class="title"></span>? <span
						class="hidden id"></span>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn actionBtn" data-dismiss="modal">
					<span id="footer_action_button" class="glyphicon"></span>
				</button>
				<button type="button" class="btn btn-info" data-dismiss="modal">
					<span class="glyphicon glyphicon"></span>close
				</button>
			</div>
		</div>
	</div>
</div>



<!-- End Modals -->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<script type="text/javascript">
$(document).on('click', '.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Add Food');
});

//Action Create

$("#add").click(function() {
    $.ajax({
        type: 'POST',
        url: '/food',
        data: {
            '_token': $('input[name=_token]').val(),
            'name': $('input[name=name]').val(),
            'detail': $('textarea[name=detail]').val()
        },
        success: function(data) {
            if (data.errors) {
                if (data.errors.name) {
                    $('.error-name').removeClass('hidden');
                    $('.error-name').addClass('text-danger');
                    $('.input-name').addClass('parsley-error');
                    $('.error-name').text(data.errors.name);
                }else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.title);
                }
            } else {
                $('#success-msg').removeClass('hidden');
                $('#datatable').append("<tr class='post" + data.id + "'>" +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.name + "</td>" +
                    "<td>" + data.detail + "</td>" +
                    "<td>" + data.created_at + "</td>" +
                    "<td><button class='show-modal btn btn-info' data-id='" + data.id +
                    "' data-name='" + data.name + "' data-title='" + data.title +
                    "' data-body='" + data.body +
                    "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-info' data-id='" +
                    data.id + "' data-name='" + data.name + "' data-detail='" + data.detail +
                    "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger' data-id='" +
                    data.id + "' data-title='" + data.name + "' data-body='" + data.detail +
                    "'><span class='glyphicon glyphicon-trash'></span></button></td>" +
                    "</tr>");
                location.reload();
            }
        },
    });
    $('.input-name').removeClass('parsley-error');
    $('.error-name').addClass('hidden');
    $('.input-detail').removeClass('parsley-error');
    $('.error-detail').addClass('hidden');
});

// Action Edit/Update
$(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-primary');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Food Edit');
    $('.deleteContent').hide();
    $('.form-horizontal').show();

    $('#fid').val($(this).data('id'));
    $('#nam1').val($(this).data('name'));
    $('#tit1').val($(this).data('title'));
    $('#det1').text($(this).data('detail'));
    $('#crd1').text($(this).data('created_at'));
    $('#lsu1').text($(this).data('updated_at'));
    $('#myModal').modal('show');

});

$('.modal-footer').on('click', '.edit', function() {
    console.log('Samdoh edit button click');
    $.ajax({
        type: 'PUT',
        url: '/food/'+$("#fid").val(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'name': $("#nam1").val(),
            'detail': $('#det1').val()
        },
        success: function(data) {
            if (data.errors) {
                if (data.errors.name) {
                    $('.error-name').removeClass('hidden');
                    $('.error-name').addClass('text-danger');
                    $('.input-name').addClass('parsley-error');
                    $('.error-name').text(data.errors.name);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.detail);
                }
            } else {
                $('.post' + data.id).replaceWith(" " +
                    "<tr class='post" + data.id + "'>" +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.detail + "</td>" +
                    "<td>" + data.created_at + "</td>" +
                    "<td><button class='show-modal btn btn-info' data-id='" + data.id +
                    "' data-body='" + data.body +
                    "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-info' data-id='" +
                    data.id + "' data-body='" + data.body +
                    "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger' data-id='" +
                    data.id + "' data-body='" + data.body +
                    "'><span class='glyphicon glyphicon-trash'></span></button></td>" +
                    "</tr>");
                location.reload();
            }
        }
    });
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('#footer_action_button').removeClass('glyphicon-check');
    $('#footer_action_button').addClass('glyphicon-trash');
    $('.actionBtn').removeClass('btn-primary');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: '/food/' + $('.id').text(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
            $('.post' + $('.id').text()).remove();
            location.reload();
        }
    });
});

// Show function
$(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#i').text($(this).data('id'));
    $('#nam').text($(this).data('name'));
    $('#tit').text($(this).data('title'));
    $('#det').text($(this).data('detail'));
    $('#crd').text($(this).data('created_at'));
    $('#lsu').text($(this).data('updated_at'));
    $('.modal-title').text('Show Food');
});
</script>



@endsection
