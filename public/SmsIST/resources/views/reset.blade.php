@extends('layouts.login')

@section('content')
<div class="container">

<form  action="" class="form-signin">
<h2 class="form-signin-heading">
    <a target="_blank" href="http://www.stl-horizon.com">
        <img class="margin10" alt="" height="32" src="{{ asset('bucket/images/eHorizon.png') }}" />
        <h5><strong>MealBooking</strong></h5>
    </a>
</h2>
 <h4 class="text-center"><strong>{{ __('Reset Password') }}</strong></h4>

 			<div class="login-wrap">
            <div class="user-login-info">
                        @csrf
                        <input type="hidden" name="id" value="{{ $id }}">
						<label for="email" class="pull-left">{{ __('E-Mail') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <label for="password" class="pull-left">{{ __('Password') }}</label>

                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                         <label for="password" class="pull-left">{{ __('Confirm Password') }}</label>
                         <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
    
                        <button type="button" class="btn btn-success pull-righ btn-block reset-password">
                            {{ __('Reset Password') }}
                        </button>
               </div>
               </div>
           </form>
         </div>
         
@endsection





