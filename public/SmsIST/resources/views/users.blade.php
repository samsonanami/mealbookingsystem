@extends('layouts.main') @section('content')
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						User Table <span class="tools pull-right"> <a href="javascript:;"
							class="fa fa-chevron-down"></a> <a href="javascript:;"
							class="fa fa-cog"></a> <a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
									<button
									type="button" id="newuser"
									class="btn btn-success"
									data-toggle="modal"
									data-target="#dynamic-modal"
									data-path="{{ route('user.create') }}"
									>
									<i class="fa fa-plus"></i> Add New User
									</button>
									<button
									type="button" id="import"
									class="btn btn-success"
									data-toggle="modal"
									data-target="#dynamic-modal"
									data-path="{{ route('user_create_import') }}"
									>
									<i class="fa fa-download"></i> Import Excel
									</button>
									<button
									type="button" id="export"
									class="btn btn-success"
									data-path="{{ route('user_export') }}"
									>
									<i class="fa fa-file"></i> Export Excel
									</button>
								</div>

							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Payroll No</th>
										<th>Email</th>
										<th>Role</th>
										<th>Phone</th>
										<th>Active</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
									@php
    									if($user->roles->first()){
    										$user_role = $user->roles->first();
    										$role = $user_role->title;
    									}else{
    										$role = 'Not set';
    									}
    								@endphp
									<tr class="">
										<td>{{$user->id}}</td>
										<td>{{$user->name}}</td>
										<td>{{$user->payroll}}</td>
										<td>{{$user->email}}</td>
										<td>{{$role}}</td>
										<td>{{$user->phone}}</td>
										<td>{{$user->active}}</td>
										<td class="text-right"><a href="#"
											class="show-modal btn btn-default "
											data-id="{{$user->id}}"
											data-name="{{$user->name}}"
											data-email="{{$user->email}}"
											data-role="{{$user_role->id}}"
											data-phone="{{$user->phone}}"
											data-payroll="{{$user->payroll}}"
											data-active="{{$user->active}}"
											data-path="{{ route('user.show',['user'=>$user->id]) }}"
											data-toggle="modal"
											data-target="#dynamic-modal"
											> <i class="fa fa-eye"></i>
										</a> <a href="#" class="edit-modal btn btn-default"
											data-id="{{$user->id}}" 
											data-name="{{$user->name}}"
											data-username="{{$user->username}}"
											data-email="{{$user->email}}"
											data-role="{{$user_role->id}}"
											data-phone="{{$user->phone}}" 
											data-active="{{$user->active}}"
											data-path="{{ route('user.edit',['user'=>$user->id]) }}"
											data-payroll="{{$user->payroll}}"
											data-toggle="modal"
											data-target="#dynamic-modal"
											>
											<i class="fa fa-pencil"></i>
										</a>
										<a href="#" class="delete-modal btn btn-danger"
											data-id="{{$user->id}}" 
											data-name="{{$user->name}}"
											data-email="{{$user->email}}" 
											data-role="{{$user_role->id}}"
											data-phone="{{$user->phone}}"
											data-payroll="{{$user->payroll}}"
											data-active="{{$user->active}}">
											<i class="fa fa-trash-o"></i>
										</a></td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>

			</div>
		</div>
		<!-- page end-->
	</section>

</section>
<!--main content end-->


<!-- My Dynamic Modal -->
<div id="dynamic-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class=" text-info pull-left">{{env('APP_NAME', 'E-Meals')}}</span>
                <button type="button" class="btn btn-danger btn-md pull-right btn-round"
                    data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <!-- Body content will come here -->
            </div>
            <div class="modal-footer">
                {{env('APP_NAME')}}
            </div>
        </div>
    </div>
</div>
<!-- End modal -->

<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 -->

<script type="text/javascript">

$("#newuser").click(function () {
    $.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            console.log(data);
            $('#dynamic-modal div.modal-body').html(data);
        },
    });
});


//export 
$("#export").click(function () {
	window.location = $(this).data('path');
});


//import 
$("#import").click(function () {
    $.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
        },
    });
});


//form Delete function
$(document).on('click', '.delete-modal', function() {
   $('#dynamic-modal').addClass('modal-danger');
   $('.modal-body').html('<h4 class="text-danger">Delete this item? ' + $(this).data('id') + ':' + $(this).data('name')+'</h4><input type="hidden" id="id-user">');
    $('.modal-footer').html('<button type="button" id="calcel" data-dismiss="modal" class="btn btn-primary" >Cancel</button> <button type="button" id="delete" class="delete btn btn-danger" >Delete</button>');
    $('#dynamic-modal').modal('show');
    $('#id-user').text($(this).data('id'));
});

$('.modal-footer').on('click', '.delete', function() {
	 var url = '{{ route("user.destroy", ["id"=>":id"]) }}';
    $.ajax({
        type: 'DELETE',
        url: url.replace(":id", $('#id-user').text()),
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            console.log(data);
            location.reload();
        }
    });
});
//Action Edit/Update
$(document).on('click', '.edit-modal', function() {
	var name = $(this).data('name');
	var payroll = $(this).data('payroll');
	var id = $(this).data('id');
	var email = $(this).data('email');
	var role = $(this).data('role');
	var active = $(this).data('active');
	var phone = $(this).data('phone');

	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            console.log(data);
            $('#dynamic-modal div.modal-body').html(data);
            $('input[name=id]').val(id);
            $('input[name=name]').val(name);
            $('input[name=payroll]').val(payroll);
            $('input[name=email]').val(email);
            $('select[name=role]').val(role);
            $('select[name=username]').val(username);
            $('select[name=active]').val(active);
            $('input[name=phone]').val(phone);
        },
    });
});

//Show function
$(document).on('click', '.show-modal', function() {
	var name = $(this).data('name');
	var payroll = $(this).data('payroll');
	var id = $(this).data('id');
	var email = $(this).data('email');
	var role = $(this).data('role');
	var active = $(this).data('active');
	var phone = $(this).data('phone');
	var payroll = $(this).data('payroll');

	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data);
            $('#id').text(id);
            $('#name').text(name);
            $('#payroll').text(payroll);
            $('#email').text(email);
            $('#role').text(role);
            $('#active').text(active);
            $('#phone').text(phone);
        },
    });
});

</script>
@endsection
