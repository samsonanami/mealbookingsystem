<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"/>

<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'MealBS') }}</title>

<link href="{{ asset('toastr/toastr.min.css') }}" rel="stylesheet" />
@toastr_css

<!-- Scripts -->

<link href="{{ asset('bucket/bs3/css/bootstrap.min.css') }}"
	rel="stylesheet">
<link href="{{ asset('bucket/css/bootstrap-reset.css') }}"
	rel="stylesheet">
<link href="{{ asset('bucket/font-awesome/css/font-awesome.css') }}"
	rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="{{ asset('bucket/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('bucket/css/style-responsive.css') }}"
	rel="stylesheet" />

</head>
</head>

<body class="login-body">
	<!-- BEGIN PAGE SPINNER -->
	<div class="page-spinner-bar" id="page-spinner-bar">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>
	<!-- END PAGE SPINNER -->


	@jquery @toastr_js @toastr_render
	<!--  For toastr -->
	<script src="{{ asset('toastr/jquery.min.js') }}"></script>
	<script src="{{ asset('toastr/toastr.min.js') }}"></script>



	@yield('content')


	<script src="{{ asset('bucket/js/jquery.js') }}"></script>
	<script src="{{ asset('bucket/bs3/js/bootstrap.min.js') }}"></script>
	<script>
    	$(document).ready(function(){
    		$('.alert-success').fadeIn().delay(3000).fadeOut();
    		$('.alert-danger').fadeIn().delay(3000).fadeOut();
    		$('.alert-info').fadeIn().delay(3000).fadeOut();
          });
    </script>


	<script type="text/javascript">

         $(".reset-password").click(function() {
             $.ajax({
                 type: 'POST',
                 url: '{{ route('password-reset-local') }}',
                 data: {
                     '_token': $('input[name=_token]').val(),
                     'password': $('input[name=password]').val(),
                     'password_confirmation': $('input[name=password_confirmation]').val(),
                     'email': $('input[name=email]').val(),
                     'id': $('input[name=id]').val(),
                 },
                 success: function(data) {
                     if (data.errors) {
                         console.log(data.errors);
                         if (data.errors.email) {
                             $('.error-email').removeClass('hidden').addClass('text-danger');
                             $('#email').addClass('has-error');
                             $('.error-email').text(data.errors.email);
                         } else if (data.errors.password) {
                             $('.error-password').removeClass('hidden').addClass('text-danger');
                             $('#password').addClass('has-error');
                             $('.error-password').text(data.errors.password);
                         }else if (data.errors.password_confirmation) {
                             $('.error-password_confirmation').removeClass('hidden').addClass('text-danger');
                             $('#password_confirmation').addClass('has-error');
                             $('.error-password_confirmation').text(data.errors.password_confirmation);
                         }
                     } else {
                         console.log(data);
                         window.location.href= '/home';
                     }
                 },
             });
         });


		</script>
		
			<script type="text/javascript">
		toastr.options = {"closeButton":true,"closeClass":"toast-close-button","closeDuration":300,"closeEasing":"swing","closeHtml":"<button><i class=\"fa fa-times\"><\/i><\/button>","closeMethod":"fadeOut","closeOnHover":true,"containerId":"toast-container","debug":false,"escapeHtml":false,"extendedTimeOut":10000,"hideDuration":1000,"hideEasing":"linear","hideMethod":"fadeOut","iconClass":"toast-info","iconClasses":{"error":"toast-error","info":"toast-info","success":"toast-success","warning":"toast-warning"},"messageClass":"toast-message","newestOnTop":false,"onHidden":null,"onShown":null,"positionClass":"toast-top-center","preventDuplicates":true,"progressBar":true,"progressClass":"toast-progress","rtl":false,"showDuration":300,"showEasing":"swing","showMethod":"fadeIn","tapToDismiss":true,"target":"body","timeOut":5000,"titleClass":"toast-title","toastClass":"toast"};
	</script>


</body>

</html>