<aside>
            <div id="sidebar" class="nav-collapse">
                <!-- sidebar menu start-->
                <div class="leftside-navigation">
                    <ul class="sidebar-menu" id="nav-accordion">
                        {{-- Modules Load --}}
                        @foreach($modules as $module)
                        @can('view-'.$module->title, 'view-'.$module->title)
                        <li>
                            <a href="{{ route($module->link, ['id'=> $module->id]) }}">
                            <i class="{{$module->icon}}"></i>
                                <span>{{$module->title}}</span>
                            </a>
                            <?php $children = $module->children; ?>
                            @if($children)
                                @foreach($children as $child)
                                    <ul class="sub" style="display: block;">
                                	@can('view-'.$child->title, 'view-'.$child->title)
                                        <li><a href="{{ route($child->link, ['id'=> $child->id]) }}"><i class="{{$child->icon}}"></i>{{$child->title}}</a></li>
                                    @endcan
                                    </ul>
                                @endforeach
                             @endif
                        </li>
                        @endcan
                        @endforeach
                        
                          <li>
                          <a href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                    class="fa fa-user"></i> Log Out</a>
                          </li>
                    </ul>
                </div>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->