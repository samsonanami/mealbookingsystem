<aside>
	<div id="sidebar" class="nav-collapse">
		<!-- sidebar menu start-->
		<div class="leftside-navigation">

			<ul class="sidebar-menu" id="nav-accordion">
				<li><a href="{{ route('home') }}"> <i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a></li>
				<li class="sub-menu"><a href="javascript:;"> <i class="fa fa-laptop"></i>
						<span>Menu</span>
				</a>
					<ul class="sub">
						<li><a href="{{ route('menu.index') }}">Menu</a></li>
						<li><a href="{{ route('menu_calendar') }}">Menu Calendar</a></li>
					</ul></li>
				<li class="sub-menu"><a href="javascript:;" class=""> <i
						class="fa fa-book"></i> <span>Booking</span>
				</a>
					<ul class="sub">
						<li><a href="{{ route('booking.index') }}">Meal Booking</a></li>
						<li><a href="{{ route('booking_admin.index') }}">Admin Booking</a></li>
					</ul></li>
				<li class="sub-menu"><a href="javascript:;"> <i
						class="fa fa-envelope"></i> <span>SMS</span>
				</a>
					<ul class="sub">
						<li><a href="{{ route('sms.index') }}">SMS Reminders</a></li>
					</ul></li>
				<li class="sub-menu"><a href="javascript:;"> <i class="fa fa-tasks"></i>
						<span>Administration</span>
				</a>
					<ul class="sub">
						<li><a href="{{ route('meal.index') }}">Meals</a></li>
						<li><a href="{{ route('user.index') }}">Users</a></li>
						<li><a href="{{ route('right.index') }}">User Rights</a></li>
						<li><a href="{{ route('user_group.index') }}">User Groups</a></li>
						<li><a href="{{ route('booking.index') }}">Bookings</a></li>
						<li><a href="{{ route('menu.index') }}">Menu Setup</a></li>
						<li><a href="{{ route('student.index') }}">Students</a></li>
						<li><a href="{{ route('caterer.index') }}">Caterers</a></li>
						<li><a href="{{ route('visitor.index') }}">Visitors</a></li>
					</ul></li>
				<li class="sub-menu"><a href="javascript:;"> <i
						class=" fa fa-bar-chart-o"></i> <span>Reports</span>
				</a>
					<ul class="sub">
						<li><a href="{{ route('report_meal') }}">Booking Report</a></li>
						<!--                                 <li><a href="">Today Booked Meals</a></li> -->
					</ul></li>

				<li><a href="login.html"
					onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
						<i class="fa fa-user"></i> <span>Logout</span>
				</a></li>
			</ul>
		</div>
		<!-- sidebar menu end-->
	</div>
</aside>
<!--sidebar end-->