<form role="form">
    @csrf
    <div class="alert alert-danger hidden">
        <label class="error"></label>
    </div><br>
    <div class="form-group">
        <label for="phone">Select well formatted XML File</label>
        <input type="file" name="file" class="file">
        <label class="error-file"></label>
    </div>
    <a href="{{ url('download/mealbs_users_import_template.xlsx') }}">Download Sample Template</a>
    <button type="button" id="import_file" class="btn btn-success pull-right"><i class="fa fa-check" ></i>Import User Data</button>
    <br>
    
    <br>
</form>


<script type="text/javascript">

$("#import_file").click(function() {
	$('.page-spinner-bar').fadeIn(1000);
	console.log('button clicked');
	var file_data = $('.file').prop('files')[0];
    if(file_data != undefined) {
    	 var form_data = new FormData();                  
         form_data.append('import_file', file_data);
         form_data.append('_token', $('input[name=_token]').val());
         $.ajax({
             type: 'POST',
             url: '{{ route('user_import') }}',
             contentType: false,
             processData: false,
             data: form_data,
             success:function(response) {
                 console.log(response);
                 if(response == 'success') {
                     alert('File uploaded successfully.');
                 } else if(response == 'false') {
                     $('.alert').removeClass('hidden');
                     $('.error').text('Error - Invalid file type.'); 
                     $('.alert').fadeOut(3000);
                 } else {
                	 $('.alert').removeClass('hidden');
                     $('.error').text('Error - Something went wrong. Please try again.'); 
                     $('.alert').fadeOut(3000);
                 }
                 $('.file').val('');
             	$('.page-spinner-bar').fadeOut(1500);
                 location.reload();
             },
             error: function(xhr , status, error){
                 $('.alert').removeClass('hidden');
                 $('.error').text('Error - '+xhr+' , Status : '+status+' ,Error : '+error); 
                 $('.alert').fadeOut(000);
                $('.page-spinner-bar').fadeOut(3000);
                //  location.reload();
             }
         });
     }
     return false;
 });
</script>