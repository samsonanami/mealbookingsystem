@extends('layouts.main') @section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <!-- Start calendar -->
                <section class="panel">
                    <header class="panel-heading">
                        Menu Calendar <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> <a href="javascript:;" class="fa fa-cog"></a> <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <!-- page start-->
                        <div class="row">
                            <aside class="col-lg-9">
                                <div id="calendar" class="has-toolbar"></div>
                            </aside>
                            <aside class="col-lg-3">
                                <hr>
                                <span>
                                    @can('create-booking')
                                    <button type="button" id="editabe-sample_new" class="create-bulk-booking btn btn-sm btn-success">
                                        <i class="fa fa-plus"></i>&nbsp;New Booking
                                    </button>
                                    @endcan
                                    @can('create-admin-booking')
                                    <button type="button" id="editabe-sample_new" class="create-bulk-booking-admin btn btn-sm btn-success">
                                        <i class="fa fa-plus"></i>&nbsp;Admin Booking
                                    </button>
                                    @endcan
                                    @can('create-menu')
                                    <button type="button" id="editabe-sample_new" class="create-modal btn btn-sm btn-success">
                                        <i class="fa fa-plus"></i>&nbsp; New Menu
                                    </button>
                                    @endcan
                                </span>
                                <hr>

                                @can('view-admin-dashboard')
                                <h4 class="drg-event-title">Draggable Menus</h4>
                                <div id='external-events'>
                                    @foreach($meals as $meal)
                                    <?php $color = array_rand($labels, 1); ?>
                                    <div class='external-event label {{ $labels	[$color] }}'>{{$meal->name}}</div>
                                    <br> @endforeach
                                </div>

                                <!-- <div class="form-group">
                                	<label for="agree" class="control-label">Remove after drop</label>
                                	<input type="checkbox" class="" id="agree" name="agree">
                                </div> -->
                                @endcan
                                <div id="actions">
                                    <h5>Action Hints</h5>
                                    <ol>
                                        <li>
                                            <h5><b>To book your meal:</b></h5>
                                            <p class="text-info">
                                                Click on the specific date on the calendar and select <button class="btn btn-sm"> Book meal for this day </button>
                                            </p>
                                        </li>
                                        <li>
                                            <h5><b>To cancel your meal booking:</b></h5>
                                            <p class="text-info">
                                                Click on the specific date on the calendar and select <button class="btn btn-sm"> Cancel this booking</button>
                                            </p>
                                        </li>
                                        <li>
                                            <h5><b>NB:></b></h5>
                                            <p class="text-info">
                                                You can only cancel your booking before the actual meal date. Contact the HR for emergency cancellations on the day of the meal before 9:00 A.M
                                            </p>
                                        </li>
                                        </ul>
                                </div>
                            </aside>
                        </div>
                        <!-- page end-->
                    </div>
                </section>

            </div>
        </div>
        <!-- page end-->
    </section>

</section>
<!--main content end-->



<!-- Modals -->
<div id="create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Meal Title :</label>
                        <div class="col-sm-10">
                            <select name="title" id="e1" class="input-name" style="width: 100%"> @foreach($meals as
                                $meal)
                                <option value="{{$meal->name}}">{{$meal->name}}</option>
                                @endforeach
                            </select>
                            <p class="error-name hidden parsley-required" role="alert">

                        </div>
                    </div>
                    <div class="form-group" id="dates">

                        <label class="control-label col-sm-2" for="detail">Date :</label>
                        <div class="col-md-4 col-xs-11">
                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="{{date('Y-m-d')}}" class="input-append date dpYears">
                                <input id="date" name="date" type="text" placeholder="Select date" size="16" class="form-control"> <span class="input-group-btn add-on">
                                    <button class="btn btn-danger" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-11">
                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="{{date('Y-m-d')}}" class="input-append date dpYears">
                                <input id="end_date" name="end_date" type="text" placeholder="Select end date" size="16" class="form-control"> <span class="input-group-btn add-on">
                                    <button class="btn btn-danger" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btndefault pull-left" data-dismiss="modal">
                    <i class="fa fa-times"></i>&nbsp; Exit
                </button>
                <button class="btn btn-success pull-right" type="submit" id="add">
                    <span class="fa fa-check">&nbsp; </span>Submit
                </button>
            </div>
        </div>
    </div>
</div>



<div id="create-bulk-booking" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="{{$user->id}}" name="user_id"> <input type="hidden" value="{{$user->id}}" name="owner"> <input type="hidden" value="{{$user->name}}" name="user_name">

                <form class="form-horizontal" role="form">
                    <h4> Booking for user :
                        <i><b>{{ $user->name }}</b></i>
                    </h4>
                    <hr>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Date</label>
                        <div class="col-sm-10">
                            <div class="input-group input-large date">
                                <input type="text" placeholder="yyyy-mm-dd" class="form-control dpd2 start" name="from" data-date-format="yyyy-mm-dd" data-date="{{date('Y-m-d')}}" @cannot('view-admin-dashboard') data-date-start-date="1d" @endcannot>
                                <span class="input-group-addon">To</span>
                                <input type="text" placeholder="yyyy-mm-dd" class="form-control dpd2 end" name="to" data-date-format="yyyy-mm-dd" data-date="{{date('Y-m-d')}}" @cannot('view-admin-dashboard') data-date-start-date="1d" @endcannot>
                            </div>
                            <span class="help-block">Select date range</span>
                            <p class="error-date hidden parsley-required" role="alert">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success pull-right" type="submit" id="add_bulk_booking">
                    <span class="fa fa-check">&nbsp; </span>Book
                </button>
                <button class="btn btn-default pull-left" type="button" data-dismiss="modal">
                    <span class="fa fa-times"></span>&nbsp; Exit
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<div id="create-bulk-booking-admin" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="detail">Select User :</label>
                        <div class="col-sm-10">
                            <select name="owner_id" id="e2" class="input-name" style="width: 100%">
                                @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                            <p class="error-name hidden parsley-required" role="alert">

                        </div>
                    </div>
                    <div class="form-group" id="dates">

                        <label class="control-label col-sm-2" for="detail">Date :</label>
                        <div class="col-md-4 col-xs-11">
                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="{{date('Y-m-d')}}" class="input-append date dpYears">
                                <input id="date" name="start_date1" type="text" placeholder="Select date" size="16" class="form-control"> <span class="input-group-btn add-on">
                                    <button class="btn btn-danger" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <p class="error-start hidden parsley-required" role="alert">
                        </div>
                        <div class="col-md-4 col-xs-11">
                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="{{date('Y-m-d')}}" class="input-append date dpYears">
                                <input id="end_date" name="end_date1" type="text" placeholder="Select end date" size="16" class="form-control"> <span class="input-group-btn add-on">
                                    <button class="btn btn-danger" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <p class="error-end hidden parsley-required" role="alert">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btndefault pull-left" data-dismiss="modal">
                    <i class="fa fa-times"></i>&nbsp; Exit
                </button>
                <button class="btn btn-success pull-right" type="submit" id="add_admin">
                    <span class="fa fa-check">&nbsp; </span> Submit
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<div id="create-booking" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">

                    @csrf

                    <input type="hidden" id="start" class="start" name="start"></input>
                    <input type="hidden" id="end-d" class="end-d" name="end-d"></input>
                    <input type="hidden" class="user_id" name="user_id" value="{{ Auth::user()->id }}"></input>
                    <input type="hidden" class="owner" name="owner" value="{{ Auth::user()->id }}"></input>
                    <input type="hidden" class="created_by" name="created_by" value="{{ Auth::user()->name }}"></input>


                    <div class="form-group">
                        <div class="col-sm-10">
                            <h5 id="header" class="control-label text-info "></h5>
                        </div>
                    </div>

                    <div class="form-group">
                        <div id="detail" class="col-sm-10">
                            <ol></ol>
                            <p class="error-detail hidden parsley-required" role="alert">

                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-8">
                            <h4>Booking for:</h4>
                            <h3>
                                <label class="control-label col-md-4" for="detail">Start : </label>

                                <label id="start-date" class="control-label"></label>
                            </h3>
                            <h3>
                                <label class="control-label col-md-4" for="detail">End : </label>
                                <label id="end-date" class="control-label"></label>
                            </h3>
                        </div>
                        <div clss="col-md-4">
                            @can('view-admin-dashboard')
                            <div class="form-group">
                                <label class="control-label"> Price : </label>
                                <select name="price" class="btn btn-default">
                                    <option value="50">50</option>
                                    <option value="80">80</option>
                                    <option value="100">100</option>
                                    <option value="150">150</option>
                                    <option value="200">200</option>
                                </select>
                                <button class="btn btn-success text-right" id="attach-price" type="button" data-dismiss="modal">
                                    <span class="fa fa-money">&nbsp; </span>Attach Price
                                </button>
                            </div>
                            @endcan
                        </div>

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class="fa fa-times"></i>&nbsp; Exit
                </button>
                <button class="btn btn-danger" id="cancel-book-meal" type="submit" data-dismiss="modal">
                    <span class="fa fa-times">&nbsp; </span>Cancel this Booking
                </button>
                <button class="btn btn-success" id="book-meal" type="submit" data-dismiss="modal">
                    <span class="fa fa-check">&nbsp; </span>Book Meal for this day
                </button>
            </div>
        </div>
    </div>


    <div id="show" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">ID :</label> <span id="i" class="text-info"></span>
                    </div>
                    <div class="form-group">
                        <label for="">Name :</label> <span id="nam" class="text-info"></span>
                    </div>
                    <div class="form-group">
                        <label for="">Created Date :</label> <span id="crd" class="text-info"></span>
                    </div>
                    <div class="form-group">
                        <label for="">Last Updated :</label> <span id="lsu" class="text-info"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="modal">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fid" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nam1">
                            </div>
                        </div>
                    </form>


                    <div class="deleteContent">
                        Are You sure want to delete <span class="title"></span>? <span class="hidden id"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn actionBtn" data-dismiss="modal">
                        <span id="footer_action_button" class="glyphicon"></span>
                    </button>
                    <button type="button" class="btn btn-white pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i>&nbsp; Exit
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).on('click', '.create-modal', function() {
            $('#create').modal('show');
            $('.form-horizontal').show();
            $('.modal-title').text('Add menu');
        });
        //Create bulk booking modal
        $(document).on('click', '.create-bulk-booking', function() {
            $('#create-bulk-booking').modal('show');
            $('.form-horizontal').show();
            $('.modal-title').text('Create bulk booking');
        });
        //Create bulk booking modal
        $(document).on('click', '.create-bulk-booking-admin', function() {
            $('#create-bulk-booking-admin').modal('show');
            $('.form-horizontal').show();
            $('.modal-title').text('Create bulk booking for admin');
        });

        //Action Create booking
        $("#add").click(function() {
            $.ajax({
                type: 'POST',
                url: '/menu',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'start': $('input[name=date]').val(),
                    'end': $('input[name=end_date]').val()
                },
                success: function(data) {
                    if (data.errors) {
                        if (data.errors.name) {
                            $('.error-name').removeClass('hidden');
                            $('.error-name').addClass('text-danger');
                            $('.input-name').addClass('parsley-error');
                            $('.error-name').text(data.errors.name);
                        }
                    } else {
                        $('#success-msg').removeClass('hidden');
                        location.reload();
                    }
                },
            });
            $('.input-name').removeClass('parsley-error');
            $('.error-name').addClass('hidden');
            $('.error-detail').addClass('hidden');
        });

        //Action Create Admin booking
        $("#add_admin").click(function() {
            var url = "{{ route('booking-for-user') }}";
            var created_by = "{{$user->name}}";
            console.log('Add clicked');
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'user_id': $('select[name=owner_id]').val(),
                    'start': $('input[name=start_date1]').val(),
                    'end': $('input[name=end_date1]').val(),
                    'created_by': created_by,
                },
                success: function(data) {
                    console.log(data);
                    if (data.errors) {
                        if (data.errors.name) {
                            $('.error-name').removeClass('hidden');
                            $('.error-name').addClass('text-danger');
                            $('.input-name').addClass('parsley-error');
                            $('.error-name').text(data.errors.name);
                        }
                        else if(data.errors.start) {
                            $('.error-start').removeClass('hidden');
                            $('.error-start').addClass('text-danger');
                            $('.input-start').addClass('parsley-error');
                            $('.error-start').text(data.errors.start);
                        }
                        else if(data.errors.end) {
                            $('.error-end').removeClass('hidden');
                            $('.error-end').addClass('text-danger');
                            $('.input-end').addClass('parsley-error');
                            $('.error-end').text(data.errors.end);
                        }
                    } else {
                        $('#success-msg').removeClass('hidden');
                        location.reload();
                    }
                },
            });
            $('.input-name').removeClass('parsley-error');
            $('.error-name').addClass('hidden');
            $('.error-detail').addClass('hidden');
        });


        // Action Edit/Update
        $(document).on('click', '.edit-modal', function() {
            $('#footer_action_button').text(" Update");
            $('#footer_action_button').addClass('glyphicon-check');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').addClass('edit');
            $('.modal-title').text('menu Edit');
            $('.deleteContent').hide();
            $('.form-horizontal').show();

            $('#fid').val($(this).data('id'));
            $('#nam1').val($(this).data('name'));
            $('#tit1').val($(this).data('title'));
            $('#crd1').text($(this).data('created_at'));
            $('#lsu1').text($(this).data('updated_at'));
            $('#myModal').modal('show');

        });

        $('.modal-footer').on('click', '.edit', function() {
            console.log('Samdoh edit button click');
            $.ajax({
                type: 'PUT',
                url: '/menu/' + $("#fid").val(),
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#fid").val(),
                    'name': $("#nam1").val(),
                },
                success: function(data) {
                    if (data.errors) {
                        if (data.errors.name) {
                            $('.error-name').removeClass('hidden');
                            $('.error-name').addClass('text-danger');
                            $('.input-name').addClass('parsley-error');
                            $('.error-name').text(data.errors.name);
                        }
                    } else {
                        $('.post' + data.id).replaceWith(" " +
                            "<tr class='post" + data.id + "'>" +
                            "<td>" + data.id + "</td>" +
                            "<td>" + data.created_at + "</td>" +
                            "<td><button class='show-modal btn btn-white' data-id='" + data.id +
                            "' data-body='" + data.body +
                            "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-white' data-id='" +
                            data.id + "' data-body='" + data.body +
                            "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger' data-id='" +
                            data.id + "' data-body='" + data.body +
                            "'><span class='glyphicon glyphicon-trash'></span></button></td>" +
                            "</tr>");
                        location.reload();
                    }
                }
            });
        });

        // form Delete function
        $(document).on('click', '.delete-modal', function() {
            $('#footer_action_button').text(" Delete");
            $('#footer_action_button').removeClass('glyphicon-check');
            $('#footer_action_button').addClass('glyphicon-trash');
            $('.actionBtn').removeClass('btn-success');
            $('.actionBtn').addClass('btn-danger');
            $('.actionBtn').addClass('delete');
            $('.modal-title').text('Delete Post');
            $('.id').text($(this).data('id'));
            $('.deleteContent').show();
            $('.form-horizontal').hide();
            $('.title').html($(this).data('title'));
            $('#myModal').modal('show');
        });

        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'DELETE',
                url: '/menu/' + $('.id').text(),
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('.id').text()
                },
                success: function(data) {
                    $('.post' + $('.id').text()).remove();
                    location.reload();
                }
            });
        });

        // Show function
        $(document).on('click', '.show-modal', function() {
            $('#show').modal('show');
            $('#i').text($(this).data('id'));
            $('#nam').text($(this).data('name'));
            $('#tit').text($(this).data('title'));
            $('#crd').text($(this).data('created_at'));
            $('#lsu').text($(this).data('updated_at'));
            $('.modal-title').text('Show menu');
        });

        // Addd bulk booking
        $('#add_bulk_booking').click(function() {
            var url = "{{ route('booking_book') }}";
            var title = "Booked";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'name': $('input[name=name]').val(),
                    'title': title,
                    'start': $('input[name=from]').val(),
                    'end': $('input[name=to]').val(),
                    'user_id': $('input[name=user_id]').val(),
                    'owner': $('input[name=owner]').val(),
                    'created_by': $('input[name=user_name]').val()
                },
                success: function(data) {
                    console.log('booking POST successful');
                    console.log(data);
                    if (data.errors) {
                        if (data.errors.start) {
                            alert(data.errors.start);
                            location.reload();
                            $('.error-from').removeClass('hidden');
                            $('.error-from').addClass('text-danger');
                            $('input[name=from]').addClass('parsley-error');
                            $('.input[namee=from]').text(data.errors.start);
                        } else if (data.errors.to) {
                            $('.error-to').removeClass('hidden');
                            $('.error-to').addClass('text-danger');
                            $('.input-to').addClass('parsley-error');
                            $('.error-to').text(data.errors.to);
                        }
                    } else {
                        console.log(data);
                        location.reload();
                    }
                },
                error: function(xhr, status, error) {
                    if (xhr.status == 500) {
                        var errorMessage = "Booking already done for the selected date range";
                        $('.error-date').removeClass('hidden');
                        $('.error-date').addClass('text-danger');
                        $('.date').addClass('has-error');
                        $('.error-date').text(errorMessage);
                        alert('Error - ' + errorMessage);
                    } else {
                        var errorMessage = xhr.status + ': ' + xhr.statusText + xhr.statusMessage
                        alert('Error - ' + errorMessage);
                    }
                },
            });
        });
        // Restrict date selection
        // 	$("#start").datepicker({ startDate: "today" });

        // 	$('#start').datepicker({
        //             format: 'mm-dd-yyyy',
        //             autoclose:true,
        //             startDate: "today",
        //             minDate: today
        //         });

        // 	$('.end').datepicker({
        //         format: 'mm-dd-yyyy',
        //         autoclose:true,
        //         endDate: "today",
        //         maxDate: today
        //     });
    </script>

    @endsection