<form role="form">
    <?php echo csrf_field(); ?>
    <div class="form-group">
        <label for="name">Route Name<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="name" name="name" placeholder="e.g route.default ">
        <label class="error-name"></label>
    </div>
   
    <button type="button" id="add_module" class="btn btn-primary"><i class="fa fa-check" ></i>Submit</button>
</form>


<script type="text/javascript">
// Craete module

$("#add_module").click(function() {
    $.ajax({
        type: 'POST',
        url: '<?php echo e(route('route.store')); ?>',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=name]').val(),
        },
        success: function(data) {
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-name').removeClass('hidden').addClass('text-danger');
                    $('#name').addClass('has-error');
                    $('.error-name').text(data.errors.title);
                } 
            } else {
                console.log(data);
                location.reload();
            }
        },
    });
});
</script><?php /**PATH /var/www/html/apps/mealbooking/resources/views/route_create.blade.php ENDPATH**/ ?>