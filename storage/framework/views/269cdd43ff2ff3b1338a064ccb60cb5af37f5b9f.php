<aside>
            <div id="sidebar" class="nav-collapse">
                <!-- sidebar menu start-->
                <div class="leftside-navigation">
                    <ul class="sidebar-menu" id="nav-accordion">
                        
                        <?php $__currentLoopData = $modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view-'.$module->title, 'view-'.$module->title)): ?>
                        <li>
                            <a href="<?php echo e(route($module->link, ['id'=> $module->id])); ?>">
                            <i class="<?php echo e($module->icon); ?>"></i>
                                <span><?php echo e($module->title); ?></span>
                            </a>
                            <?php $children = $module->children; ?>
                            <?php if($children): ?>
                                <?php $__currentLoopData = $children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <ul class="sub" style="display: block;">
                                	<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view-'.$child->title, 'view-'.$child->title)): ?>
                                        <li><a href="<?php echo e(route($child->link, ['id'=> $child->id])); ?>"><i class="<?php echo e($child->icon); ?>"></i><?php echo e($child->title); ?></a></li>
                                    <?php endif; ?>
                                    </ul>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php endif; ?>
                        </li>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                          <li>
                          <a href="<?php echo e(route('logout')); ?>"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                    class="fa fa-user"></i> Log Out</a>
                          </li>
                    </ul>
                </div>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end--><?php /**PATH /var/www/html/apps/mealbooking/resources/views/layouts/sidebar.blade.php ENDPATH**/ ?>