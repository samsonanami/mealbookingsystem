<form role="form">
    <?php echo csrf_field(); ?>
    <div class="form-group">
        <label for="title">Title<i class="text-danger">*</i></label>
        <input type="title" class="form-control" id="title" name="title" placeholder="Enter title ...">
        <label class="error-title"></label>
    </div>
    <div class="form-group">
        <label for="detail">Detail</label>
        <textarea class="form-control" id="detail" name="detail" placeholder="Describe here ..."></textarea>
        <label class="error-detail"></label>
    </div>
    <div class="text-right">
        <button type="button" data-dismiss="modal" id="" class="btn btn-default pull-left"><i class="fa fa-times" ></i>&nbsp; Exit</button>
        <button type="button" id="add_right" class="btn btn-success pull-right"><i class="fa fa-check" ></i>&nbsp; Submit</button>
        <br><br>
    </div>
</form>


<script type="text/javascript">
// Craete right

$("#add_right").click(function() {
    $.ajax({
        type: 'POST',
        url: '<?php echo e(route('right.store')); ?>',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=title]').val(),
            'detail': $('textarea[name=detail]').val(),
        },
        success: function(data) {
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden').addClass('text-danger');
                    $('#title').addClass('has-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden').addClass('text-danger');
                    $('#detail').addClass('has-error');
                    $('.error-detail').text(data.errors.detail);
                }
            } else {
                console.log(data);
                location.reload();
            }
        },
    });
});
</script><?php /**PATH /var/www/html/apps/mealbooking/resources/views/right_create.blade.php ENDPATH**/ ?>