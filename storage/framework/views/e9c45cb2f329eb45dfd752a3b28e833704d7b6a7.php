<div class="col-md-12">
	<!--widget start-->
	<aside class="profile-nav alt">
		<section class="panel">
			<div class="user-heading alt gray-bg">
				<a href="#"> <img alt="" src="<?php echo e(asset('bucket/images/user.png')); ?>">
				</a>
				<h1 id="title">Jim Doe</h1>
				<p id="phone">Project Manager</p>
			</div>

			<ul class="nav nav-pills nav-stacked">
				<li><a href="javascript:;"> <i class="fa fa-phone"></i> 
						Phone <span id="phone"></span></span><span class="badge label-success pull-right r-activity">1</span></a></li>
				<li><a href="javascript:;"> <i class="fa fa-envelope-o"></i> 
						Address <span id="address"></span></span><span class="badge label-success pull-right r-activity">1</span></a></li>
				<li><a href="javascript:;"> <i class="fa fa-tasks"></i>
						Active <span id="active"></span><span class="badge label-danger pull-right r-activity">1</span></a></li>
			</ul>
		</section>
	</aside>
	<!--widget end-->
</div><?php /**PATH /var/www/html/apps/mealbooking/resources/views/caterer_show.blade.php ENDPATH**/ ?>