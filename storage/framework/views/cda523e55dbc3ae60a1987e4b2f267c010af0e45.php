<?php

use App\User;

$user = User::find(Auth::user()->id);
?>

<!--header start-->
<header class="header fixed-top clearfix">
	<!--logo start-->
	<div class="brand">

		<a href="#" class="logo col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<img src="<?php echo e(asset('bucket/images/STL Logo_.png')); ?>" alt="eMeal" class="img-fluid responsive" style="margin: -8% 0 0 12%; min-width:10%; max-width:55%;">
		</a>
		<div class="sidebar-toggle-box">
			<div class="fa fa-bars"></div>
		</div>
	</div>
	<!--logo end-->

	<div class="nav notify-row" id="top_menu">
		<!--  notification start -->

		<!--  notification end -->
	</div>
	<div class="top-nav clearfix">
		<!--search & user info start-->
		<ul class="nav pull-right top-menu">
			<!-- <li>
				<input type="text" class="form-control search" placeholder=" Search">
			</li> -->
			<!-- user login dropdown start-->
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle">
					<img alt="" src="<?php echo e(asset('bucket/images/user.png')); ?>">
					<span class="username"><?php echo e(Auth::user()->username); ?></span>
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu extended logout">
					<li><a href="#" id="user-profile" data-toggle="modal" data-target="#profile-modal"><i class=" fa fa-suitcase"></i>Profile</a></li>
					<!--<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li> -->
					<li><a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-key"></i> Log Out</a></li>
					<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;"><?php echo csrf_field(); ?></form>
				</ul>
			</li>
			<!-- user login dropdown end -->
			<!-- <li>
				<div class="toggle-right-box">
					<div class="fa fa-bars"></div>
				</div>
			</li> -->
		</ul>
		<!--search & user info end-->
	</div>
</header>

<!-- User profile modal -->
<div class="modal fade" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title pull-left" id="exampleModalLongTitle">User Profile</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body clearfix">
				<aside class="profile-nav alt">
					<section class="panel">
						<div class="user-heading alt bg-info text-center">
							<a href="#">
								<img alt="" src="<?php echo e(asset('bucket/images/user.png')); ?>">
							</a>
							<h1><?php echo e($user->FIRST_NAME); ?></h1>
							<p><?php echo e($user->roles[0]['title']); ?></p>
						</div>

						<div class="" id="view-section">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="javascript:;"> <i class="fa fa-tag"></i> Full Name : <span class="text-info text-lg pull-right"><?php echo e($user->FIRST_NAME); ?> <?php echo e($user->LAST_NAME); ?></span></a></li>
								<li><a href="javascript:;"> <i class="fa fa-envelope-o"></i> Email Address : <span class="text-info text-lg pull-right"><?php echo e($user->email); ?></span></a></li>
								<li><a href="javascript:;"> <i class="fa fa-list"></i> Payroll # : <span class="text-info text-lg pull-right"><?php echo e($user->USER_ID); ?></span></a></li>
								<li><a href="javascript:;"> <i class="fa fa-phone"></i> Phone # : <span class="text-info text-lg pull-right"><?php echo e(Auth::user()->PHONE_NO); ?></span></a></li>
								<li><a href="javascript:;"> <i class="fa fa-users"></i> Role : <span class="text-info text-lg pull-right"><?php echo e($user->roles[0]['title']); ?></span></a></li>
							</ul>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-info btn-md border btn-edit-user" id="btn-edit-user"><i class="fa fa-pencil"></i>&nbsp; Edit</button>
						</div>

						<div class="hidden" id="edit-section">
							<div class="form-group col-md-5 col-sm-2">
								<label for="id">User ID<i class="text-danger">*</i></label>
								<input type="text" class="form-control" id="h_id" name="h_id" value="<?php echo e($user->id); ?>" disabled>
								<label class="error-id"></label>
							</div>
							<div class="form-group col-md-7 col-sm-2">
								<label for="name">User Name<i class="text-danger">*</i></label>
								<input type="text" class="form-control" id="h_username" name="h_username" value="<?php echo e($user->username); ?>">
								<label class="error-h_username"></label>
							</div>
							<div class="form-group col-md-6 col-sm-6">
								<label for="name">Full Name<i class="text-danger">*</i></label>
								<input type="text" class="form-control" id="h_name" name="h_name" value="<?php echo e($user->name); ?>">
								<label class="error-h_name"></label>
							</div>
							<div class="form-group col-md-6 col-sm-6">
								<label for="email">Email</label>
								<input type="text" class="form-control" id="h_email" name="h_email" value="<?php echo e($user->email); ?>">
								<label class="error-h_email"></label>
							</div>
							<div class="form-group col-md-6 col-sm-6">
								<label for="phone">Phone</label>
								<input type="text" class="form-control" id="h_phone" name="h_phone" value="<?php echo e($user->phone); ?>">
								<label class="error-h_phone"></label>
							</div>
							<div class="form-group col-md-6 col-sm-6">
								<label for="payroll">Payroll</label>
								<input type="text" class="form-control" id="h_payroll" name="h_payroll" value="<?php echo e($user->payroll); ?>">
								<label class="error-h_payroll"></label>
							</div>
							<div class="form-group col-md-6 col-sm-6">
								<label for="h_password">Password</label>
								<input type="password" class="form-control" id="h_password" name="h_password">
								<label class="error-h_password"></label>
							</div>

							<div class="form-group col-md-6 col-sm-6">
								<label for="h_password_confiormation">Confirm Password</label>
								<input type="password" class="form-control" id="h_password_confirmation" name="h_password_confirmation">
								<label class="error-h_password_confirmation"></label>
							</div>


							<div class="form-group col-md-6 col-sm-6">
								<label for="h_role">Role</label>
								<input type="h_role" class="form-control" id="h_role" name="h_role" value="<?php echo e($user->roles[0]['title']); ?>" disabled>
								<label class="error-h_role"></label>
							</div>

							<div class="form-group col-md-6 col-sm-6">
								<button type="button" class="btn btn-success btn-md border" id="btn-update-user"><i class="fa fa-pencil"></i>&nbsp; Update Profile</button>
							</div>
						</div>
					</section>
				</aside>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white btn-md border " data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Exit</button>
			</div>
		</div>
	</div>
</div>


<script>
	$(".btn-edit-user").click(function() {
		console.log('user edit clicked');
		$("#edit-section").removeClass('hidden');
		$(this).addClass('hidden');
		$("#view-section").addClass('hidden');
	});



	$("#btn-update-user").click(function() {
		var url = '<?php echo e(route("user.update", ["user"=>":user"])); ?>';
		$.ajax({
			type: 'PATCH',
			url: url.replace(":user", $('input[name=id]').val()),
			data: {(":user", $('input[name=id]').val()),
			data: {
				'_token': $('input[name=_token]').val(),
				'name': $('input[name=h_name]').val(),
				'username': $('input[name=h_username]').val(),
				'email': $('input[name=h_email]').val(),
				'phone': $('input[name=h_phone]').val(),
				'payroll': $('input[name=h_payroll]').val(),
				'password': $('input[name=h_password]').val(),
				'active': 1,
			},
			success: function(data) {
				if (data.errors) {
					console.log(data.errors);
					if (data.errors.phone) {
						$('.error-phone').removeClass('hidden').addClass('text-danger');
						$('#phone').addClass('has-error');
						$('.error-phone').text(data.errors.phone);
					} else if (data.errors.payroll) {
						$('.error-payroll').removeClass('hidden').addClass('text-danger');
						$('#payroll').addClass('has-error');
						$('.error-payroll').text(data.errors.payroll);
					} else if (data.errors.password) {
						$('.error-password').removeClass('hidden').addClass('text-danger');
						$('#password').addClass('has-error');
						$('.error-password').text(data.errors.password);
					}
				} else {
					console.log(data);
					location.reload();
				}
			},
		});
	});
</script><?php /**PATH /var/www/html/apps/mealbooking/resources/views/layouts/header.blade.php ENDPATH**/ ?>