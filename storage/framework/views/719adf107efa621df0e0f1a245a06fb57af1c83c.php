<?php $__env->startSection('content'); ?>
<div class="container">

<form  action="" class="form-signin">
<h2 class="form-signin-heading">
    <a target="_blank" href="http://www.stl-horizon.com">
        <img class="margin10" alt="" height="32" src="<?php echo e(asset('bucket/images/eHorizon.png')); ?>" />
        <h5><strong>MealBooking</strong></h5>
    </a>
</h2>
 <h4 class="text-center"><strong><?php echo e(__('Reset Password')); ?></strong></h4>

 			<div class="login-wrap">
            <div class="user-login-info">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="id" value="<?php echo e($id); ?>">
						<label for="email" class="pull-left"><?php echo e(__('E-Mail')); ?></label>
                        <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e($email ?? old('email')); ?>" required autocomplete="email" autofocus>

                        <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                        <label for="password" class="pull-left"><?php echo e(__('Password')); ?></label>

                        <input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="new-password">

                        <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                         <label for="password" class="pull-left"><?php echo e(__('Confirm Password')); ?></label>
                         <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
    
                        <button type="button" class="btn btn-success pull-righ btn-block reset-password">
                            <?php echo e(__('Reset Password')); ?>

                        </button>
               </div>
               </div>
           </form>
         </div>
         
<?php $__env->stopSection(); ?>






<?php echo $__env->make('layouts.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/reset.blade.php ENDPATH**/ ?>