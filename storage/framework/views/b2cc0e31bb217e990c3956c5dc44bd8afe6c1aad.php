<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">

    <title><?php echo e(config('app.name', 'MealBS')); ?></title>
    
    <link rel="shortcut icon" href="images/favicon.html">
    <title>BucketAdmin</title>
    <!--Core CSS -->
	<script src="<?php echo e(asset('bucket/js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/jquery-ui/jquery-ui-1.10.1.custom.min.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/bs3/js/bootstrap.min.js')); ?>"></script>
    
    <link href="<?php echo e(asset('bucket/bs3/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/js/jquery-ui/jquery-ui-1.10.1.custom.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/css/bootstrap-reset.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/js/jvector-map/jquery-jvectormap-1.2.2.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/css/clndr.css')); ?>" rel="stylesheet">
    <!--clock css-->
    <link href="<?php echo e(asset('bucket/js/css3clock/css/style.css')); ?>" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('bucket/js/morris-chart/morris.css')); ?>">
    <!-- Custom styles for this template -->
    <link href="<?php echo e(asset('bucket/css/style.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/css/style-responsive.css')); ?>" rel="stylesheet"/>
    
    
    <link href="<?php echo e(asset('css/components-rounded.css')); ?>" rel="stylesheet">
    <script src="<?php echo e(asset('js/spinner.js')); ?>"></script>
    
     <?php echo toastr_css(); ?>
     
</head>
<body>
<!-- BEGIN PAGE SPINNER -->
	<div class="page-spinner-bar" id="page-spinner-bar">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>
	<!-- END PAGE SPINNER -->


<?php echo toastr_js(); ?>
<?php echo app('toastr')->render(); ?>

    <section id="container">
        <!--header start-->
        <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!--header end-->

        <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>
        
    </section>

<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="<?php echo e(asset('bucket/js/jquery.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/bs3/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jquery-ui/jquery-ui-1.10.1.custom.min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jquery.dcjqaccordion.2.7.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jquery.scrollTo.min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jquery.nicescroll.js')); ?>"></script>

<!--[if lte IE 8]>
<script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="<?php echo e(asset('bucket/js/skycons/skycons.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jquery.scrollTo/jquery.scrollTo.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/calendar/clndr.js')); ?> "></script>
<script src="<?php echo e(asset('bucket/cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/calendar/moment-2.2.1.js')); ?> "></script>
<script src="<?php echo e(asset('bucket/js/evnt.calendar.init.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jvector-map/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jvector-map/jquery-jvectormap-us-lcc-en.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/gauge/gauge.js')); ?>"></script>
<!--clock init-->
<script src="<?php echo e(asset('bucket/js/css3clock/js/css3clock.js')); ?>"></script>
<!--Easy Pie Chart-->
<script src="<?php echo e(asset('bucket/js/easypiechart/jquery.easypiechart.js')); ?>"></script>
<!--Sparkline Chart-->
<script src="<?php echo e(asset('bucket/js/sparkline/jquery.sparkline.js')); ?>"></script>
<!--Morris Chart-->
<script src="<?php echo e(asset('bucket/js/morris-chart/morris.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/morris-chart/raphael-min.js')); ?>"></script>
<!--jQuery Flot Chart-->
<script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.tooltip.min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.resize.js')); ?>"></script>
<!--<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>-->
<script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.pie.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.animator.min.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.growraf.js')); ?>"></script>

<script src="<?php echo e(asset('bucket/js/dashboard.js')); ?>"></script>
<script src="<?php echo e(asset('bucket/js/jquery.customSelect.min.js')); ?>" ></script>
<!--common script init for all pages-->
<script src="<?php echo e(asset('bucket/js/scripts.js')); ?>"></script>

<script type="text/javascript"
	src="<?php echo e(asset('bucket/js/data-tables/jquery.dataTables.js')); ?>"></script>
<script type="text/javascript"
	src="<?php echo e(asset('bucket/js/data-tables/DT_bootstrap.js')); ?>"></script>

<!--common script init for all pages-->
<script src="<?php echo e(asset('bucket/js/scripts.js')); ?>"></script>

<!--script for this page only-->
<script src="<?php echo e(asset('bucket/js/table-editable.js')); ?>"></script>
	
</body>

</html><?php /**PATH /var/www/html/apps/mealbooking/resources/views/layouts/app.blade.php ENDPATH**/ ?>