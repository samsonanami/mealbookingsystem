 <?php $__env->startSection('content'); ?>

<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->

		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading"> SMS Reminders Dashboard </header>
					<div class="panel-body">
						<div class="row">
						<div class="alert alert-success alert-block fade in hidden">
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <h4>
                                <i class="icon-ok-sign"></i>
                                <label id="error-header"></label>
                            </h4>
                            <label id="error-message"></label>
                        </div>
                        
							<form class="form-horizontal bucket-form" method="get">
								<div class="col-md-3">
									<label class="btn btn-round btn-block btn-white"><i class="fa fa-users"></i> &nbsp; Step 1 :
									Select Recipients</label>
									<br>
									<select id="recepient"
										name="recepient" class="form-control">
										<option label="" value="0">Select recepients ...</option>
										<option label="" value="1">Caterers: All caterers</option>
										<option label="" value="2">Users : All system users</option>
										<option label="" value="3">Visitors: All Registered Visitors</option>
										<option label="" value="4">Students: All registered students</option>
									</select>
								</div>

								<div class="col-md-4">
									<label class="btn btn-round btn-block btn-white"><i class="fa fa-envelope"></i>&nbsp;Step 2:
											Select Message</label>
									</label>
										<div class="" style="margin-left: 50px;">
										<?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php $color = array_rand($colors,1); ?>
										<div class="square-<?php echo e($colors[$color]); ?> single-row">
											<div class="checkbox">
												<input type="checkbox" name="sms" value="<?php echo e($message->detail); ?>">
												<label class="text-primary "><b><?php echo e($message->title); ?></b></label>
											</div>
										</div>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</div>
								</div>
								<div class="col-md-2">
									<p class="btn btn-round btn-block btn-white">Step 3</p>
									<br>
									<div class="form-group">
										<div class="text-center">
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('send-sms')): ?>
											<button type="button" class="btn btn-success" id="sendSms"
												data-sms="">
												Send Message(s) <i class="fa fa-check"></i>
											</button>
										<?php endif; ?>
										</div>
									</div>
								</div>
							</form>
						</div>
						<br>

					</div>
				</section>
			</div>
		</div>
		<!-- page end-->


		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Messages Table <span class="tools pull-right"> <a
							href="javascript:;" class="fa fa-chevron-down"></a> <a
							href="javascript:;" class="fa fa-cog"></a> <a href="javascript:;"
							class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
								<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-sms')): ?>
									<button type="button" id="editabe-sample_new"
										class="create-modal btn btn-success">
										<i class="fa fa-plus"></i> Add New
									</button>
								<?php endif; ?>
								</div>

							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Detail</th>
										<th>Date Created</th>
										<th>Late Updated</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr class="">
										<td><?php echo e($message->id); ?></td>
										<td><?php echo e($message->title); ?></td>
										<td><?php echo e($message->detail); ?></td>
										<td><?php echo e($message->created_at); ?></td>
										<td><?php echo e($message->updated_at); ?></td>
										<td class="text-right"><a href="#"
											class="show-modal btn btn-default"
											data-id="<?php echo e($message->id); ?>" data-title="<?php echo e($message->title); ?>"
											data-detail="<?php echo e($message->detail); ?>"
											data-created_at="<?php echo e($message->created_at); ?>"
											data-updated_at="<?php echo e($message->updated_at); ?>"> <i
												class="fa fa-eye"></i>
										</a>
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('update-sms')): ?>
										<a href="#" class="edit-modal btn btn-default"
											data-id="<?php echo e($message->id); ?>" data-title="<?php echo e($message->title); ?>"
											data-detail="<?php echo e($message->detail); ?>"
											data-created_at="<?php echo e($message->created_at); ?>"
											data-updated_at="<?php echo e($message->updated_at); ?>"> <i
												class="fa fa-pencil"></i>
										</a>
										<?php endif; ?>
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete-sms')): ?>
										<a href="#" class="delete-modal btn btn-danger"
											data-id="<?php echo e($message->id); ?>" data-title="<?php echo e($message->title); ?>"
											data-detail="<?php echo e($message->detail); ?>"
											data-created_at="<?php echo e($message->created_at); ?>"
											data-updated_at="<?php echo e($message->updated_at); ?>"> <i
												class="fa fa-trash-o"></i>
										</a>
										<?php endif; ?>
										</td>

									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
		</div>
		<!-- page end-->

	</section>
</section>
<!--main content end-->


<!-- Modals -->


<div id="create" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Name :</label>
						<div class="col-sm-10">
							<input id="name" type="text" class="form-control input-name"
								id="name" name="name" placeholder="Meal name here">
							<p class="error-title hidden parsley-required" role="alert">
						
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Detail :</label>
						<div class="col-sm-10">
							<textarea id="detail" class="form-control input-detail"
								id="detail" name="detail" placeholder="Description here"
								rows="6" required></textarea>
							<p class="error-detail hidden parsley-required" role="alert">
						
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success" type="submit" id="add">
					<span class="fa fa-check">&nbsp; </span>Submit
				</button>
				<button class="btn btn-default pull-left" type="button"
					data-dismiss="modal">
					<span class="fa fa-times"></span>&nbsp; Exit
				</button>
			</div>
		</div>
	</div>
</div>
</div>



<div id="show" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">ID :</label> <span id="i" class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Name :</label> <span id="nam" class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Detail :</label> <span id="det" class="text-primary"></b>
				
				</div>
				<div class="form-group">
					<label for="">Created Date :</label> <span id="crd"
						class="text-primary"></span>
				</div>
				<div class="form-group">
					<label for="">Last Updated :</label> <span id="lsu"
						class="text-primary"></span>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="modal">
					<div class="form-group">
						<label class="control-label col-sm-2" for="id">ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="fid" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="name">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nam1">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Detail</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="det1" rows="6"></textarea>
						</div>
					</div>
				</form>


				<div class="deleteContent">
					Are You sure want to delete <span class="title"></span>? <span
						class="hidden id"></span>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn actionBtn" data-dismiss="modal">
					<span id="footer_action_button" class="glyphicon"></span>
				</button>
				<button type="button" class="btn btn-success pull-left"
					data-dismiss="modal">
					<span class="glyphicon glyphicon"></span>close
				</button>
			</div>
		</div>
	</div>
</div>



<!-- End Modals -->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<script type="text/javascript">
$(document).on('click', '.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Add SMS');
});

//Action Create

$("#add").click(function() {
    $.ajax({
        type: 'POST',
        url: '/sms',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=name]').val(),
            'detail': $('textarea[name=detail]').val()
        },
        success: function(data) {
            console.log('Submit successful');
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden');
                    $('.error-title').addClass('text-danger');
                    $('.input-title').addClass('parsley-error');
                    $('.error-title').text(data.errors.name);
                }else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.title);
                }
            } else {
                location.reload();
            }
        },
    });
    $('.input-name').removeClass('parsley-error');
    $('.error-name').addClass('hidden');
    $('.input-detail').removeClass('parsley-error');
    $('.error-detail').addClass('hidden');
});

// Action Edit/Update
$(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-primary');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Meal Edit');
    $('.deleteContent').hide();
    $('.form-horizontal').show();

    $('#fid').val($(this).data('id'));
    $('#nam1').val($(this).data('title'));
    $('#det1').text($(this).data('detail'));
    $('#crd1').text($(this).data('created_at'));
    $('#lsu1').text($(this).data('updated_at'));
    $('#myModal').modal('show');

});

$('.modal-footer').on('click', '.edit', function() {
    console.log('Samdoh edit button click');
    $.ajax({
        type: 'PUT',
        url: '/sms/'+$("#fid").val(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'title': $("#nam1").val(),
            'detail': $('#det1').val()
        },
        success: function(data) {
                console.log('Submit successful');
            if (data.errors) {
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden');
                    $('.error-title').addClass('text-danger');
                    $('.input-title').addClass('parsley-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.detail);
                }
            } else {
                location.reload();
            }
        }
    });
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('#footer_action_button').removeClass('glyphicon-check');
    $('#footer_action_button').addClass('glyphicon-trash');
    $('.actionBtn').removeClass('btn-primary');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: '/sms/' + $('.id').text(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
            $('.post' + $('.id').text()).remove();
            location.reload();
        }
    });
});

// Show function
$(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#i').text($(this).data('id'));
    $('#nam').text($(this).data('name'));
    $('#tit').text($(this).data('title'));
    $('#det').text($(this).data('detail'));
    $('#crd').text($(this).data('created_at'));
    $('#lsu').text($(this).data('updated_at'));
    $('.modal-title').text('Show SMS');
});


// Create
$("#sendSms").click(function() {
    console.log("send sms clicked");
    var messages= { 'messages' : []};
    var group = $('#recepient option:selected').val();

    $(":checked").each(function() {
    	messages['messages'].push($(this).val());
    });
    
    if (confirm("Are you SURE you want to send the MESSAGE(s)?")) { 
		
	$('#page-spinner-bar').fadeIn(1000);
	
        $.ajax({
            type: 'POST',
            url: '<?php echo e(route('sms.send')); ?>',
            data: {
                '_token': $('input[name=_token]').val(),
                'messages': messages['messages'],
                'group': group,
            },
            success: function(data) {
                console.log(data);
                if (data.error) {
                    // location.reload();
                    $('.alert').removeClass('hidden');
                	$('.alert').removeClass('alert-success');
                	$('.alert').addClass('alert-danger');
                	$('#error-header').text("Error");
                	$('#error-message').text(data.error);
                    console.log(data.error);
					$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
                }if (data.error2) {
                    // location.reload();
                    $('.alert').removeClass('hidden');
                	$('.alert').removeClass('alert-success');
                	$('.alert').addClass('alert-danger');
                	$('#error-header').text("Error");
					$('#error-message').text(data.error2);
                    console.log(data.error);
					$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
                } else {
					console.log(data);
					$('.alert').removeClass('hidden');
                	$('.alert').removeClass('alert-danger');
                	$('.alert').addClass('alert-success');
                	$('#error-header').text("Success!");
					$('#error-message').text('Message(s) sent successfully!');
					$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
				}
			},
			default: function(){
				$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
			},
			error: function(){
				$('#page-spinner-bar').fadeOut(1000);
					$('.alert').fadeOut(3000);
			}
        });
    }
});
</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.sms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/sms.blade.php ENDPATH**/ ?>