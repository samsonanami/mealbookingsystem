<?php $__env->startSection('content'); ?>

<?php

use App\Models\Admin\Caterers;
use App\Models\Admin\Students;
use App\Models\Admin\Visitors;
use App\Models\Booking\Booking;
use App\User;
use Illuminate\Support\Carbon;
use koolreport\widgets\google\BarChart;
use koolreport\widgets\google\LineChart;
use koolreport\widgets\google\PieChart;
use koolreport\widgets\koolphp\Table;

$users = User::all()->count();
$students = Students::all()->count();
$caterers = Caterers::all()->count();
$visitor = Visitors::all()->count();

$user_categories = array(
    array("category" => "Users", "user_count" => $users),
    array("category" => "Students", "user_count" => $students),
    array("category" => "Caterers", "user_count" => $caterers),
    array("category" => "Visitors", "user_count" => $visitor),
);

$booking_1  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 1)->count();
$booking_1_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 1)->count();
$booking_2  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 2)->count();
$booking_2_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 2)->count();
$booking_3  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 3)->count();
$booking_3_start = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 3)->count();
$booking_4  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 4)->count();
$booking_4_start = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 4)->count();
$booking_5  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 5)->count();
$booking_5_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 5)->count();
$booking_6  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 6)->count();
$booking_6_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 6)->count();
$booking_7  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 7)->count();
$booking_7_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 7)->count();
$booking_8  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 8)->count();
$booking_8_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 8)->count();
$booking_9  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 9)->count();
$booking_9_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 9)->count();
$booking_10  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 10)->count();
$booking_10_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 10)->count();
$booking_11  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 11)->count();
$booking_11_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 11)->count();
$booking_12  = Booking::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', 12)->count();
$booking_12_start  = Booking::whereYear('start', Carbon::now()->year)->whereMonth('start', 12)->count();

$time_sale = array(
    array("month" => "January", "booking" => $booking_1, 'booking_for' => $booking_1_start),
    array("month" => "February", "booking" => $booking_2, 'booking_for' => $booking_2_start),
    array("month" => "March", "booking" => $booking_3, 'booking_for' => $booking_3_start),
    array("month" => "April", "booking" => $booking_4, 'booking_for' => $booking_4_start),
    array("month" => "May", "booking" => $booking_5, 'booking_for' => $booking_5_start),
    array("month" => "June", "booking" => $booking_6, 'booking_for' => $booking_6_start),
    array("month" => "July", "booking" => $booking_7, 'booking_for' => $booking_7_start),
    array("month" => "August", "booking" => $booking_8, 'booking_for' => $booking_8_start),
    array("month" => "September", "booking" => $booking_9, 'booking_for' => $booking_9_start),
    array("month" => "October", "booking" => $booking_10, 'booking_for' => $booking_10_start),
    array("month" => "November", "booking" => $booking_11, 'booking_for' => $booking_11_start),
    array("month" => "December", "booking" => $booking_12, 'booking_for' => $booking_12_start),
);


$booking  = Booking::whereDate('created_at', Carbon::today())->count();
$plates  = Booking::whereDate('start', Carbon::today())->count();


$today_booking = array(
    array("category" => "Staffs", "booking" => $booking, "plates" => $plates),
    // array("category" => "Students", "booking" => 4, "plates" => 3, "cancels" => 7),
    // array("category" => "Visitors", "booking" => 5, "plates" => 3, "cancels" => 10),
);


$booking_data = Booking::whereDate('start', Carbon::today())->get();
$array = $booking_data->toArray();
foreach($array as $key=>$value){
    $array[$key] = array_merge($array[$key], ['price'=>80]);
    $user_name = User::findOrFail($array[$key]['user_id'])->first('name');
    $array[$key]['user_id'] = $user_name['name'];
}

?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!--mini statistics start-->
        <div class="row">
            <div class="col-md-3">
                <a href="<?php echo e(route('user.index')); ?>">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon orange"><i class="fa fa-users"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo e($user_count); ?></span>
                            Users
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="<?php echo e(route('meal.index')); ?>">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon tar"><i class="fa fa-tag"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo e($meal_count); ?></span>
                            Meals
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="<?php echo e(route('booking.index')); ?>">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon pink"><i class="fa fa-list"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo e($booking_count); ?></span>
                            Bookings
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="<?php echo e(route('menu.index')); ?>">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon green"><i class="fa fa-list"></i></span>
                        <div class="mini-stat-info">
                            <span><?php echo e($food_count); ?></span>
                            Menu(s)
                        </div>
                    </div>
            </div>
            </a>
        </div>
        <!--mini statistics end-->
        <div class="row">
            <div class="col-md-4">
                <a href="<?php echo e(route('booking.index')); ?>">
                    <section class="panel">
                        <div class="panel-body">
                            <h4 class="widget-h">User Categories</h4>
                            <?php
                            PieChart::create(array(
                                // "title" => "Use Categories",
                                "dataSource" => $user_categories,
                                "columns" => array(
                                    "category",
                                    "user_count" => array(
                                        "type" => "number",
                                        "prefix" => "Count : ",
                                    )
                                )
                            ));
                            ?>
                        </div>
                    </section>
                </a>
            </div>


            
            <div class="col-md-8">
                <section class="panel">
                    <div class="panel-body">
                        <h4 class="widget-h">Today's Booking</h4>
                        <?php
                        BarChart::create(array(
                            "dataSource" => $today_booking,
                            "columns" => array(
                                "category",
                                "booking" => array("label" => "Booking", "type" => "number"),
                                "plates" => array("label" => "Plates", "type" => "number"),
                            )
                        ));
                        ?>
                    </div>
                </section>
            </div>


        </div>
        <!--mini statistics end-->


        <!--mini statistics start-->
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="top-stats-panel">
                            <div class="daily-visit">
                                <h4 class="widget-h">Monthly Bookings</h4>
                                <?php
                                Table::create(array(
                                    "dataStore"=>$array,
                                    "columns"=>array(
                                        "created_by"=>array(
                                            "label"=>"Booked By",
                                            "type"=>"string",
                                        ),
                                        "start"=>array(
                                            "label"=>"Date",
                                            "type"=>"date",
                                        ),
                                        "user_id"=>array(
                                            "label"=>"User",
                                            "type"=>'number'
                                        ),
                                        "user_id"=>array(
                                            "label"=>"User",
                                            "type"=>'string'
                                        ),
                                        "updated_at"=>array(
                                            "label"=>"Created",
                                            "type"=>'date'
                                        ),
                                        "price"=>array(
                                            "label"=>"Price",
                                            "type"=>'number'
                                        )
                                    ),
                                    "cssClass"=>array(
                                        "table"=>"table table-hover table-bordered"
                                    )
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>



        <!--mini statistics start-->
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="top-stats-panel">
                            <div class="daily-visit">
                                <h4 class="widget-h">Monthly Bookings</h4>
                                <?php
                                LineChart::create(array(
                                    // "title"=>"Sale vs Cost",
                                    "dataSource" => $time_sale,
                                    "columns" => array(
                                        "month",
                                        "booking" => array(
                                            "label" => "Booking",
                                            "type" => "number",
                                            // "prefix"=>"$"
                                        ),
                                        "booking_for" => array(
                                            "label" => "Plates Booked",
                                            "type" => "number",
                                            // "prefix"=>"$"
                                        ),
                                    )
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

    </section>
</section>
<!--main content end-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashboard', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/dashboard-admin.blade.php ENDPATH**/ ?>