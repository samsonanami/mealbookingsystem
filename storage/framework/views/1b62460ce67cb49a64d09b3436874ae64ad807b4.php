 <?php $__env->startSection('content'); ?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Menu Table <span class="tools pull-right"> <a href="javascript:;"
                                class="fa fa-chevron-down"></a> <a href="javascript:;" class="fa fa-cog"></a> <a
                                href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table editable-table ">
                            <div class="clearfix">
                                <div class="btn-group pull-right">
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-menu')): ?>
                                    <button type="button" id="editabe-sample_new" class="create-modal btn btn-success">
                                        <i class="fa fa-plus"></i> Add New
                                    </button>
                                 <?php endif; ?>
                                </div>
                                
                            </div>
                            <div class="space15"></div>
                            <table class="table table-striped table-hover table-bordered" id="editable-sample">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Detail</th>
                                        <th>Date</th>
                                        <th>Date End</th>
                                        <th>Date Created</th>
                                        <th>Late Updated</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="">
                                        <td><?php echo e($menu->id); ?></td>
                                        <td><?php echo e($menu->title); ?></td>
                                        <td><?php echo e($menu->detail); ?></td>
                                        <td><?php echo e($menu->start); ?></td>
                                        <td><?php echo e($menu->end); ?></td>
                                        <td><?php echo e($menu->created_at); ?></td>
                                        <td><?php echo e($menu->updated_at); ?></td>
                                        <td class="text-right">
                                        
                                		<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view-menu')): ?>	
                                        	<a href="#" class="show-modal btn btn-default"
                                                data-id="<?php echo e($menu->id); ?>" data-name="<?php echo e($menu->name); ?>"
                                                data-detail="<?php echo e($menu->detail); ?>" data-created_at="<?php echo e($menu->created_at); ?>"
                                                data-updated_at="<?php echo e($menu->updated_at); ?>"> <i class="fa fa-eye"></i>
                                            </a>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('update-menu')): ?>
                                        <a href="#" class="edit-modal btn btn-default" data-id="<?php echo e($menu->id); ?>"
                                                data-name="<?php echo e($menu->name); ?>" data-detail="<?php echo e($menu->detail); ?>"
                                                data-created_at="<?php echo e($menu->created_at); ?>"
                                                data-updated_at="<?php echo e($menu->updated_at); ?>"> <i class="fa fa-pencil"></i>
                                            </a>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete-menu')): ?>
                                            <a href="#" class="delete-modal btn btn-danger" data-id="<?php echo e($menu->id); ?>"
                                                data-name="<?php echo e($menu->name); ?>" data-detail="<?php echo e($menu->detail); ?>"
                                                data-created_at="<?php echo e($menu->created_at); ?>"
                                                data-updated_at="<?php echo e($menu->updated_at); ?>"> <i class="fa fa-trash-o"></i>
                                            </a>
                                        <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
               
            </div>
        </div>
        <!-- page end-->
    </section>

</section>
<!--main content end-->



<!-- Modals -->
<div id="create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="detail">Title :</label>
                        <div class="col-sm-10">
                            <select name="title" id="e1" class="input-name" style="width: 100%"> <?php $__currentLoopData = $meals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $meal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($meal->name); ?>"><?php echo e($meal->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <p class="error-name hidden parsley-required" role="alert">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="detail">Detail :</label>
                        <div class="col-sm-10">
                            <textarea id="detail" class="form-control input-detail" id="detail" name="detail"
                                placeholder="Description here" required></textarea>
                            <p class="error-detail hidden parsley-required" role="alert">

                        </div>
                    </div>
                    <div class="form-group" id="dates">

                        <label class="control-label col-sm-2" for="detail">Date :</label>
                        <div class="col-md-4 col-xs-11">
                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="<?php echo e(date('Y-m-d')); ?>"
                               class="input-append date dpYears">
                                <input id="date" name="date" type="text" placeholder="Select date"
                                    size="16" class="form-control"> <span class="input-group-btn add-on">
                                    <button class="btn btn-danger" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-11">
                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="<?php echo e(date('Y-m-d')); ?>"
                                 class="input-append date dpYears">
                                <input id="end_date" name="end_date" type="text"
                                    placeholder="Select end date" size="16" class="form-control"> <span
                                    class="input-group-btn add-on">
                                    <button class="btn btn-danger" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                  
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default pull-left" type="button" data-dismiss="modal">
                    <span class="fa fa-times"></span>&nbsp; Exit
                </button>
                <button class="btn btn-success pull-right" type="submit" id="add">
                    <span class="fa fa-check">&nbsp; </span>Submit
                </button>
            </div>
        </div>
    </div>
</div>


<!-- Modals -->
<div id="create-booking" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">

                    <?php echo csrf_field(); ?>

                    <input type="hidden" id="start" class="start" name="start"></input> <input type="hidden"
                        class="user_id" name="user_id" value="<?php echo e(Auth::user()->id); ?>"></input> <input type="hidden"
                        class="owner" name="owner" value="<?php echo e(Auth::user()->id); ?>"></input> <input type="hidden"
                        class="created_by" name="created_by" value="<?php echo e(Auth::user()->name); ?>"></input>


                    <div class="form-group">
                        <div class="col-sm-10">
                            <h5 id="header" class="control-label text-info "></h5>
                        </div>
                    </div>

                    <div class="form-group">
                        <div id="detail" class="col-sm-10">
                            <ol></ol>
                            <p class="error-detail hidden parsley-required" role="alert">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="detail">Start : </label>
                        <div class="col-sm-10">
                            <label id="start-date" class="control-label"></label>
                        </div>
                        <label class="control-label col-sm-2" for="detail">End : </label>
                        <div class="col-sm-10">
                            <label id="end-date" class="control-label"></label>
                        </div>

                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default pull-left" type="button" data-dismiss="modal">
                <span class="fa fa-times"></span>&nbsp; Exit
            </button>
            <button class="btn btn-warning" id="cancel-book-meal" type="submit" data-dismiss="modal">
                <span class="fa fa-times">&nbsp; </span>Cancel this Booking
            </button>
            <button class="btn btn-success pull-right" id="book-meal" type="submit" data-dismiss="modal">
                <span class="fa fa-check">&nbsp; </span>Book Meal for this day
            </button>
        </div>
    </div>
</div>
</div>


<div id="show" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">ID :</label> <span id="i" class="text-info"></span>
                </div>
                <div class="form-group">
                    <label for="">Name :</label> <span id="nam" class="text-info"></span>
                </div>
                <div class="form-group">
                    <label for="">Detail :</label> <span id="det" class="text-info"></b>

                </div>
                <div class="form-group">
                    <label for="">Created Date :</label> <span id="crd" class="text-info"></span>
                </div>
                <div class="form-group">
                    <label for="">Last Updated :</label> <span id="lsu" class="text-info"></span>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="modal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="id">ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fid" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nam1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="detail">Detail</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="det1"></textarea>
                        </div>
                    </div>
                </form>


                <div class="deleteContent">
                    Are You sure want to delete <span class="title"></span>? <span class="hidden id"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn actionBtn" data-dismiss="modal">
                    <span id="footer_action_button" class="fa fa-check"></span>
                </button>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <span class="fa fa-times"></span>&nbsp; Exit
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).on('click', '.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Add menu');
});

//Action Create

$("#add").click(function() {
    $.ajax({
        type: 'POST',
        url: '/menu',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('select[name=title]').val(),
            'start': $('input[name=date]').val(),
            'end': $('input[name=end_date]').val(),
            'detail': $('textarea[name=detail]').val()
        },
        success: function(data) {
            if (data.errors) {
                if (data.errors.name) {
                    $('.error-name').removeClass('hidden');
                    $('.error-name').addClass('text-danger');
                    $('.input-name').addClass('parsley-error');
                    $('.error-name').text(data.errors.name);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.title);
                }
            } else {
                $('#success-msg').removeClass('hidden');
                location.reload();
            }
        },
    });
    $('.input-name').removeClass('parsley-error');
    $('.error-name').addClass('hidden');
    $('.input-detail').removeClass('parsley-error');
    $('.error-detail').addClass('hidden');
});

// Action Edit/Update
$(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-info');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Menu Edit');
    $('.deleteContent').hide();
    $('.form-horizontal').show();

    $('#fid').val($(this).data('id'));
    $('#nam1').val($(this).data('name'));
    $('#tit1').val($(this).data('title'));
    $('#det1').text($(this).data('detail'));
    $('#crd1').text($(this).data('created_at'));
    $('#lsu1').text($(this).data('updated_at'));
    $('#myModal').modal('show');

});

$('.modal-footer').on('click', '.edit', function() {
    console.log('Samdoh edit button click');
    $.ajax({
        type: 'PUT',
        url: '/menu/' + $("#fid").val(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'name': $("#nam1").val(),
            'detail': $('#det1').val()
        },
        success: function(data) {
            if (data.errors) {
                if (data.errors.name) {
                    $('.error-name').removeClass('hidden');
                    $('.error-name').addClass('text-danger');
                    $('.input-name').addClass('parsley-error');
                    $('.error-name').text(data.errors.name);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.detail);
                }
            } else {
                $('.post' + data.id).replaceWith(" " +
                    "<tr class='post" + data.id + "'>" +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.detail + "</td>" +
                    "<td>" + data.created_at + "</td>" +
                    "<td><button class='show-modal btn btn-white' data-id='" + data.id +
                    "' data-body='" + data.body +
                    "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-white' data-id='" +
                    data.id + "' data-body='" + data.body +
                    "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger' data-id='" +
                    data.id + "' data-body='" + data.body +
                    "'><span class='glyphicon glyphicon-trash'></span></button></td>" +
                    "</tr>");
                location.reload();
            }
        }
    });
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('#footer_action_button').removeClass('glyphicon-check');
    $('#footer_action_button').addClass('glyphicon-trash');
    $('.actionBtn').removeClass('btn-info');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: '/menu/' + $('.id').text(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
            $('.post' + $('.id').text()).remove();
            location.reload();
        }
    });
});

// Show function
$(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#i').text($(this).data('id'));
    $('#nam').text($(this).data('name'));
    $('#tit').text($(this).data('title'));
    $('#det').text($(this).data('detail'));
    $('#crd').text($(this).data('created_at'));
    $('#lsu').text($(this).data('updated_at'));
    $('.modal-title').text('Show menu');
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/menu.blade.php ENDPATH**/ ?>