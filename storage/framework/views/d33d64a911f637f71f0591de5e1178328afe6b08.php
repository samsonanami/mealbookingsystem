<form role="form">
    <?php echo csrf_field(); ?>
    <div class="form-group">
        <label for="title">Full Name<i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="title" name="title" placeholder="enter studnt name ...">
        <label class="error-title"></label>
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" class="form-control" id="phone" name="phone" placeholder="phone...">
        <label class="error-phone"></label>
    </div>
    <div class="form-group">
        <label for="school">School</label>
        <input type="text" class="form-control" id="school" name="school" placeholder="enter school/college...">
        <label class="error-school"></label>
    </div>
    <div class="form-group">
        <label for="password">Address</label>
        <textarea class="form-control" id="address" name="address" placeholder="enter address ..."></textarea>
        <label class="error-address"></label>
    </div>
   
    <div class="form-group">
        <label for="active">Active<i class="text-danger">*</i></label>
        <select class="form-control" id="active" name="active">
            <option value=1 selected>Yes</option>
            <option value="0">No</option>
        </select>
        <label class="error-active"></label>
    </div>
    
	<button type="button" id="calcel" data-dismiss="modal" class="btn btn-default pull-left" ><i class="fa fa-times" ></i>Exit</button> 
    <button type="button" id="add" class="btn btn-success pull-right"><i class="fa fa-check" ></i>Submit</button>
    <br><br>
    
    
</form>


<script type="text/javascript">
// Craete module

$("#add").click(function() {
    $.ajax({
        type: 'POST',
        url: '<?php echo e(route('student.store')); ?>',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=title]').val(),
            'phone': $('input[name=phone]').val(),
            'school': $('input[name=school]').val(),
            'address': $('textarea[name=address]').val(),
            'active': $('select[name=active]').val(),
        },
        success: function(data) {
            if (data.errors) {
                console.log(data.errors);
                if (data.errors.name) {
                    $('.error-title').removeClass('hidden').addClass('text-danger');
                    $('#title').addClass('has-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.school) {
                    $('.error-school').removeClass('hidden').addClass('text-danger');
                    $('#school').addClass('has-error');
                    $('.error-school').text(data.errors.school);
                }else if (data.errors.phone) {
                    $('.error-phone').removeClass('hidden').addClass('text-danger');
                    $('#phone').addClass('has-error');
                    $('.error-phone').text(data.errors.phone);
                }else if (data.errors.address) {
                    $('.error-address').removeClass('hidden').addClass('text-danger');
                    $('#address').addClass('has-error');
                    $('.error-address').text(data.errors.address);
                }else if (data.errors.active) {
                    $('.error-active').removeClass('hidden').addClass('text-danger');
                    $('#active').addClass('has-error');
                    $('.error-active').text(data.errors.active);
                }
            } else {
                console.log(data);
                location.reload();
            }
        },
    });
});
</script><?php /**PATH /var/www/html/apps/mealbooking/resources/views/student_create.blade.php ENDPATH**/ ?>