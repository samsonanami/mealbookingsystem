<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head"></div>
		<!-- END PAGE HEAD -->


		<!-- BEGIN ACTUAL CONTENT -->
		<div id="menus_page" class="hidden portlet light" id="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-home font-green-sharp"></i> <span
						class="caption-subject font-green-sharp bold uppercase ng-binding">
						Administration </span> <span class="caption-helper ng-binding">
						Panel </span>
				</div>
				<div>
					<button title="Click to switch to import mode"
						class="add-button btn  btn-success btn-sm pull-right">
						<i class="fa fa-upload"></i> Import Task
					</button>
					
					<button class="btn btn-success btn-sm pull-right"
						style="margin-right: 15px">
						<i class="fa fa-plus"></i> Create Task
					</button>
				</div>
			</div>
			<!-- Echo Data Import Template -->
			<div class="portlet-body paddingT20"></div>
			<!-- Echo Data Import Template -->
		</div>
		<!-- END ACTUAL CONTENT -->
		
		<!-- BEGIN BOOKING ACTUAL CONTENT -->
		<div class="hidden portlet light" id="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-home font-green-sharp"></i> <span
						class="caption-subject font-green-sharp bold uppercase ng-binding">
						Administration </span> <span class="caption-helper ng-binding">
						Panel </span>
				</div>
				<div>
					<button title="Click to switch to import mode"
						class="add-button btn  btn-success btn-sm pull-right">
						<i class="fa fa-upload"></i> Import Task
					</button>
					<button class="btn btn-success btn-sm pull-right"
						style="margin-right: 15px">
						<i class="fa fa-plus"></i> Create Task
					</button>
				</div>
			</div>
			<!-- Echo Data Import Template -->
			<div class="portlet-body paddingT20"></div>
			<!-- Echo Data Import Template -->
		</div>
		<!-- END ACTUAL BOOKING CONTENT -->
		
		
		

	</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dashboard', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/home.blade.php ENDPATH**/ ?>