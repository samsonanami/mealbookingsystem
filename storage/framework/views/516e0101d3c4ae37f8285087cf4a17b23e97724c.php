<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bucketadmin.themebucket.net/editable_table.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Feb 2018 17:35:23 GMT -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">

    <title><?php echo e(config('app.name', 'MealBS')); ?></title>
    
    <!--Core CSS -->
	<script src="<?php echo e(asset('bucket/js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/jquery-ui/jquery-ui-1.10.1.custom.min.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/bs3/js/bootstrap.min.js')); ?>"></script>


    <link href="<?php echo e(asset('bucket/bs3/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/css/bootstrap-reset.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet" />

    <link href="<?php echo e(asset('bucket/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('bucket/js/bootstrap-datepicker/css/datepicker.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('bucket/js/bootstrap-timepicker/css/timepicker.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('bucket/js/bootstrap-colorpicker/css/colorpicker.css')); ?>" />
    <link rel="stylesheet" type="text/css"
        href="<?php echo e(asset('bucket/js/bootstrap-daterangepicker/daterangepicker-bs3.css')); ?>" />
    <link rel="stylesheet" type="text/css"
        href="<?php echo e(asset('bucket/js/bootstrap-datetimepicker/css/datetimepicker.css')); ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('bucket/js/jquery-multi-select/css/multi-select.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('bucket/js/jquery-tags-input/jquery.tagsinput.css')); ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('bucket/js/select2/select2.css')); ?>" />

    <!-- Custom styles for this template -->
    <link href="<?php echo e(asset('bucket/css/style.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/css/style-responsive.css')); ?>" rel="stylesheet" />
    
    <!--Core CSS -->
    <link href="<?php echo e(asset('bucket/js/jquery-ui/jquery-ui-1.10.1.custom.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/js/jvector-map/jquery-jvectormap-1.2.2.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('bucket/css/clndr.css')); ?>" rel="stylesheet">
    <!--clock css-->
    <link href="<?php echo e(asset('bucket/js/css3clock/css/style.css')); ?>" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('bucket/js/morris-chart/morris.css')); ?>">
    <!-- Custom styles for this template -->
    <link href="<?php echo e(asset('bucket/css/style.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('bucket/css/style-responsive.css')); ?>" rel="stylesheet" />

    <link href="<?php echo e(asset('css/components-rounded.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(asset('bucket/js/fullcalendar/bootstrap-fullcalendar.css')); ?>" rel="stylesheet" />

    <script src="<?php echo e(asset('js/spinner.js')); ?>"></script>

    <?php echo toastr_css(); ?>
</head>

<body>

    <!-- BEGIN PAGE SPINNER -->
    <div class="page-spinner-bar" id="page-spinner-bar">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
    <!-- END PAGE SPINNER -->

    <?php echo toastr_js(); ?>
    <?php echo app('toastr')->render(); ?>

    <section id="container">
        <!--header start-->
        <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!--header end-->
        <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!--sidebar end-->
        <?php echo $__env->yieldContent('content'); ?>

        <!--right sidebar start-->
        <div class="right-sidebar">
            <div class="search-row">
                <input type="text" placeholder="Search" class="form-control">
            </div>
            <div class="right-stat-bar">
                <ul class="right-side-accordion">
                    <li class="widget-collapsible"><a href="#" class="head widget-head red-bg active clearfix"> <span
                                class="pull-left">work progress (5)</span> <span class="pull-right widget-collapse"><i
                                    class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row side-mini-stat clearfix">
                                    <div class="side-graph-info">
                                        <h4>Target sell</h4>
                                        <p>25%, Deadline 12 june 13</p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="target-sell"></div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info">
                                        <h4>product delivery</h4>
                                        <p>55%, Deadline 12 june 13</p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="p-delivery">
                                            <div class="sparkline" data-type="bar" data-resize="true" data-height="30"
                                                data-width="90%" data-bar-color="#39b7ab" data-bar-width="5"
                                                data-data="[200,135,667,333,526,996,564,123,890,564,455]"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info payment-info">
                                        <h4>payment collection</h4>
                                        <p>25%, Deadline 12 june 13</p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="p-collection">
                                            <span class="pc-epie-chart" data-percent="45"> <span class="percent"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info">
                                        <h4>delivery pending</h4>
                                        <p>44%, Deadline 12 june 13</p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="d-pending"></div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="col-md-12">
                                        <h4>total progress</h4>
                                        <p>50%, Deadline 12 june 13</p>
                                        <div class="progress progress-xs mtop10">
                                            <div style="width: 50%" aria-valuemax="100" aria-valuemin="0"
                                                aria-valuenow="20" role="progressbar"
                                                class="progress-bar progress-bar-info">
                                                <span class="sr-only">50% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible"><a href="#" class="head widget-head terques-bg active clearfix">
                            <span class="pull-left">contact online (5)</span> <span
                                class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo e(asset('bucket/images/avatar1_small.jpg')); ?>" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4>
                                            <a href="#">Jonathan Smith</a>
                                        </h4>
                                        <p>Work for fun</p>
                                    </div>
                                    <div class="user-status text-danger">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo e(asset('bucket/images/avatar1.jpg')); ?>" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4>
                                            <a href="#">Anjelina Joe</a>
                                        </h4>
                                        <p>Available</p>
                                    </div>
                                    <div class="user-status text-success">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo e(asset('bucket/images/chat-avatar2.jpg')); ?>" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4>
                                            <a href="#">John Doe</a>
                                        </h4>
                                        <p>Away from Desk</p>
                                    </div>
                                    <div class="user-status text-warning">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo e(asset('bucket/images/avatar1_small.jpg')); ?>" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4>
                                            <a href="#">Mark Henry</a>
                                        </h4>
                                        <p>working</p>
                                    </div>
                                    <div class="user-status text-info">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo e(asset('bucket/images/avatar1.jpg')); ?>" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4>
                                            <a href="#">Shila Jones</a>
                                        </h4>
                                        <p>Work for fun</p>
                                    </div>
                                    <div class="user-status text-danger">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <p class="text-center">
                                    <a href="#" class="view-btn">View all Contacts</a>
                                </p>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible"><a href="#" class="head widget-head purple-bg active"> <span
                                class="pull-left">
                                recent activity (3)</span> <span class="pull-right widget-collapse"><i
                                    class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">just now</p>
                                        <p>
                                            <a href="#">Jim Doe </a>Purchased new equipments for zonal
                                            office setup
                                        </p>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">2 min ago</p>
                                        <p>
                                            <a href="#">Jane Doe </a>Purchased new equipments for zonal
                                            office setup
                                        </p>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">1 day ago</p>
                                        <p>
                                            <a href="#">Jim Doe </a>Purchased new equipments for zonal
                                            office setup
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible"><a href="#" class="head widget-head yellow-bg active"> <span
                                class="pull-left">
                                shipment status</span> <span class="pull-right widget-collapse"><i
                                    class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="col-md-12">
                                    <div class="prog-row">
                                        <p>Full sleeve baby wear (SL: 17665)</p>
                                        <div class="progress progress-xs mtop10">
                                            <div class="progress-bar progress-bar-success" role="progressbar"
                                                aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                                style="width: 40%">
                                                <span class="sr-only">40% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row">
                                        <p>Full sleeve baby wear (SL: 17665)</p>
                                        <div class="progress progress-xs mtop10">
                                            <div class="progress-bar progress-bar-info" role="progressbar"
                                                aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                                style="width: 70%">
                                                <span class="sr-only">70% Completed</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!--right sidebar end-->

    </section>

    <!-- Placed js at the end of the document so the pages load faster -->


    <!--Core js-->
    <script src="<?php echo e(asset('bucket/js/jquery-migrate.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/jquery-ui-1.9.2.custom.min.js')); ?>"></script>
    <script class="include" type="text/javascript" src="<?php echo e(asset('bucket/js/jquery.dcjqaccordion.2.7.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/jquery.scrollTo.min.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/easypiechart/jquery.easypiechart.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/jquery.nicescroll.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/jquery.nicescroll.js')); ?>"></script>

    <script src="<?php echo e(asset('bucket/js/bootstrap-switch.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset('bucket/js/fuelux/js/spinner.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-fileupload/bootstrap-fileupload.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-wysihtml5/wysihtml5-0.3.0.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>">
    </script>
    <script type="text/javascript"
        src="<?php echo e(asset('bucket/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-daterangepicker/moment.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js')); ?>">
    </script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-timepicker/js/bootstrap-timepicker.js')); ?>">
    </script>

    <script type="text/javascript" src="<?php echo e(asset('bucket/js/jquery-multi-select/js/jquery.multi-select.js')); ?>">
    </script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/jquery-multi-select/js/jquery.quicksearch.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset('bucket/js/bootstrap-inputmask/bootstrap-inputmask.min.js')); ?>">
    </script>

    <script src="<?php echo e(asset('bucket/js/jquery-tags-input/jquery.tagsinput.js')); ?>"></script>

    <script src="<?php echo e(asset('bucket/js/select2/select2.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/select-init.js')); ?>"></script>


    <!--common script init for all pages-->
    <script src="<?php echo e(asset('bucket/js/toggle-init.js')); ?>"></script>

    <script src="<?php echo e(asset('bucket/js/advanced-form.js')); ?>"></script>
    <!--Easy Pie Chart-->
    <script src="<?php echo e(asset('bucket/js/easypiechart/jquery.easypiechart.js')); ?>"></script>
    <!--Sparkline Chart-->
    <script src="<?php echo e(asset('bucket/js/sparkline/jquery.sparkline.js')); ?>"></script>
    <!--jQuery Flot Chart-->
    <script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.tooltip.min.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.resize.js')); ?>"></script>
    <script src="<?php echo e(asset('bucket/js/flot-chart/jquery.flot.pie.resize.js')); ?>"></script>


    <script type="text/javascript" src="<?php echo e(asset('bucket/js/data-tables/jquery.dataTables.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('bucket/js/data-tables/DT_bootstrap.js')); ?>"></script>

    <!--common script init for all pages-->
    <script src="<?php echo e(asset('bucket/js/scripts.js')); ?>"></script>

    <!--script for this page only-->
    <script src="<?php echo e(asset('bucket/js/table-editable.js')); ?>"></script>



	<script src="<?php echo e(asset('bucket/js/fullcalendar/fullcalendar.min.js')); ?>"></script>
	<script src="<?php echo e(asset('bucket/js/external-dragging-calendar.js')); ?>"></script>

    <!-- END JAVASCRIPTS -->
    <script>
    jQuery(document).ready(function() {
        EditableTable.init();
    });
    </script>


</body>

<!-- Mirrored from bucketadmin.themebucket.net/editable_table.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Feb 2018 17:35:24 GMT -->

</html><?php /**PATH /var/www/html/apps/mealbooking/resources/views/layouts/main.blade.php ENDPATH**/ ?>