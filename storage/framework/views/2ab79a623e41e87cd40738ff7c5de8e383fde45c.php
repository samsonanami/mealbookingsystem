 <?php $__env->startSection('content'); ?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Caterers Table <span class="tools pull-right"> <a href="javascript:;"
							class="fa fa-chevron-down"></a> <a href="javascript:;"
							class="fa fa-cog"></a> <a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">

								<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-caterer')): ?>
									<button
									type="button" id="newcaterer"
									class="btn btn-success"
									data-toggle="modal"
									data-target="#dynamic-modal"
									data-path="<?php echo e(route('caterer.create')); ?>"
									>
										<i class="fa fa-plus"></i> Add New
									</button>
								<?php endif; ?>
								</div>
								
							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Phone</th>
										<th>Address</th>
										<th>Active</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $caterers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $caterer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr class="">
										<td><?php echo e($caterer->id); ?></td>
										<td><?php echo e($caterer->title); ?></td>
										<td><?php echo e($caterer->phone); ?></td>
										<td><?php echo e($caterer->address); ?></td>
										<td><?php echo e($caterer->active); ?></td>
										<td class="text-right"><a href="#"
											class="show-modal btn btn-default"
											data-id="<?php echo e($caterer->id); ?>"
											data-title="<?php echo e($caterer->title); ?>"
											data-phone="<?php echo e($caterer->phone); ?>"
											data-phone="<?php echo e($caterer->phone); ?>"
											data-address="<?php echo e($caterer->address); ?>"
											data-active="<?php echo e($caterer->active); ?>"
											data-path="<?php echo e(route('caterer.show',['caterer'=>$caterer->id])); ?>"
											data-toggle="modal"
											data-target="#dynamic-modal"
											> <i class="fa fa-eye"></i>
										</a> <a href="#" class="edit-modal btn btn-default"
											data-id="<?php echo e($caterer->id); ?>"
											data-title="<?php echo e($caterer->title); ?>"
											data-phone="<?php echo e($caterer->phone); ?>"
											data-phone="<?php echo e($caterer->phone); ?>"
											data-address="<?php echo e($caterer->address); ?>"
											data-active="<?php echo e($caterer->active); ?>"
											data-path="<?php echo e(route('caterer.edit',['caterer'=>$caterer->id])); ?>"
											data-toggle="modal"
											data-target="#dynamic-modal"
											>
											<i class="fa fa-pencil"></i>
										</a> <a href="#" class="delete-modal btn btn-danger"
											data-id="<?php echo e($caterer->id); ?>"
											data-title="<?php echo e($caterer->title); ?>"
											data-phone="<?php echo e($caterer->phone); ?>"
											data-phone="<?php echo e($caterer->phone); ?>"
											data-address="<?php echo e($caterer->address); ?>"
											data-active="<?php echo e($caterer->active); ?>"
											data-phone="<?php echo e($caterer->phone); ?>" data-detail="<?php echo e($caterer->active); ?>">
												<i class="fa fa-trash-o"></i>
										</a></td>

									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>
						</div>
					</div>
				</section>

			</div>
		</div>
		<!-- page end-->
	</section>

</section>
<!--main content end-->


<!-- My Dynamic Modal -->
<div id="dynamic-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class=" text-info pull-left"><?php echo e(env('APP_NAME')); ?></span>
                <button type="button" class="btn btn-danger btn-md pull-right btn-round"
                    data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <!-- Body content will come here -->
            </div>
            <div class="modal-footer">
                <?php echo e(env('APP_NAME')); ?>

            </div>
        </div>
    </div>
</div>
<!-- End modal -->


<script type="text/javascript">

$("#newcaterer").click(function () {
    $.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
        },
    });
});

//form Delete function
$(document).on('click', '.delete-modal', function() {
   $('#dynamic-modal').addClass('modal-danger');
   $('.modal-body').html('<h4 class="text-danger">Delete this item? ' + $(this).data('id') + ':' + $(this).data('title')+'</h4><input type="hidden" class="id">');
    $('.modal-footer').html('<button type="button" id="calcel" data-dismiss="modal" class="btn btn-primary" >Cancel</button> <button type="button" id="delete" class="delete btn btn-danger" >Delete</button>');
    $('#dynamic-modal').modal('show');
    $('.id').text($(this).data('id'));
});

$('.modal-footer').on('click', '.delete', function() {
	 var url = '<?php echo e(route("caterer.destroy", ["id"=>":id"])); ?>';
    $.ajax({
        type: 'DELETE',
        url: url.replace(":id", $('.id').text()),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
            location.reload();
        }
    });
});
//Action Edit/Update
$(document).on('click', '.edit-modal', function() {
	var id = $(this).data('id');
	var title = $(this).data('title');
	var phone = $(this).data('phone');
	var address = $(this).data('address');
	var active = $(this).data('active');
	
	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
            $('input[name=id]').val(id);
            $('input[name=title]').val(title);
            $('input[name=phone]').val(phone);
            $('textarea[name=address]').val(address);
            $('select[name=active]').val(active);
        },
    });
});

//Show function
$(document).on('click', '.show-modal', function() {
	var id = $(this).data('id');
	var title = $(this).data('title');
	var phone = $(this).data('phone');
	var address = $(this).data('address');
	var active = $(this).data('active');
	
	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data); 
            $('#id').text(id);
            $('#title').text(title);
            $('#phone').text(phone);
            $('#address').text(address);
            $('#active').text(active);
        },
    });
});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/caterer.blade.php ENDPATH**/ ?>