 <?php $__env->startSection('content'); ?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->

		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Booking Table <span class="tools pull-right"> <a
							href="javascript:;" class="fa fa-chevron-down"></a> <a
							href="javascript:;" class="fa fa-cog"></a> <a href="javascript:;"
							class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
								<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-admin-booking')): ?>
									<button type="button" id="editabe-sample_new"
										class="create-modal btn btn-success">
										<i class="fa fa-plus"></i> Add New
									</button>
								<?php endif; ?>
								</div>

							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Booking Date</th>
										<th>User</th>
										<th>Owner</th>
										<th>Date Created</th>
										<th>Late Updated</th>
										<th>Group</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $bookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $booking): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr class="">
										<td><?php echo e($booking->id); ?></td>
										<td><?php echo e($booking->start); ?></td>
										<td><?php echo e($booking->user->name); ?></td>
										
										<?php if($booking->group == 1): ?>
											<td><?php echo e($booking->caterer->title); ?></td>
										<?php elseif($booking->group == 2): ?>
											<td><?php echo e($booking->owner); ?></td>
										<?php elseif($booking->group == 3): ?>
											<td><?php echo e($booking->visitor->title); ?></td>
										<?php elseif($booking->group == 4): ?>
											<td><?php echo e($booking->student->title); ?></td>
										<?php else: ?>
											<td><i>Not set</i></td>
										<?php endif; ?>
										<td><?php echo e($booking->created_at); ?></td>
										<td><?php echo e($booking->updated_at); ?></td>
										
										<?php if($booking->group == 1): ?>
											<td>Caterers</td>
										<?php elseif($booking->group == 2): ?>
											<td>Users</td>
										<?php elseif($booking->group == 3): ?>
											<td>Visitors</td>
										<?php elseif($booking->group == 4): ?>
											<td>Students</td>
										<?php else: ?>
											<td><i>Not set</i></td>
										<?php endif; ?>
										
										<td class="text-right">
										
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view-admin-booking')): ?>
										<a href="#"
											class="show-modal btn btn-default" data-id="<?php echo e($booking->id); ?>"
											data-name="<?php echo e($booking->name); ?>"
											data-detail="<?php echo e($booking->detail); ?>"
											data-created_at="<?php echo e($booking->created_at); ?>"
											data-updated_at="<?php echo e($booking->updated_at); ?>"> <i
												class="fa fa-eye"></i>
										</a>
										<?php endif; ?>
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('update-admin-booking')): ?>
										<a href="#" class="edit-modal btn btn-default"
											data-id="<?php echo e($booking->id); ?>" data-name="<?php echo e($booking->name); ?>"
											data-detail="<?php echo e($booking->detail); ?>"
											data-created_at="<?php echo e($booking->created_at); ?>"
											data-updated_at="<?php echo e($booking->updated_at); ?>"> <i
												class="fa fa-pencil"></i>
										</a>
										<?php endif; ?>
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete-admin-booking')): ?>
										<a href="#" class="delete-modal btn btn-danger"
											data-id="<?php echo e($booking->id); ?>" data-name="<?php echo e($booking->name); ?>"
											data-detail="<?php echo e($booking->detail); ?>"
											data-created_at="<?php echo e($booking->created_at); ?>"
											data-updated_at="<?php echo e($booking->updated_at); ?>"> <i
												class="fa fa-trash-o"></i>
										</a>
										<?php endif; ?>
										</td>

									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>

							<div id="calendar" class="has-toolbar"></div>

						</div>
					</div>
				</section>
			</div>
		</div>
		<!-- page end-->
	</section>
</section>
<!--main content end-->


<!-- Modals -->
<div id="create-booking" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">

					<?php echo csrf_field(); ?> <input type="hidden" id="start" class="start" name="start"></input>
					<input type="hidden" class="user_id" name="user_id"
						value="<?php echo e(Auth::user()->id); ?>"></input>


					<input type="hidden" class="created_by" name="created_by"
						value="<?php echo e(Auth::user()->name); ?>"></input>


					<div class="form-group">
						<div class="col-sm-10">
							<h5 id="header" class="control-label text-info "></h5>
						</div>
					</div>

					<div class="form-group">
						<div id="detail" class="col-sm-10">
							<ol></ol>
							<p class="error-detail hidden parsley-required" role="alert">

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Start : </label>
						<div class="col-sm-10">
							<label id="start-date" class="control-label"></label>
						</div>
						<label class="control-label col-sm-2" for="detail">End : </label>
						<div class="col-sm-10">
							<label id="end-date" class="control-label"></label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default pull-left pull-left" type="button"
					data-dismiss="modal">
					<span class="fa fa-times"></span>&nbsp; Exit
				</button>
				<button class="btn btn-danger" id="cancel-book-meal" type="submit"
					data-dismiss="modal">
					<span class="fa fa-times">&nbsp; </span>Cancel Booking
				</button>
				<button class="btn btn-success" id="book-meal" type="submit"
					data-dismiss="modal">
					<span class="fa fa-check">&nbsp; </span>Book Meal for this day
				</button>
			</div>
		</div>
	</div>
</div>
<!-- Modals -->


<div id="create" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">

				<input
					type="hidden" value="<?php echo e($user->name); ?>" name="user_name">

				<form class="form-horizontal" role="form">
					<div class="form-group">
    					<label class="control-label col-sm-2" for="group">Group</label>
    					<div class="col-sm-10">
        					<select id="group"
        						name="group" class="form-control pull-left">
        						<option label="" value="0">Select group ...</option>
        						<option label="" value="2">Users</option>
        						<option label="" value="3">Visitors</option>
        						<option label="" value="4">Students</option>
        					</select>
        					<p class="error-group hidden parsley-required" role="alert"></p>
        					
        				</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Name :</label>
						<div class="col-sm-10">
							<input id="name" type="text" class="form-control input-name"
								id="name" name="name" placeholder="booking name here" required>
							<p class="error-title hidden parsley-required" role="alert"></p>

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">Date</label>
						<div class="col-sm-10">
							<div class="input-group input-large date"
								data-date="<?php echo e(date('Y-m-d')); ?>" data-date-format="mm/dd/yyyy">
								<input type="text" class="form-control dpd1" name="from"
									data-date-format="yyyy-mm-dd" data-date="<?php echo e(date('Y-m-d')); ?>"> <span
									class="input-group-addon">To</span> <input type="text"
									class="form-control dpd2" name="to"
									data-date-format="yyyy-mm-dd" data-date="<?php echo e(date('Y-m-d')); ?>">
							</div>
							<span class="help-block">Select date range</span>
							<p class="error-date hidden parsley-required" role="alert"></p>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success pull-right" type="submit"
					id="add_booking">
					<span class="fa fa-check">&nbsp; </span>Book
				</button>
				<button class="btn btn-default pull-left pull-left" type="button"
					data-dismiss="modal">
					<span class="fa fa-times"></span>&nbsp; Exit
				</button>
			</div>
		</div>
	</div>
</div>



<div id="show" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">ID :</label> <span id="i" class="text-info"></span>
				</div>
				<div class="form-group">
					<label for="">Name :</label> <span id="nam" class="text-info"></span>
				</div>
				<div class="form-group">
					<label for="">Detail :</label> <span id="det" class="text-info"></span>
				</div>
				<div class="form-group">
					<label for="">Created Date :</label> <span id="crd"
						class="text-info"></span>
				</div>
				<div class="form-group">
					<label for="">Last Updated :</label> <span id="lsu"
						class="text-info"></span>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="modal">
					<div class="form-group">
						<label class="control-label col-sm-2" for="id">ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="fid" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="name">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nam1">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="detail">Detail</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="det1"></textarea>
						</div>
					</div>
				</form>


				<div class="deleteContent">
					Are You sure want to delete <span class="title"></span>?
					<input type="hidden" name="selected_id" id="selected_id">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn actionBtn" data-dismiss="modal">
					<span id="footer_action_button" class="glyphicon"></span>
				</button>
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
					Exit
				</button>
			</div>
		</div>
	</div>
</div>



<!-- End Modals -->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<script type="text/javascript">
$(document).on('click', '.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Add Booking');
});

//Action Create

$('#add_booking').click(function() {
	console.log($('select[name=group]').val());
    $.ajax({
        type: 'POST',
        url: '/booking_admin',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': $('input[name=name]').val(),
            'detail': $('textarea[name=detail]').val(),
            'start': $('input[name=from]').val(),
            'end': $('input[name=to]').val(),
            'user_id': $('input[name=user_id]').val(),
            'group': $('select[name=group]').val(),
            'created_by': $('input[name=user_name]').val()
        },
        success: function(data) {
            console.log('booking POST successful');
            if (data.errors) {
                console.log(data);
                if (data.errors.title) {
                    $('.error-title').removeClass('hidden');
                    $('.error-title').addClass('text-danger');
                    $('.input-title').addClass('parsley-error');
                    $('.error-title').text(data.errors.title);
                } else if (data.errors.start) {
                    $('.error-date').removeClass('hidden');
                    $('.error-date').addClass('text-danger');
                    $('.input-start').addClass('parsley-error');
                    $('.error-date').text(data.errors.title);
                }else if (data.errors.end) {
                    $('.error-date').removeClass('hidden');
                    $('.error-date').addClass('text-danger');
                    $('.input-end').addClass('parsley-error');
                    $('.error-date').text(data.errors.end);
                }else if (data.errors.group) {
                    $('.error-group').removeClass('hidden');
                    $('.error-group').addClass('text-danger');
                    $('.input-group').addClass('parsley-error');
                    $('.error-group').text(data.errors.group);
                }
            } else {
                console.log(data);
                location.reload();
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error:'+XMLHttpRequest+' : '+textStatus+' : '+errorThrown+' : Suggestion;  Please Confirm that the group selected is not empty');
         }
       
    });
});

// Action Edit/Update
$(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update");
    $('#footer_action_button').removeClass('fa fa-trash');
    $('.actionBtn').addClass('btn-success pull-right');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Booking Edit');
    $('.deleteContent').hide();
    $('.form-horizontal').show();

    $('#fid').val($(this).data('id'));
    $('#nam1').val($(this).data('name'));
    $('#tit1').val($(this).data('title'));
    $('#det1').text($(this).data('detail'));
    $('#crd1').text($(this).data('created_at'));
    $('#lsu1').text($(this).data('updated_at'));
    $('#myModal').modal('show');

});

$('.modal-footer').on('click', '.edit', function() {
    console.log('Samdoh edit button click');
    $.ajax({
        type: 'PUT',
        url: '/booking_admin/' + $("#fid").val(),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'name': $("#nam1").val(),
            'detail': $('#det1').val()
        },
        success: function(data) {
            if (data.errors) {
                if (data.errors.name) {
                    $('.error-name').removeClass('hidden');
                    $('.error-name').addClass('text-danger');
                    $('.input-name').addClass('parsley-error');
                    $('.error-name').text(data.errors.name);
                } else if (data.errors.detail) {
                    $('.error-detail').removeClass('hidden');
                    $('.error-detail').addClass('text-danger');
                    $('.input-detail').addClass('parsley-error');
                    $('.error-detail').text(data.errors.detail);
                }else if (data.errors.group) {
                    $('.error-group').removeClass('hidden');
                    $('.error-group').addClass('text-danger');
                    $('.input-group').addClass('parsley-error');
                    $('.error-group').text(data.errors.group);
                }
            } else {
                $('.post' + data.id).replaceWith(" " +
                    "<tr class='post" + data.id + "'>" +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.detail + "</td>" +
                    "<td>" + data.created_at + "</td>" +
                    "<td><button class='show-modal btn btn-success' data-id='" + data.id +
                    "' data-body='" + data.body +
                    "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-success' data-id='" +
                    data.id + "' data-body='" + data.body +
                    "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger' data-id='" +
                    data.id + "' data-body='" + data.body +
                    "'><span class='glyphicon glyphicon-trash'></span></button></td>" +
                    "</tr>");
                location.reload();
            }
        }
    });
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('.actionBtn').removeClass('btn-success');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('input[name=selected_id]').val($(this).data('id'));
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function() {

	var url = '<?php echo e(route("booking.destroy", ["booking"=>":booking"])); ?>';
	
    $.ajax({
        type: 'DELETE',
        url: url.replace(":booking", $('input[name=selected_id]').val()),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('input[name=selected_id]').val(),
        },
        success: function(data) {
            $('.post' + $('.id').text()).remove();
            window.location.reload();
        }
    });
});

// Show function
$(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#i').text($(this).data('id'));
    $('#nam').text($(this).data('name'));
    $('#tit').text($(this).data('title'));
    $('#det').text($(this).data('detail'));
    $('#crd').text($(this).data('created_at'));
    $('#lsu').text($(this).data('updated_at'));
    $('.modal-title').text('Show Booking');
});
</script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/book_admin.blade.php ENDPATH**/ ?>