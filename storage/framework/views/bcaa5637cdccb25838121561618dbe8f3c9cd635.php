 <?php $__env->startSection('content'); ?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Rights View <span class="tools pull-right"> <a href="javascript:;"
							class="fa fa-chevron-down"></a> <a href="javascript:;"
							class="fa fa-cog"></a> <a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group pull-right">
								<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-rights')): ?>
									<button
									type="button" id="newresource"
									class="btn btn-success"
									data-toggle="modal"
									data-target="#dynamic-modal"
									data-path="<?php echo e(route('right.create')); ?>"
									>
										<i class="fa fa-plus"></i> Add New
									</button>
								<?php endif; ?>
								</div>
								<div class="btn-group pull-left">
									<button class="btn btn-default dropdown-toggle"
										data-toggle="dropdown">
										Tools <i class="fa fa-angle-down"></i>
									</button>

								</div>
							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered"
								id="editable-sample">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Detail</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $resources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resource): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr class="">
										<td><?php echo e($resource->id); ?></td>
										<td><?php echo e($resource->title); ?></td>
										<td><?php echo e($resource->detail); ?></td>
										<td class="text-right">
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('update-rights')): ?>
										<a href="#" class="edit-modal btn btn-default"
											data-id="<?php echo e($resource->id); ?>"
											data-title="<?php echo e($resource->title); ?>"
											data-detail="<?php echo e($resource->detail); ?>"
											data-path="<?php echo e(route('right.edit',['resource'=>$resource->id])); ?>"
											data-toggle="modal"
											data-target="#dynamic-modal"
											><i class="fa fa-pencil"></i>
										</a>
										<?php endif; ?>
										<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete-rights')): ?>
										<a href="#" class="delete-modal btn btn-danger"
											data-id="<?php echo e($resource->id); ?>"
											data-title="<?php echo e($resource->title); ?>"
											data-detail="<?php echo e($resource->detail); ?>"
											><i class="fa fa-trash-o"></i>
										</a>
										<?php endif; ?>
										</td>
									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>
						</div>
					</div>
				</section>

			</div>
		</div>
		<!-- page end-->
	</section>

</section>
<!--main content end-->


<!-- My Dynamic Modal -->
<div id="dynamic-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class=" text-info pull-left"><?php echo e(env('APP_NAME')); ?></span>
                <button type="button" class="btn btn-danger btn-md pull-right btn-round"
                    data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <!-- Body content will come here -->
            </div>
            <div class="modal-footer">
                <?php echo e(env('APP_NAME')); ?>

            </div>
        </div>
    </div>
</div>
<!-- End modal -->

<script type="text/javascript">

$("#newresource").click(function () {
    $.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data);
        },
    });
});

//form Delete function
$(document).on('click', '.delete-modal', function() {
   $('#dynamic-modal').addClass('modal-danger');
   $('.modal-body').html('<h4 class="text-danger">Delete this item? ' + $(this).data('id') + ':' + $(this).data('title')+'</h4><input type="hidden" class="id">');
    $('.modal-footer').html('<button type="button" id="calcel" data-dismiss="modal" class="btn btn-default" >Cancel</button>
    	    <button type="button" id="delete" class="delete btn btn-danger" >Delete</button>');
    $('#dynamic-modal').modal('show');
    $('.id').text($(this).data('id'));
});

$('.modal-footer').on('click', '.delete', function() {
	 var url = '<?php echo e(route("right.destroy", ["id"=>":id"])); ?>';
    $.ajax({
        type: 'DELETE',
        url: url.replace(":id", $('.id').text()),
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('.id').text()
        },
        success: function(data) {
            location.reload();
        }
    });
});
//Action Edit/Update
$(document).on('click', '.edit-modal', function() {
	var name = $(this).data('name');
	var id = $(this).data('id');
	var title = $(this).data('title');
	var color = $(this).data('color');
	var order = $(this).data('order');
	var link = $(this).data('link');
	var icon = $(this).data('icon');
	var active = $(this).data('active');
	var root = $(this).data('root');
	var parent = $(this).data('parent');
	var detail = $(this).data('detail');

	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data);
            $('input[name=id]').val(id);
            $('input[name=title]').val(title);
            $('input[name=color]').val(color);
            $('input[name=order]').val(order);
            $('input[name=link]').val(link);
            $('input[name=icon]').val(icon);
            $('select[name=active]').val(active);
            $('select[name=root]').val(root);
            $('select[name=parent]').val(parent);
            $('textarea[name=detail]').val(detail);
        },
    });
});

//Show function
$(document).on('click', '.show-modal', function() {
	var name = $(this).data('name');
	var id = $(this).data('id');
	var email = $(this).data('email');
	var role = $(this).data('role');
	var active = $(this).data('active');
	var phone = $(this).data('phone');

	$.ajax({
        type: 'GET',
        url: $(this).data('path'),
        success: function (data) {
            $('#dynamic-modal div.modal-body').html(data);
            $('#id').text(id);
            $('#name').text(name);
            $('#email').text(email);
            $('#role').text(role);
            $('#active').text(active);
            $('#phone').text(phone);
        },
    });
});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/right.blade.php ENDPATH**/ ?>