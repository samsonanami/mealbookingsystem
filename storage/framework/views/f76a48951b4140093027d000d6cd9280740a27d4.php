<?php $__env->startSection('content'); ?>

<div class="container">

    <!-- Status and notifications -->

    <?php if(session('status')): ?>
    <?php
    toastr()->success(session('status'), 'Notification', [
    'timeOut' => 3000
    ]);
    ?>
    <?php endif; ?>

    <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
    <?php
    toastr()->error($message, 'Notification', [
    'timeOut' => 3000
    ]);
    ?>
    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

    <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
    <?php
    toastr()->error($message, 'Notification', [
    'timeOut' => 3000
    ]);
    ?>
    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

    <form class="form-signin" method="POST" action="<?php echo e(route('login')); ?>">
        <?php echo csrf_field(); ?>
        <h2 class="form-signin-heading">
            <a target="_blank" href="http://www.stl-horizon.com">
                <img class="margin10" alt="" height="64px" src="<?php echo e(asset('bucket/images/STL Logo_.png')); ?>" />
                <!-- <h5><strong>MealBooking</strong></h5> -->
            </a>
        </h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(__('E-Mail')); ?></label>
                <input id="email" type="text" class="form-control <?php echo e($errors->has('email') ? ' has-warning' : ''); ?>" name="email" placeholder="User ID" value="<?php echo e(old('email')); ?>">
                <br>
                <label for="password" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Password')); ?></label>

                <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' has-warning' : ''); ?>" name="password" placeholder="Password">

            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember">
                <label class="form-check-label" for="remember">
                    <?php echo e(__('Remember Me')); ?>

                </label>
                <a class="pull-right" data-toggle="modal" href="#myModal"> Forgot Password?</a>
            </div>
<span>
            <button type="submit" id="submit" class="btn pull-right btn-default btn-block" name="submit"><i class="fa fa-check"></i> Sign in</button>
    </span>
            <br>
            <div class="text-center copyright">
                <!-- <img class="margin10" alt="" height="80" src="<?php echo e(asset('stl/theme/img/STL Logo_.png')); ?>" /><br> -->
                <a target="_blank" href="http://stl-horizon.com"><?php echo e(date('Y')); ?> &copy Software Technologies Ltd.</a>
            </div>
            <br>

        </div>
    </form>

    <form class="form-signin" method="POST" action="<?php echo e(route('password.email')); ?>">
        <?php echo csrf_field(); ?>
        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Forgot Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>

                        <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" type="submit">Send Password Reset Link</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

    </form>


</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/apps/mealbooking/resources/views/auth/login.blade.php ENDPATH**/ ?>