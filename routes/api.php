<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users', 'HomeController@users')->name('users');
Route::post('/login', 'Api\UserController@login');
Route::post('/menu', 'Api\UserController@menu');
Route::post('/booking', 'Api\UserController@booking');
Route::post('/cancel', 'Api\UserController@cancel');
Route::post('/book', 'Api\UserController@book');